/* Time and Space: O(n*2^n)
 */
/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {string[]}
 */
var wordBreak = function(s, wordDict) {
  // recursion and memoization
  const res = traverse(s, wordDict, 0, {});

  for (let i = 0; i < res.length; i++) {
    res[i] = res[i].join(" ");
  }

  return res;
};

const traverse = (str, wordDict, pos, memo) => {
  if (memo[pos]) {
    return memo[pos];
  }
  const res = [];
  if (pos === str.length) {
    res.push([]);
  }

  for (const word of wordDict) {
    const substr = str.slice(pos);
    if (substr.startsWith(word)) {
      const list = traverse(str, wordDict, pos + word.length, memo);
      for (const arr of list) {
        res.push([word, ...arr]);
      }
    }
  }

  memo[pos] = res;
  return res;
};
