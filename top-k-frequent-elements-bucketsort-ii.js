/* Time: O(N)
 * space: O(N) */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function(nums, k) {
  const freqs = {};
  for (const num of nums) {
    freqs[num] = (freqs[num] || 0) + 1;
  }

  const arr = [];
  for (const key in freqs) {
    const f = freqs[key];
    if (!arr[f]) arr[f] = [];
    arr[f].push(key);
  }

  let res = [];
  let pos = arr.length - 1;
  while (pos >= 0 && k > 0) {
    if (!arr[pos]) {
      pos--;
      continue;
    }
    if (arr[pos].length >= k) {
      res = res.concat(arr[pos].slice(0, k));
      k -= k;
    } else {
      res = res.concat(arr[pos]);
      k -= arr[pos].length;
    }
    pos--;
  }

  return res;
};
