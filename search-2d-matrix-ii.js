/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function(matrix, target) {
  if (matrix.length === 0) return false;

  const height = matrix.length;
  const width = matrix[0].length;
  let row = height - 1;
  let col = 0;
  while (row >= 0) {
    if (matrix[row][col] > target) {
      row--;
    } else {
      while (col < width && row >= 0) {
        if (matrix[row][col] === target) return true;
        if (target > matrix[row][col]) col++;
        else {
          row--;
        }
      }
      return false;
    }
  }

  return false;
};
