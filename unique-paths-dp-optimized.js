/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var uniquePaths = function(m, n) {
  const pre = new Array(n).fill(0);
  const curr = new Array(n).fill(0);
  pre[0] = 1;
  curr[0] = 1;

  for (let r = 0; r < m; r++) {
    for (let c = 0; c < n; c++) {
      if (c === 0) continue;
      curr[c] = curr[c - 1] + pre[c];
      pre[c] = curr[c];
    }
  }

  return curr[n - 1];
};
