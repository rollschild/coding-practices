/* Time: O(mn)
 * Space: O(n)
 */
/**
 * @param {character[][]} matrix
 * @return {number}
 */

var maximalSquare = function(matrix) {
  if (matrix.length === 0) return 0;
  const row = matrix.length;
  const col = matrix[0].length;
  const dp = new Array(col).fill(0);
  let prev = 0;
  let maxLen = 0;

  for (let r = 0; r < row; r++) {
    for (let c = 0; c < col; c++) {
      if (matrix[r][c] === "1") {
        const temp = dp[c];

        if (c === 0) {
          dp[c] = 1;
        } else {
          dp[c] = Math.min(dp[c - 1], prev, temp) + 1;
        }

        prev = temp;
        maxLen = Math.max(maxLen, dp[c]);
      } else {
        dp[c] = 0;
      }
    }
  }

  return maxLen * maxLen;
};
