# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        node = ListNode(0)
        node_tail = node
        carry = 0
        while l1 or l2 or carry:
            num1 = l1.val if l1 else 0
            num2 = l2.val if l2 else 0
            res = num1 + num2 + carry
            if res > 9:
                carry = 1
                res -= 10
            else:
                carry = 0

            node_tail.next = ListNode(res)
            node_tail = node_tail.next

            l1 = l1.next if l1 else None
            l2 = l2.next if l2 else None

        return node.next
