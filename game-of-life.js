/**
 * @param {number[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var gameOfLife = function(board) {
  if (board.length === 0) return;
  const h = board.length;
  const w = board[0].length;
  const directions = [
    [0, -1],
    [0, 1],
    [-1, 0],
    [1, 0],
    [-1, -1],
    [-1, 1],
    [1, -1],
    [1, 1],
  ];
  const copyBoard = new Array(h);
  for (let i = 0; i < h; i++) {
    copyBoard[i] = [...board[i]];
  }

  for (let j = 0; j < h; j++) {
    for (let k = 0; k < w; k++) {
      let liveCells = 0;
      for (direction of directions) {
        const row = j + direction[0];
        const col = k + direction[1];
        if (row >= 0 && row < h && col >= 0 && col < w) {
          if (copyBoard[row][col] === 1) liveCells++;
        }
      }
      if (copyBoard[j][k] === 1) {
        if (liveCells < 2 || liveCells > 3) board[j][k] = 0;
      } else {
        // dead
        if (liveCells === 3) board[j][k] = 1;
      }
    }
  }
};
