/* Time: worst case O(N^2)
 * Space: O(N)
 */
/**
 * @param {number[][]} intervals
 * @return {number}
 */

var minMeetingRooms = function(intervals) {
  const len = intervals.length;
  if (len === 0) return 0;
  if (len === 1) return 1;

  intervals.sort((a, b) => {
    if (a[0] < b[0]) return -1;
    else if (a[0] > b[0]) return 1;
    else return a[1] - b[1];
  });

  const rooms = [];
  let pos = 0;

  while (pos < len) {
    if (rooms.length === 0) {
      rooms.push(intervals[pos]);
    } else {
      let isAvailable = false;
      for (let i = 0; i < rooms.length; i++) {
        if (rooms[i][1] <= intervals[pos][0]) {
          rooms[i] = intervals[pos];
          isAvailable = true;
          break;
        }
      }

      if (!isAvailable) {
        rooms.push(intervals[pos]);
      }
    }

    pos++;
  }

  return rooms.length;
};
