/* Time: O(N*L^2)
 */
/**
 * @param {string[]} words
 * @return {string[]}
 */

var findAllConcatenatedWordsInADict = function(words) {
  const len = words.length;
  if (len <= 0) return [];
  const res = [];
  words = new Set(words);

  words.forEach(word => {
    if (dfs(word, words)) res.push(word);
  });

  return res;
};

const dfs = (word, words) => {
  if (word.length === 0) return false;

  for (let i = 1; i < word.length; i++) {
    const prefix = word.substring(0, i);
    const suffix = word.substring(i, word.length);

    if (words.has(prefix) && words.has(suffix)) return true;
    if (words.has(prefix) && dfs(suffix, words)) return true;
    if (words.has(suffix) && dfs(prefix, words)) return true;
  }

  return false;
};
