/* Time: O(N^2)
 * Space: O(N)
 */
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @param {number} k
 * @return {number[][]}
 */

var kSmallestPairs = function(nums1, nums2, k) {
  const arr = [];

  for (const num1 of nums1) {
    for (const num2 of nums2) {
      arr.push([num1, num2]);
    }
  }

  arr.sort((a, b) => {
    return a[0] + a[1] - b[0] - b[1];
  });

  if (arr.length < k) {
    return arr;
  }

  return arr.slice(0, k);
};
