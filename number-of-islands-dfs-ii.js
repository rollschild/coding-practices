/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function(grid) {
  // dfs
  const height = grid.length;
  if (height === 0) return 0;
  const width = grid[0].length;
  let num = 0;
  const visited = {};

  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  const dfs = (r, c, island) => {
    if (
      !visited[r * width + c] &&
      r >= 0 &&
      r < height &&
      c >= 0 &&
      c < width &&
      grid[r][c] === "1"
    ) {
      island.push(r * width + c);
      visited[r * width + c] = true;
      for (const [row, col] of directions) {
        dfs(r + row, c + col, island);
      }
    }
  };

  let island = [];
  for (let r = 0; r < height; r++) {
    for (let c = 0; c < width; c++) {
      island = [];
      dfs(r, c, island);
      if (island.length > 0) num++;
    }
  }

  return num;
};
