/**
 * @param {number} n - a positive integer
 * @return {number} - a positive integer
 */
var reverseBits = function(n) {
  let res = 0;
  let counter = 32;

  while (counter--) {
    res *= 2;
    res += n & 1;
    n >>= 1;
  }

  return res;
};
