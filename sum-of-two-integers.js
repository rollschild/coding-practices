/* Time: O(1)
 * Space: O(1) */
/**
 * @param {number} a
 * @param {number} b
 * @return {number}
 */
var getSum = function(a, b) {
  let x = Math.abs(a);
  let y = Math.abs(b);
  const sign =
    (a >= 0 && b >= 0) ||
    (a < 0 && b >= 0 && x <= y) ||
    (a >= 0 && b < 0 && x >= y)
      ? 1
      : -1;
  if (y > x) {
    const temp = x;
    x = y;
    y = temp;
  }

  if ((a >= 0 && b >= 0) || (a <= 0 && b <= 0)) {
    // add
    while (y > 0) {
      let answer = x ^ y;
      let carry = (x & y) * 2;
      x = answer;
      y = carry;
    }
  } else {
    // subtraction
    while (y > 0) {
      // y is borrow
      let answer = x ^ y;
      let borrow = (~x & y) * 2;
      x = answer;
      y = borrow;
    }
  }

  return x * sign;
};
