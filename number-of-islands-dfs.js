/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function(grid) {
  if (grid.length === 0) return 0;
  const height = grid.length;
  const width = grid[0].length;
  const islands = [];
  let island = [];
  const visited = {};

  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      explore(i, j, height, width, grid, visited, island);

      if (island && island.length > 0) islands.push(island);
      island = [];
    }
  }

  return islands.length;
};

const explore = (row, col, height, width, grid, visited, island) => {
  if (row >= 0 && row < height && col >= 0 && col < width) {
    if (!visited[row * width + col] && grid[row][col] === "1") {
      island.push(row * width + col);
      visited[row * width + col] = true;

      explore(row - 1, col, height, width, grid, visited, island);
      explore(row + 1, col, height, width, grid, visited, island);
      explore(row, col - 1, height, width, grid, visited, island);
      explore(row, col + 1, height, width, grid, visited, island);
    }
  }
};
