/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var nextPermutation = function(nums) {
  let pos = nums.length - 2;
  while (pos >= 0 && nums[pos] >= nums[pos + 1]) {
    pos--;
  }
  if (pos >= 0) {
    let j = nums.length - 1;
    while (j >= 0 && nums[j] <= nums[pos]) {
      j--;
    }
    const temp = nums[pos];
    nums[pos] = nums[j];
    nums[j] = temp;
  }

  console.log(nums);

  let start = pos + 1;
  let end = nums.length - 1;
  while (start < end) {
    const tmp = nums[start];
    nums[start] = nums[end];
    nums[end] = tmp;
    start++;
    end--;
  }
};
const nums = [1, 3, 2];
nextPermutation(nums);
console.log(nums);
