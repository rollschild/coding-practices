/**
 * @param {number[][]} grid
 * @return {number}
 */
const explore = (row, col, grid, visited, direction, res) => {
  const height = grid.length;
  const width = grid[0].length;
  if (row >= 0 && row < height && col >= 0 && col < width) {
    if (!visited[row * width + col] && grid[row][col] === 1) {
      visited[row * width + col] = true;
      res.push(direction);

      // land!
      // four directions
      explore(row - 1, col, grid, visited, 1, res);
      explore(row, col - 1, grid, visited, 2, res);
      explore(row, col + 1, grid, visited, 3, res);
      explore(row + 1, col, grid, visited, 4, res);
      res.push(0);
    }
  }
  return res;
};

var numDistinctIslands = function(grid) {
  if (grid.length === 0) return 0;
  const height = grid.length;
  const width = grid[0].length;
  const visited = {};
  const obj = {};

  for (let i = 0; i < height; i++) {
    for (let j = 0; j < width; j++) {
      const res = explore(i, j, grid, visited, 0, []);
      console.log("res:", res);
      if (res.length > 0 && !obj[res]) obj[res] = 1;
      console.log("obj: ", obj);
    }
  }

  return Object.entries(obj).length;
};

console.log(
  numDistinctIslands([
    [1, 1, 0, 0, 0],
    [1, 1, 0, 0, 0],
    [0, 0, 0, 1, 1],
    [0, 0, 0, 1, 1],
  ]),
);
