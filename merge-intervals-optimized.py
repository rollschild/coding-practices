class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        intervals.sort(key=lambda item: item[0])

        res = []

        for item in intervals:
            if not res or res[-1][1] < item[0]:
                res.append(item)
            else:
                res[-1][1] = max(res[-1][1], item[1])

        return res
