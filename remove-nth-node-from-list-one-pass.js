/* time: O(L)
 * space: O(1)
 */
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function(head, n) {
  const virtualHead = new ListNode(0);
  virtualHead.next = head;

  let pre = virtualHead;
  let post = virtualHead;

  for (let i = 1; i <= n + 1; i++) {
    post = post.next;
  }

  while (post) {
    pre = pre.next;
    post = post.next;
  }

  let node = pre.next;
  pre.next = node.next;
  node = null;
  return virtualHead.next;
};
