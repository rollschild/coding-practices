class Solution:
    def traverse(self, s, wordDict, start, memo):
        if start == len(s):
            return True
        if start in memo:
            return memo[start]

        for word in wordDict:
            sub = s[start:]
            if sub.startswith(word) and self.traverse(
                s, wordDict, start + len(word), memo
            ):
                memo[start] = True
                return True

        memo[start] = False
        return False

    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        # recursion with memo
        return self.traverse(s, wordDict, 0, {})
