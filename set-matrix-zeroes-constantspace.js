/* Time: O(mn)
 * space: O(1)
 */
/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var setZeroes = function(matrix) {
  const height = matrix.length;
  if (height === 0) return;
  const width = matrix[0].length;

  let firstColZero = false;
  for (let row = 0; row < height; row++) {
    if (matrix[row][0] === 0) firstColZero = true;

    for (let col = 1; col < width; col++) {
      if (matrix[row][col] === 0) {
        matrix[row][0] = 0;
        matrix[0][col] = 0;
      }
    }
  }

  for (let row = 1; row < height; row++) {
    for (let col = 1; col < width; col++) {
      if (matrix[0][col] === 0 || matrix[row][0] === 0) {
        matrix[row][col] = 0;
      }
    }
  }

  if (matrix[0][0] === 0) {
    for (let c = 0; c < width; c++) {
      matrix[0][c] = 0;
    }
  }

  if (firstColZero) {
    for (let r = 0; r < height; r++) {
      matrix[r][0] = 0;
    }
  }
};
