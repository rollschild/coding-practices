/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

function Node(val) {
  this.val = val;
  this.next = null;
}

function addList(l1, l2, carry) {
  let node = null;
  let res = 0;
  if (l1 && l2) {
    res = l1.val + l2.val + carry;
    if (res > 9) {
      carry = 1;
      res -= 10;
    } else {
      carry = 0;
    }
    node = new Node(res);
    node.next = addList(l1.next, l2.next, carry);
  } else if (l1) {
    // l2 is null
    res = l1.val + carry;
    if (res > 9) {
      carry = 1;
      res -= 10;
    } else {
      carry = 0;
    }

    node = new Node(res);
    node.next = addList(l1.next, undefined, carry);
  } else if (l2) {
    // l1 is null
    res = l2.val + carry;
    if (res > 9) {
      carry = 1;
      res -= 10;
    } else {
      carry = 0;
    }
    node = new Node(res);
    node.next = addList(undefined, l2.next, carry);
  } else {
    if (carry > 0) {
      node = new Node(1);
      node.next = null;
      carry = 0;
    }
  }

  return node;
}
var addTwoNumbers = function(l1, l2) {
  return addList(l1, l2, 0);
};
