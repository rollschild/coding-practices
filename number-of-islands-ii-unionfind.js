/* Time: O(m*n+L), where L is length of positions
 * Space: O(m*n)
 */
/**
 * @param {number} m
 * @param {number} n
 * @param {number[][]} positions
 * @return {number[]}
 */

var numIslands2 = function(m, n, positions) {
  const unionFind = new UnionFind(m * n);
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];
  const res = [];

  for (const [row, col] of positions) {
    unionFind.setParent(row * n + col);
    for (const [r, c] of directions) {
      const newRow = row + r;
      const newCol = col + c;
      if (
        newRow >= 0 &&
        newRow < m &&
        newCol >= 0 &&
        newCol < n &&
        unionFind.isValid(newRow * n + newCol)
      ) {
        unionFind.union(row * n + col, newRow * n + newCol);
      }
    }
    res.push(unionFind.getCount());
  }

  return res;
};

class UnionFind {
  constructor(num) {
    this.parents = new Array(num).fill(-1);
    this.ranks = new Array(num).fill(0);
    this.count = 0;
  }

  find(x) {
    if (x !== this.parents[x]) this.parents[x] = this.find(this.parents[x]);
    return this.parents[x];
  }

  getCount() {
    return this.count;
  }

  setParent(index) {
    if (this.parents[index] >= 0) return;
    this.parents[index] = index;
    this.count++;
  }

  isValid(index) {
    return this.parents[index] >= 0;
  }

  union(x, y) {
    const rootX = this.find(x);
    const rootY = this.find(y);

    if (rootX !== rootY) {
      if (this.ranks[rootX] < this.ranks[rootY]) {
        this.parents[rootX] = rootY;
      } else if (this.ranks[rootX] > this.ranks[rootY]) {
        this.parents[rootY] = rootX;
      } else {
        this.parents[rootX] = rootY;

        this.ranks[rootY] += 1;
      }
      this.count--;
    }
  }
}

console.log(
  numIslands2(3, 3, [[0, 1], [1, 2], [2, 1], [1, 0], [0, 2], [0, 0], [1, 1]]),
);
