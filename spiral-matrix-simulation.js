/* Time: O(N)
 * Space: O(N)
 */
/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function(matrix) {
  const height = matrix.length;
  if (height === 0) return [];
  const width = matrix[0].length;
  const visited = new Set();
  const res = [];
  const directions = [[0, 1], [1, 0], [0, -1], [-1, 0]];
  let dir = 0;
  let row = 0;
  let col = 0;

  while (visited.size < height * width) {
    res.push(matrix[row][col]);
    visited.add(row * width + col);
    if (
      col + directions[dir % 4][1] >= width ||
      row + directions[dir % 4][0] >= height ||
      col + directions[dir % 4][1] < 0 ||
      row + directions[dir % 4][0] < 0 ||
      visited.has(
        (row + directions[dir % 4][0]) * width + col + directions[dir % 4][1],
      )
    ) {
      dir++;
    }

    row += directions[dir % 4][0];
    col += directions[dir % 4][1];
  }

  return res;
};
