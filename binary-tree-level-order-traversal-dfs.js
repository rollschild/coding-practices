/* Time: O(N)
 * space: O(N) */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrder = function(root) {
  // dfs
  if (!root) return [];
  const levels = [];
  dfs(root, 0, levels);
  return levels;
};

const dfs = (node, level, levels) => {
  if (!node) return;
  if (level === levels.length) {
    levels.push([]);
  }

  levels[level].push(node.val);
  dfs(node.left, level + 1, levels);
  dfs(node.right, level + 1, levels);
};
