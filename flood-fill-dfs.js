/**
 * @param {number[][]} image
 * @param {number} sr
 * @param {number} sc
 * @param {number} newColor
 * @return {number[][]}
 */
var floodFill = function(image, sr, sc, newColor) {
  const visited = {};
  const initialColor = image[sr][sc];
  if (initialColor === newColor) return image;
  dfs(image, sr, sc, initialColor, newColor, visited);
  return image;
};

const dfs = (image, row, col, initialColor, newColor, visited) => {
  const height = image.length;
  const width = image[0].length;
  if (visited[row * width + col]) return;

  image[row][col] = newColor;
  visited[row * width + col] = true;
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  for (const [r, c] of directions) {
    const newRow = row + r;
    const newCol = col + c;
    if (
      newRow >= 0 &&
      newRow < height &&
      newCol < width &&
      newCol >= 0 &&
      image[newRow][newCol] === initialColor
    ) {
      dfs(image, newRow, newCol, initialColor, newColor, visited);
    }
  }
};
