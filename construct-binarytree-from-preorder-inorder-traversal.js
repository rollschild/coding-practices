/* Time: O(N)
 * Space: O(N) */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} preorder
 * @param {number[]} inorder
 * @return {TreeNode}
 */
var buildTree = function(preorder, inorder) {
  if (preorder.length === 0 || inorder.length === 0) return null;
  const rootVal = preorder[0];
  const index = inorder.indexOf(rootVal);
  const rootNode = new TreeNode(rootVal);
  rootNode.left = buildTree(
    preorder.slice(1),
    index === 0 ? [] : inorder.slice(0, index),
  );
  rootNode.right = buildTree(
    index === preorder.length - 1 ? [] : preorder.slice(index + 1),
    inorder.slice(index + 1),
  );
  return rootNode;
};
