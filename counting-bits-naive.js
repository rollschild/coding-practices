/* Time: O(Nk), where N is the number, and k is number of bits in each integer
 * space: O(N) */
/**
 * @param {number} num
 * @return {number[]}
 */
var countBits = function(num) {
  if (num === 0) return [0];
  const res = new Array(num + 1);
  for (let i = 0; i <= num; i++) {
    res[i] = count(i);
  }
  return res;
};

const count = num => {
  let c = 0;
  while (num !== 0) {
    num &= num - 1;
    c++;
  }
  return c;
};
