/**
 * @param {string[]} words
 * @param {number} k
 * @return {string[]}
 */
var topKFrequent = function(words, k) {
    const len = words.length;

    const buckets = {};

    for (let word of words) {
        if (!buckets[word]) buckets[word] = 0;
        buckets[word]++;
    }

    let res = [];

    const freqs = new Array(len + 1);
    for (let i = 0; i < freqs.length; i++) {
        freqs[i] = [];
    }
    for (let key of Object.keys(buckets)) {
        freqs[buckets[key]].push(key);
    }

    for (let j = freqs.length - 1; j >= 1; j--) {
        const arr = freqs[j];
        arr.sort((a, b) => {
            if (a < b) return -1;
            else if (a > b) return 1;
            else return 0;
        });
        if (arr.length >= k) {
            res = [...res, ...arr.slice(0, k)];
            break;
        } else if (arr.length > 0) {
            res = [...res, ...arr.slice(0)];
            k -= arr.length;
        }
    }

    return res;
};
