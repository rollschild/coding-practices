/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {number[]}
 */
var findOrder = function(numCourses, prerequisites) {
  const graph = {};
  const incomingLinks = new Array(numCourses).fill(0);
  const res = [];
  // first contruct the graph
  for (let i = 0; i < prerequisites.length; i++) {
    const [course, pre] = prerequisites[i];
    if (graph[pre]) {
      graph[pre].push(course);
    } else {
      graph[pre] = [course];
    }
    incomingLinks[course] += 1;
  }

  const queue = [];
  for (let j = 0; j < incomingLinks.length; j++) {
    if (incomingLinks[j] == 0) {
      queue.push(j);
    }
  }

  while (queue.length > 0) {
    const key = queue.shift();
    res.push(key);
    if (graph[key]) {
      for (let value of graph[key]) {
        incomingLinks[value]--;
        if (incomingLinks[value] === 0) {
          queue.push(value);
        }
      }
    }
  }

  return res.length === numCourses ? res : [];
};

console.log(findOrder(2, [[1, 0]]));
