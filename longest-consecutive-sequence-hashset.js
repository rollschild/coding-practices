/* Time: O(N)
 * space: O(N) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var longestConsecutive = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;

  const numsSet = new Set([...nums]);
  let globalMax = 0;
  for (const num of numsSet) {
    if (numsSet.has(num - 1)) continue;
    let temp = num;
    let currMax = 1;
    while (numsSet.has(temp + 1)) {
      currMax++;
      temp++;
    }
    globalMax = Math.max(globalMax, currMax);
  }

  return globalMax;
};
