/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
function Heap() {
  this.items = [];
  // this.priority = 0;
}
function Node(val, priority) {
  this.val = val;
  this.priority = priority;
}

Heap.prototype = {
  push: function(element, priority) {
    const node = new Node(element, priority);
    for (let i = 0; i < this.items.length; i++) {
      if (node.priority > this.items[i].priority) {
        this.items.splice(i, 0, node);
        return;
      }
    }
    this.items.push(node);
    /* this.items.push(node);
     * this.bubbleUp(this.items.length - 1); */
  },

  bubbleUp: function(index) {
    while (index > 0) {
      let parentIndex = Math.floor((index - 1) / 2);
      const parentNode = this.items[parentIndex];
      if (parentNode.priority >= this.items[index].priority) {
        break;
      }
      this.items[parentIndex] = this.items[index];
      this.items[index] = parentNode;
      index = parentIndex;
    }
  },
};

var topKFrequent = function(nums, k) {
  const obj = {};
  for (let item of nums) {
    obj[item] = (obj[item] || 0) + 1;
  }
  console.log("obj:", obj);

  const heap = new Heap();
  for (let key in obj) {
    const freq = obj[key];
    heap.push(key, freq);
  }

  const res = [];
  for (let i = 0; i < k; i++) {
    res.push(heap.items[i].val);
  }
  console.log("heap:", heap);

  return res;
};

console.log(topKFrequent([2, 3, 4, 1, 4, 0, 4, -1, -2, -1], 2));
console.log(topKFrequent([-1, 1, 4, -4, 3, 5, 4, -2, 3, -1], 3));
