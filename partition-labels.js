/**
 * @param {string} S
 * @return {number[]}
 */
var partitionLabels = function(S) {
  const freqs = new Array(26).fill(0);
  for (let i = 0; i < S.length; i++) {
    freqs[S.charCodeAt(i) - "a".charCodeAt(0)]++;
  }
  const tempFreqs = new Array(26).fill(0);
  let head = 0;
  let tail = head;
  const res = [];
  while (head <= tail && tail < S.length) {
    const pos = S.charCodeAt(tail) - "a".charCodeAt(0);
    tempFreqs[pos]++;
    let full = true;
    for (let j = 0; j <= tail; j++) {
      const letterPos = S.charCodeAt(j) - "a".charCodeAt(0);
      if (tempFreqs[letterPos] === freqs[letterPos]) {
        continue;
      } else {
        full = false;
        break;
      }
    }

    if (full) {
      res.push(tail - head + 1);
      head = tail + 1;
      tail = head;
    } else {
      tail++;
    }
  }

  return res;
};
