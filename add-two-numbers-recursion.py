# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def addList(self, l1, l2, carry):
        node = None
        res = 0
        if l1 and l2:
            res = l1.val + l2.val + carry
            if res > 9:
                res -= 10
                carry = 1
            else:
                carry = 0

            node = ListNode(res)
            node.next = self.addList(l1.next, l2.next, carry)
        elif l1:
            res = l1.val + carry
            if res > 9:
                res -= 10
                carry = 1
            else:
                carry = 0

            node = ListNode(res)
            node.next = self.addList(l1.next, None, carry)
        elif l2:
            res = l2.val + carry
            if res > 9:
                res -= 10
                carry = 1
            else:
                carry = 0

            node = ListNode(res)
            node.next = self.addList(None, l2.next, carry)
        else:
            if carry > 0:
                node = ListNode(1)
                carry = 0
                node.next = None

        return node

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        return self.addList(l1, l2, 0)
