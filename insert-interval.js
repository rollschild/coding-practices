/* Time: O(N)
 * space: O(1)
 */
/**
 * @param {number[][]} intervals
 * @param {number[]} newInterval
 * @return {number[][]}
 */
var insert = function(intervals, newInterval) {
  const len = intervals.length;
  if (len === 0) return [newInterval];
  const res = [];
  let inserted = false;

  for (let i = 0; i < len; i++) {
    if (inserted) newInterval = res.pop();
    const interval = intervals[i];
    if (interval[1] < newInterval[0]) {
      res.push(interval);
    } else {
      if (interval[0] > newInterval[1]) {
        res.push(newInterval);
        res.push(interval);
      } else {
        res.push([
          Math.min(interval[0], newInterval[0]),
          Math.max(interval[1], newInterval[1]),
        ]);
      }
      inserted = true;
    }
  }

  if (!inserted) res.push(newInterval);
  return res;
};
