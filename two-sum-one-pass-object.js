/* time and space both O(n) */
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
  if (nums.length === 2) {
    return [0, 1];
  }

  let obj = {};
  for (let i = 0; i < nums.length; i++) {
    const goal = target - nums[i];
    if (obj[goal] != undefined) {
      return [obj[goal], i];
    } else {
      obj[nums[i]] = i;
    }
  }
};
