/**
 * @param {string[]} logs
 * @return {string[]}
 */
var reorderLogFiles = function(logs) {
  const digitArr = [];
  const letterArr = [];

  for (let log of logs) {
    const arr = log.split(" ");
    if (isNaN(arr[1])) {
      // letter
      letterArr.push(log);
    } else {
      digitArr.push(log);
    }
  }

  // Sort letterArr
  letterArr.sort((str1, str2) => {
    const firstLetterPos1 = str1.indexOf(" ") + 1;
    const firstLetterPos2 = str2.indexOf(" ") + 1;
    const letter1 = str1.slice(firstLetterPos1);
    const identifier1 = str1.slice(0, str1.indexOf(" "));
    const letter2 = str2.slice(firstLetterPos2);
    const identifier2 = str2.slice(0, str2.indexOf(" "));
    console.log(identifier1, identifier2);

    if (letter1 < letter2) {
      return -1;
    } else if (letter1 > letter2) {
      return 1;
    } else {
      if (identifier1 < identifier2) {
        return -1;
      } else {
        return 1;
      }
    }
  });

  return [...letterArr, ...digitArr];
};
