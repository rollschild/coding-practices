/* Time O(n^2)
 * Space O(n)
 */
/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */
var wordBreak = function(s, wordDict) {
  return traverse(s, wordDict, 0, {});
};

const traverse = (str, wordDict, start, memo) => {
  if (start === str.length) return true;
  if (memo[start] != undefined) return memo[start];

  for (let word of wordDict) {
    const substr = str.slice(start);
    if (
      substr.startsWith(word) &&
      traverse(str, wordDict, start + word.length, memo)
    ) {
      memo[start] = true;
      return true;
    }
  }
  memo[start] = false;
  return false;
};
