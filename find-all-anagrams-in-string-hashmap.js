/* Time: O(Np + Ns)
 * Space: O(1)
 */
/**
 * @param {string} s
 * @param {string} p
 * @return {number[]}
 */

var findAnagrams = function(s, p) {
  const sLen = s.length;
  const pLen = p.length;
  if (pLen > sLen) return [];

  const res = [];
  const pDict = {};
  for (const letter of p) {
    pDict[letter] = (pDict[letter] || 0) + 1;
  }

  const dict = {};

  for (let i = 0; i < sLen; i++) {
    dict[s[i]] = (dict[s[i]] || 0) + 1;
    if (i >= pLen) {
      // remove the first letter in the window from dict
      const pos = i - pLen;
      if (dict[s[pos]] === 1) {
        delete dict[s[pos]];
      } else {
        dict[s[pos]]--;
      }
    }
    if (compareObjects(pDict, dict)) res.push(i - pLen + 1);
  }

  return res;
};

const compareObjects = (obj1, obj2) => {
  if (Object.keys(obj1).length !== Object.keys(obj2).length) return false;

  for (const [key1, value1] of Object.entries(obj1)) {
    if (obj2[key1] !== value1) return false;
  }

  return true;
};
