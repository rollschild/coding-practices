/* Time: O(NlogN)
 * space: O(1) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var longestConsecutive = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;
  nums.sort((a, b) => a - b);
  let maxLen = 1;
  let currLen = 1;
  for (let i = 1; i < len; i++) {
    if (nums[i] === nums[i - 1]) continue;
    if (nums[i] - nums[i - 1] !== 1) {
      maxLen = Math.max(currLen, maxLen);
      currLen = 1;
    } else {
      currLen++;
    }
  }
  maxLen = Math.max(currLen, maxLen);

  return maxLen;
};
