/**
 * @param {number} n
 * @return {string[]}
 */
var generateParenthesis = function(n) {
  const arr = [];
  const res = [];
  newParenthesis("", 0, arr, n);
  for (let paren of arr) {
    const stack = [];
    for (let i = 0; i < paren.length; i++) {
      if (paren[i] === "(") {
        stack.push("(");
      } else {
        if (stack.length > 0 && stack[stack.length - 1] === "(") {
          stack.pop();
        } else {
          stack.push(")");
        }
      }
    }
    if (stack.length === 0) res.push(paren);
  }

  return res;
};

const newParenthesis = (str, num, arr, limit) => {
  if (num === limit * 2) {
    arr.push(str);
    return;
  }
  newParenthesis(str + "(", num + 1, arr, limit);
  newParenthesis(str + ")", num + 1, arr, limit);
};
