/**
 * @param {string} beginWord
 * @param {string} endWord
 * @param {string[]} wordList
 * @return {number}
 */
var ladderLength = function(beginWord, endWord, wordList) {
  wordList = [...new Set(wordList)];
  if (!wordList.includes(endWord)) return 0;
  let layers = {};
  layers[beginWord] = [[beginWord]];
  const alphabet = "abcdefghijklmnopqrstuvwxyz";
  const res = [];
  let minLevel = wordList.length;
  // BFS
  while (layers && Object.keys(layers).length > 0) {
    const newLayers = {};
    for (let currWord of Object.keys(layers)) {
      console.log("currWord", currWord);
      if (currWord === endWord) {
        for (let finalPath of layers[currWord]) {
          res.push([...finalPath]);
        }

        continue;
      }
      for (let j = 0; j < currWord.length; j++) {
        for (let k = 0; k < 26; k++) {
          const newWord =
            currWord.slice(0, j) + alphabet[k] + currWord.slice(j + 1);
          if (wordList.includes(newWord)) {
            if (!newLayers[newWord]) newLayers[newWord] = [];
            for (let path of layers[currWord]) {
              newLayers[newWord].push([...path, newWord]);
            }
          }
        }
      }
    }
    for (let key of [...new Set(Object.keys(newLayers))]) {
      wordList.splice(wordList.indexOf(key), 1);
    }

    layers = newLayers;
  }

  for (let resPath of res) {
    console.log("resPath", resPath);
    if (resPath.length < minLevel) {
      minLevel = resPath.length;
    }
  }

  return minLevel;
};

console.log(
  ladderLength("hit", "cog", ["hot", "dot", "dog", "lot", "log", "cog"]),
);
