/**
 * @param {number[][]} rooms
 * @return {void} Do not return anything, modify rooms in-place instead.
 */
var wallsAndGates = function(rooms) {
  if (rooms.length === 0) return [];
  const h = rooms.length;
  const w = rooms[0].length;
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  // ideally [position]: minDistance
  const visited = {};
  const queue = [];

  for (let i = 0; i < h; i++) {
    for (let j = 0; j < w; j++) {
      if (rooms[i][j] === 0) queue.push([i, j]);
    }
  }

  while (queue && queue.length > 0) {
    const [row, col] = queue.shift();
    for (direction of directions) {
      const r = row + direction[0];
      const c = col + direction[1];
      if (r >= 0 && r < h && c >= 0 && c < w) {
        if (rooms[r][c] === 2147483647) {
          // room
          rooms[r][c] = rooms[row][col] + 1;
          queue.push([r, c]);
        }
      }
    }
  }

  return;
};
