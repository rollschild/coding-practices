/**
 * @param {string} s
 * @return {number}
 */
const decode = (index, s, memo) => {
  if (index === s.length) return 1;
  if (s[index] === "0") return 0;
  if (index === s.length - 1) return 1;
  if (memo[index]) return memo[index];
  let res = decode(index + 1, s, memo);
  const num = parseInt(s.substring(index, index + 2), 10);
  if (num >= 0 && num <= 26) {
    res += decode(index + 2, s, memo);
  }
  memo[index] = res;

  return res;
};

var numDecodings = function(s) {
  if (s.length === 0 || s[0] === "0") return 0;
  const memo = {};
  return decode(0, s, memo);
};
