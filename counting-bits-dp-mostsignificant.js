Time: O(N);
space: O(N);
/**
 * @param {number} num
 * @return {number[]}
 */
var countBits = function(num) {
  if (num === 0) return [0];
  const dp = new Array(num + 1).fill(0);

  let i = 0;
  let goal = 1;
  while (goal <= num) {
    while (i < goal && i + goal <= num) {
      dp[goal + i] = dp[i] + 1;
      i++;
    }
    i = 0;
    goal *= 2;
  }

  return dp;
};
