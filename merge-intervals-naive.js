/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
const mergeArray = (arr, res) => {
  if (arr.length >= 2) {
    if (arr[0][1] >= arr[1][0]) {
      // can merge
      const left = arr[0][0];
      const right = arr[0][1] < arr[1][1] ? arr[1][1] : arr[0][1];
      const newInterval = [left, right];
      return mergeArray([newInterval, ...arr.slice(2)], res);
    } else {
      res.push(arr[0]);
      return mergeArray(arr.slice(1), res);
    }
  } else if (arr.length === 1) {
    res.push(arr[0]);
    return res;
  }
  return res;
};

var merge = function(intervals) {
  intervals.sort((a, b) => a[0] - b[0]);
  let res = [];
  return mergeArray(intervals, res);
};
