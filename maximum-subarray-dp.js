/* Time: O(N)
 * space: O(1)
 */
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
  if (nums.length === 0) return 0;
  const dp = new Array(nums.length).fill(0);
  let maxSum = nums[0];
  for (let i = 1; i < nums.length; i++) {
    nums[i] = nums[i - 1] >= 0 ? nums[i - 1] + nums[i] : nums[i];
    maxSum = Math.max(maxSum, nums[i]);
  }

  return maxSum;
};
