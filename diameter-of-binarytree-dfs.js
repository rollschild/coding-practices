/* Time and space: O(N)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * @param {TreeNode} root
 * @return {number}
 */

var diameterOfBinaryTree = function(root) {
  const dfs = node => {
    if (!node) return 0;
    const left = dfs(node.left);
    const right = dfs(node.right);
    this.currMax = Math.max(this.currMax, left + right);
    return Math.max(left, right) + 1;
  };

  this.currMax = 0;

  dfs(root);

  return this.currMax;
};
