/** time O(n); space O(logn) but O(n) in worst case */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} sum
 * @return {boolean}
 */
var hasPathSum = function(root, sum) {
  if (!root) return false;
  const sumArr = traverse(root, 0, []);
  return sumArr.indexOf(sum) >= 0;
};

const traverse = (node, currSum, sumArr) => {
  if (!node) return sumArr;

  currSum += node.val;
  if (!node.left && !node.right) {
    sumArr.push(currSum);
  } else {
    sumArr = traverse(node.left, currSum, sumArr);
    sumArr = traverse(node.right, currSum, sumArr);
  }

  return sumArr;
};
