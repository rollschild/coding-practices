/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */

var maxSlidingWindow = function(nums, k) {
  const len = nums.length;
  const res = [];
  const window = nums.slice(0, k);

  let maxNum = Math.max(...window);
  res.push(maxNum);
  let right = 1 + k - 1;

  while (right < len) {
    const readyToRemove = window.shift();
    const newItem = nums[right];
    window.push(newItem);

    if (readyToRemove === maxNum) {
      maxNum = Math.max(...window);
    } else {
      if (maxNum < newItem) {
        maxNum = newItem;
      }
    }
    res.push(maxNum);
    right++;
  }

  return res;
};
