/**
 * @param {string} s
 * @return {number}
 */
var calculate = function(s) {
  console.log("s:", s);
  const len = s.length;
  if (len === 0) return 0;
  if (!isNaN(s)) return Number.parseInt(s);

  const operators = ["+", "-", "*", "/"];
  let head = 0;
  let tail = 1;
  while (head < tail && tail < len) {
    if (operators.includes(s[tail])) {
      // operator
      const lhs = Number.parseInt(s.slice(head, tail));
      let rhs;
      if (s[tail] === "*" || s[tail] === "/") {
        let newHead = tail + 1;
        let newTail = tail + 2;
        while (newTail <= len) {
          if (newHead === len - 1) {
            rhs = Number.parseInt(s[newHead]);
            break;
          }
          if (operators.includes(s[newTail]) || newTail === len - 1) {
            rhs = Number.parseInt(s.slice(newHead, newTail));
            break;
          }
          newTail++;
        }
        const result =
          s[tail] === "*"
            ? (lhs * rhs).toString()
            : Math.floor(lhs / rhs).toString();
        const newString = s.slice(0, head) + result + s.slice(newTail);
        return calculate(newString);
      } else {
        // + or -
        return s[tail] === "+"
          ? calculate(s.slice(0, tail)) + calculate(s.slice(tail + 1))
          : calculate(s.slice(0, tail)) - calculate(s.slice(tail + 1));
      }
    }
    tail++;
  }
};

console.log(calculate("1-1+1"));
