/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */

var validTree = function(n, edges) {
  // Union Find
  if (edges.length !== n - 1) return false;
  const unionFind = new UnionFind(n);
  for (const [node1, node2] of edges) {
    const res = unionFind.union(node1, node2);
    if (!res) {
      return false;
    }
  }

  return true;
};

class UnionFind {
  constructor(num) {
    this.parents = new Array(num);
    for (let i = 0; i < num; i++) {
      this.parents[i] = i;
    }
  }

  find(node) {
    while (node !== this.parents[node]) {
      node = this.parents[node];
    }
    return node;
  }

  union(node1, node2) {
    const parent1 = this.find(node1);
    const parent2 = this.find(node2);

    if (parent1 === parent2) {
      return false;
    }
    this.parents[parent1] = parent2;
    return true;
  }
}
