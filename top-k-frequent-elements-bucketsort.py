from typing import List


class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        freqs = [[] for _ in range(0, len(nums) + 1)]
        freqMap = {}
        for x in nums:
            freqMap[x] = (freqMap[x] if x in freqMap else 0) + 1

        for key, freq in freqMap.items():
            freqs[freq].append(key)
        print(freqs)

        counter = 0
        res = []
        for freq_list in reversed(freqs):
            print(freq_list)
            if freq_list and len(freq_list) > 0 and counter < k:
                if len(freq_list) > k:
                    res = [*res, *freq_list[:k]]
                    counter += k
                else:
                    res = [*res, *freq_list[:]]
                    counter += len(freq_list)

        return res


sol = Solution()
print(sol.topKFrequent([1, 1, 1, 2, 2, 3], 2))
