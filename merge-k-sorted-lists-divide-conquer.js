/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
const mergeTwoLists = (l1, l2) => {
  if (!l1) {
    return l2;
  }
  if (!l2) {
    return l1;
  }

  if (l1.val < l2.val) {
    l1.next = mergeTwoLists(l1.next, l2);
    return l1;
  } else {
    l2.next = mergeTwoLists(l1, l2.next);
    return l2;
  }
};

var mergeKLists = function(lists) {
  if (lists.length > 2) {
    let mid = Math.floor(lists.length / 2);
    return mergeTwoLists(
      mergeKLists(lists.slice(0, mid)),
      mergeKLists(lists.slice(mid, lists.length)),
    );
  } else if (lists.length === 2) {
    return mergeTwoLists(lists[0], lists[1]);
  } else if (lists.length === 1) {
    return lists[0];
  }
  return null;
};
