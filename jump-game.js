/**
 * @param {number[]} nums
 * @return {boolean}
 */

var canJump = function(nums) {
  const len = nums.length;

  if (len <= 1) return true;

  let maxPos = nums[0];

  for (let i = 1; i < len; i++) {
    if (maxPos < i) return false;
    maxPos = Math.max(maxPos, i + nums[i]);
  }

  return true;
};
