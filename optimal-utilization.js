const optimalUtilization = (arr1, arr2, target) => {
  arr1.sort((a, b) => a[1] - b[1]);
  arr2.sort((a, b) => a[1] - b[1]);
  let right1 = arr1.length - 1;
  let right2 = arr2.length - 1;
  let maxSum = 0;
  let res = [];

  for (let i = right1; i >= 0; i--) {
    for (let j = right2; j >= 0; j--) {
      const sum = arr1[i][1] + arr2[j][1];
      if (sum > target) continue;
      if (maxSum === 0) maxSum = sum;
      if (sum > maxSum) {
        maxSum = sum;
        res = [];
        res.push([arr1[i][0], arr2[j][0]]);
      } else if (sum === maxSum) {
        res.push([arr1[i][0], arr2[j][0]]);
      }
    }
  }

  return res;
};

console.log(
  optimalUtilization([[1, 8], [2, 15], [3, 9]], [[1, 8], [2, 11], [3, 12]], 20),
);
