class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> List[str]:
        res = self.traverse(s, wordDict, 0, {})

        for i in range(0, len(res)):
            res[i] = " ".join(res[i])

        return res

    def traverse(self, s, wordDict, pos, memo):
        if pos in memo:
            return memo[pos]

        res = []

        if pos == len(s):
            res.append([])

        for word in wordDict:
            substring = s[pos:]
            if substring.startswith(word):
                arr = self.traverse(s, wordDict, pos + len(word), memo)
                for l in arr:
                    res.append([word, *l])

        memo[pos] = res
        return res
