/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var mergeTwoLists = function(l1, l2) {
  let virtualHead = new ListNode(0);
  let node = virtualHead;

  let currOne = l1;
  let currTwo = l2;

  while (currOne && currTwo) {
    if (currOne.val <= currTwo.val) {
      if (!currOne.next) {
        node.next = new ListNode(currOne.val);
        node.next.next = new ListNode(currTwo.val);
        currOne = null;
        currTwo = currTwo.next;
      } else if (currTwo.val <= currOne.next.val) {
        node.next = new ListNode(currOne.val);
        node.next.next = new ListNode(currTwo.val);
        currOne = currOne.next;
        currTwo = currTwo.next;
      } else if (currTwo.val > currOne.next.val) {
        node.next = new ListNode(currOne.val);
        node.next.next = new ListNode(currOne.next.val);
        currOne = currOne.next.next;
      }
    } else {
      if (!currTwo.next) {
        node.next = new ListNode(currTwo.val);
        node.next.next = new ListNode(currOne.val);
        currTwo = null;
        currOne = currOne.next;
      } else if (currOne.val <= currTwo.next.val) {
        node.next = new ListNode(currTwo.val);
        node.next.next = new ListNode(currOne.val);
        currOne = currOne.next;
        currTwo = currTwo.next;
      } else if (currOne.val > currTwo.next.val) {
        node.next = new ListNode(currTwo.val);
        node.next.next = new ListNode(currTwo.next.val);
        currTwo = currTwo.next.next;
      }
    }
    node = node.next.next;
  }

  while (currOne) {
    node.next = new ListNode(currOne.val);
    node = node.next;
    currOne = currOne.next;
  }
  while (currTwo) {
    node.next = new ListNode(currTwo.val);
    currTwo = currTwo.next;
    node = node.next;
  }

  return virtualHead.next;
};
