from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if not strs:
            return ""
        if len(strs) == 1:
            return strs[0]

        min_len = len(strs[0])
        for str in strs:
            if len(str) < min_len:
                min_len = len(str)

        head = 0
        tail = min_len

        while head < tail:
            common = strs[0][:tail]
            for i, item in enumerate(strs):
                if strs[i][:tail] != common:
                    break
                if i == len(strs) - 1:
                    return common

            tail -= 1

        return ""
