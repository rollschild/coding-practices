/* Time: O(logN)
 * space: O(1) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var findMin = function(nums) {
  // binary search
  const len = nums.length;
  if (len === 0) return 0;

  let left = 0;
  let right = len - 1;
  if (nums[right] >= nums[left]) return nums[left];
  while (left >= 0 && right < len) {
    if (left === right) return nums[right];
    const mid = Math.floor((left + right) / 2);
    const midNum = nums[mid];
    if (midNum >= nums[left]) {
      left++;
    } else {
      right--;
    }
  }
};
