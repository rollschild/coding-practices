/* Time: O(N)
 * Space: O(N)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */

var lowestCommonAncestor = function(root, p, q) {
  this.resNode = null;

  const traverse = function(node, p, q) {
    if (!node) return false;
    const left = traverse(node.left, p, q) ? 1 : 0;
    const right = traverse(node.right, p, q) ? 1 : 0;
    const foundNode = node.val === p.val || node.val === q.val ? 1 : 0;

    if (left + right + foundNode >= 2) {
      this.resNode = node;
    }

    return foundNode || left || right;
  };

  traverse(root, p, q);
  return this.resNode;
};
