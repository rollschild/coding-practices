/* Time: O(N)
 * space: O(N) */
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
  const len = prices.length;
  if (len === 0) return 0;
  let minPrice = prices[0];
  const dp = new Array(prices.length).fill(0);
  dp[0] = 0;
  for (let i = 1; i < prices.length; i++) {
    minPrice = Math.min(minPrice, prices[i - 1]);
    dp[i] = Math.max(prices[i] - minPrice, dp[i - 1]);
  }
  return dp[len - 1];
};
