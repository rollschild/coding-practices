/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;
  if (len === 1) return nums[0];
  if (len === 2) return Math.max(...nums);

  // count the first
  let prev = nums[0];
  let curr1 = Math.max(prev, nums[1]);
  for (let i = 2; i < len - 1; i++) {
    const temp = curr1;
    curr1 = Math.max(prev + nums[i], curr1);
    prev = temp;
  }

  prev = nums[1];
  let curr2 = Math.max(prev, nums[2]);
  for (let i = 3; i < len; i++) {
    const temp = curr2;
    curr2 = Math.max(prev + nums[i], curr2);
    prev = temp;
  }

  return Math.max(curr1, curr2);
};
