/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function(head) {
  // find middle
  let slow = head;
  let fast = head;

  while (fast && fast.next) {
    slow = slow.next;
    fast = fast.next.next;
  }

  // now slow is middle
  // reverse
  let prev = null;
  let curr = slow;
  while (curr) {
    const temp = curr.next;
    curr.next = prev;
    prev = curr;
    curr = temp;
  }

  // now merge
  let first = head;
  let second = prev;
  while (second && second.next) {
    const temp = first.next;
    first.next = second;
    first = temp;
    const secondTemp = second.next;
    second.next = first;
    second = secondTemp;
  }
};
