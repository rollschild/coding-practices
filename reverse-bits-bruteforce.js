/* Time: O(1)
 * space: O(1) */
/**
 * @param {number} n - a positive integer
 * @return {number} - a positive integer
 */
var reverseBits = function(n) {
  let bits = n.toString(2).split("");
  if (bits.length < 32) {
    bits = [...new Array(32 - bits.length).fill("0"), ...bits];
  }
  for (let i = 0; i <= Math.floor((bits.length - 1) / 2); i++) {
    const temp = bits[i];
    bits[i] = bits[bits.length - 1 - i];
    bits[bits.length - i - 1] = temp;
  }
  return parseInt(bits.join(""), 2);
};
