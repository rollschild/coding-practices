/**
 * @param {number[][]} maze
 * @param {number[]} start
 * @param {number[]} destination
 * @return {number}
 */

var shortestDistance = function(maze, start, destination) {
  // bfs
  if (maze.length === 0) return -1;
  const height = maze.length;
  const width = maze[0].length;
  const arr = new Array(height);
  for (let i = 0; i < height; i++) {
    arr[i] = new Array(width).fill(Infinity);
  }
  arr[start[0]][start[1]] = 0;

  const queue = [];
  queue.push(start);
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  while (queue.length > 0) {
    const [row, col] = queue.shift();
    for (const [r, c] of directions) {
      let tempRow = row;
      let tempCol = col;
      let count = 0;
      while (
        tempRow + r >= 0 &&
        tempRow + r < height &&
        tempCol + c >= 0 &&
        tempCol + c < width &&
        maze[tempRow + r][tempCol + c] === 0
      ) {
        tempRow += r;
        tempCol += c;
        count++;
      }

      if (arr[row][col] + count < arr[tempRow][tempCol]) {
        queue.push([tempRow, tempCol]);
        arr[tempRow][tempCol] = arr[row][col] + count;
      }
    }
  }

  return arr[destination[0]][destination[1]] === Infinity
    ? -1
    : arr[destination[0]][destination[1]];
};
