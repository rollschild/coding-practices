# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Codec:
    def serialize(self, root):

        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """

        if root is None:
            return "[None]"
        arr = []

        def dfs(node, arr):
            if node is None:
                arr.append("None")
                return
            arr.append(str(node.val))
            dfs(node.left, arr)
            dfs(node.right, arr)
            return

        dfs(root, arr)
        return "[" + ",".join(arr) + "]"

    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """

        if len(data) <= 2:
            return None
        arr = data[1 : (len(data) - 1)].split(",")
        if arr[0] == "None":
            return None

        def rdfs(arr):
            val = arr.pop(0)
            if val == "None":
                return None
            node = TreeNode(int(val))
            node.left = rdfs(arr)
            node.right = rdfs(arr)
            return node

        return rdfs(arr)


# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))
