/**
 * // Definition for a Node.
 * function Node(val, next, random) {
 *    this.val = val;
 *    this.next = next;
 *    this.random = random;
 * };
 */
/**
 * @param {Node} head
 * @return {Node}
 */
var copyRandomList = function(head) {
  if (!head) return null;
  let oldHead = head;
  const obj = new WeakMap();
  const newHead = new Node(head.val, null, null);
  obj.set(head, newHead);
  let node = obj.get(head);

  while (oldHead) {
    // console.log('head.random:', head.random);
    if (oldHead.next) {
      if (obj.has(oldHead.next)) {
        node.next = obj.get(oldHead.next);
      } else {
        obj.set(oldHead.next, new Node(oldHead.next.val, null, null));
        node.next = obj.get(oldHead.next);
      }
    } else {
      node.next = null;
    }

    if (oldHead.random) {
      if (obj.has(oldHead.random)) {
        node.random = obj.get(oldHead.random);
      } else {
        obj.set(oldHead.random, new Node(oldHead.random.val, null, null));
        node.random = obj.get(oldHead.random);
      }
    } else {
      node.random = null;
    }
    node = node.next;
    oldHead = oldHead.next;
  }

  return obj.get(head);
};
