/* Time: O(mn)
 * Space: O(mn)
 */
/**
 * @param {character[][]} matrix
 * @return {number}
 */

var maximalSquare = function(matrix) {
  if (matrix.length === 0) return 0;
  const row = matrix.length;
  const col = matrix[0].length;
  const dp = new Array(row);

  for (let i = 0; i < row; i++) {
    dp[i] = new Array(col).fill(0);
  }

  let maxSideLen = 0;

  for (let r = 0; r < row; r++) {
    for (let c = 0; c < col; c++) {
      if (matrix[r][c] === "1") {
        if (r === 0 || c === 0) {
          dp[r][c] = 1;
        } else {
          dp[r][c] = Math.min(dp[r - 1][c], dp[r][c - 1], dp[r - 1][c - 1]) + 1;
        }

        maxSideLen = Math.max(maxSideLen, dp[r][c]);
      }
    }
  }

  return maxSideLen * maxSideLen;
};
