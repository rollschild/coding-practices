/* Time: O(n^2)
 * space: O(nlogn) - for sorting
 */
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var threeSum = function(nums) {
  if (nums.length < 3) return [];
  const len = nums.length;
  const res = [];
  nums.sort((a, b) => a - b);
  console.log("nums:", nums);
  for (let i = 0; i < len; i++) {
    if (nums[i] > 0) break;
    if (i > 0 && nums[i - 1] === nums[i]) continue;
    threeSumSearch(nums, i, res);
  }

  return res;
};

const threeSumSearch = (nums, pos, res) => {
  const goal = 0 - nums[pos];
  let lo = pos + 1;
  let hi = nums.length - 1;

  while (lo < hi) {
    const sum = nums[lo] + nums[hi];
    if (sum < goal || (lo > pos + 1 && nums[lo - 1] + nums[hi] === sum)) {
      lo++;
    } else if (
      sum > goal ||
      (hi < nums.length - 1 && nums[hi + 1] + nums[lo] === sum)
    ) {
      hi--;
    } else {
      res.push([nums[lo], nums[hi], nums[pos]]);
      lo++;
      hi--;
    }
  }
};
