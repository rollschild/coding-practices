/* Time and space O(n^2)
 */
/**
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function(s) {
  if (s.length === 0) return "";
  if (s.length === 1) return s;
  const len = s.length;
  const matrix = new Array(len);
  for (let i = 0; i < len; i++) {
    matrix[i] = new Array(len).fill(false);
    matrix[i][i] = true;
    if (i < len - 1) matrix[i][i + 1] = s[i] === s[i + 1];
  }
  let maxStr = "";

  for (let i = len - 1; i >= 0; i--) {
    for (let j = i; j < len; j++) {
      if (j > i + 1) matrix[i][j] = matrix[i + 1][j - 1] && s[i] === s[j];
      if (matrix[i][j]) {
        if (j - i + 1 > maxStr.length) {
          maxStr = s.substring(i, j + 1);
        }
      }
    }
  }

  return maxStr;
};
