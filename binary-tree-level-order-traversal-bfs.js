/* Time: O(N)
 * space: O(N) */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrder = function(root) {
  if (!root) return [];
  const queue = [root];
  const res = [];

  while (queue.length > 0) {
    const len = queue.length;
    const arr = [];
    for (let i = 0; i < len; i++) {
      const node = queue.shift();
      if (node) {
        arr.push(node.val);
        queue.push(node.left, node.right);
      }
    }
    if (arr.length > 0) res.push(arr);
  }

  return res;
};
