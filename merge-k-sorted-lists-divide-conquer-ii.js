/* Time: O(NlogK) - K is length of the list and N is the number of nodes of two
 * lists
 * Space: O(1) if both use iterative approach
 *** O(NK) if using recursion
 */
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
var mergeKLists = function(lists) {
  const len = lists.length;
  if (len === 0) return null;
  if (len === 1) return lists[0];
  if (len === 2) return mergeTwoLists(lists[0], lists[1]);

  let mid = Math.floor((len - 1) / 2);
  return mergeTwoLists(
    mergeKLists(lists.slice(0, mid + 1)),
    mergeKLists(lists.slice(mid + 1, len)),
  );
};

const mergeTwoLists = (l1, l2) => {
  if (!l1) return l2;
  if (!l2) return l1;

  if (l1.val < l2.val) {
    l1.next = mergeTwoLists(l1.next, l2);
    return l1;
  } else {
    l2.next = mergeTwoLists(l1, l2.next);
    return l2;
  }
};
