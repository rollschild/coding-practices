/**
 * @param {string} s1
 * @param {string} s2
 * @param {string} s3
 * @return {boolean}
 */
var isInterleave = function(s1, s2, s3) {
  if (s1.length === 0) return s2 === s3;
  if (s2.length === 0) return s1 === s3;
  if (s3.length === 0) return false;
  if (s3.length !== s1.length + s2.length) return false;

  const s1len = s1.length;
  const s2len = s2.length;
  const dp = new Array(s1len + 1);
  for (let m = 0; m <= s1len; m++) {
    dp[m] = new Array(s2len + 1).fill(true);
  }

  /* for (let i = 0; i < s1len; i++) {
   *   for (let j = 0; j < s2len; j++) {
   *     console.log("i, j", i, j);
   *     if (i === 0 && j === 0) {
   *       dp[i][j] = s3[0] === s1[0] || s3[0] === s2[0];
   *     } else if (i === 0) {
   *       dp[i][j] = dp[i][j - 1] && s3[i + j] === s2[j];
   *     } else if (j === 0) {
   *       dp[i][j] = dp[i - 1][j] && s3[i + j] === s1[i];
   *     } else {
   *       dp[i][j] =
   *         (dp[i - 1][j] && s3[i + j] === s1[i - 1]) ||
   *         (dp[i][j - 1] && s3[j + i] === s2[j - 1]);
   *       console.log("dp[i][j]", dp[i][j]);
   *     }
   *   }
   * } */
  for (let i = 0; i <= s1len; i++) {
    for (let j = 0; j <= s2len; j++) {
      if (j === 0 && i === 0) {
        dp[i][j] = true;
      } else if (i === 0) {
        dp[i][j] = dp[i][j - 1] && s3[i + j - 1] === s2[j + i - 1];
      } else if (j === 0) {
        dp[i][j] = dp[i - 1][j] && s3[i + j - 1] === s1[i + j - 1];
      } else {
        dp[i][j] =
          (dp[i - 1][j] && s3[i + j - 1] === s1[i - 1]) ||
          (dp[i][j - 1] && s3[i + j - 1] === s2[j - 1]);
      }
    }
  }

  return dp[s1len][s2len];
};

console.log(isInterleave("aabcc", "dbbca", "aadbbcbcac"));
