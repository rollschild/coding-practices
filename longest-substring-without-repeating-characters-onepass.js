/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
  // one-pass sliding window + hashtable
  if (s.length === 0 || s.length === 1) return s.length;
  const len = s.length;

  let left = 0;
  let right = 0;
  const freqs = {};
  let maxLen = 0;

  while (left <= right && right < len) {
    const letter = s[right];
    if (letter in freqs) {
      left = Math.max(freqs[letter], left);
    }
    right++;
    maxLen = Math.max(maxLen, right - left);
    freqs[letter] = right;
  }

  return maxLen;
};
