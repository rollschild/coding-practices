/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum2 = function(candidates, target) {
  const len = candidates.length;
  if (len === 0) return [];

  candidates.sort((a, b) => a - b);
  const res = [];
  search(candidates, target, res, 0, []);

  return res;
};

const search = (candidates, target, res, pos, curr) => {
  if (target < 0) return;
  if (target === 0) {
    res.push(curr);
    return;
  }

  for (let i = pos; i < candidates.length; i++) {
    const num = candidates[i];
    // skip duplicates
    if (i > pos && num === candidates[i - 1]) continue;
    search(candidates, target - num, res, i + 1, [...curr, num]);
  }

  return;
};
