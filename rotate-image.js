/* Time: O(N^2)
 * space: O(1)
 */
/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var rotate = function(matrix) {
  if (matrix.length === 0) return matrix;
  const n = matrix.length;
  const visited = new Set();
  for (let r = 0; r < n; r++) {
    for (let c = 0; c < n; c++) {
      if (visited.has(r * n + c)) continue;
      visited.add(r * n + c);
      visited.add(c * n + n - 1 - r);
      visited.add((n - 1 - r) * n + n - 1 - c);
      const temp = matrix[n - 1 - c][r];
      visited.add((n - 1 - c) * n + r);
      matrix[n - 1 - c][r] = matrix[n - 1 - r][n - 1 - c];
      matrix[n - 1 - r][n - 1 - c] = matrix[c][n - 1 - r];
      matrix[c][n - 1 - r] = matrix[r][c];
      matrix[r][c] = temp;
    }
  }
};
