/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */

var rightSideView = function(root) {
  const res = [];

  if (!root) return res;

  if (!root.left && !root.right) return [root.val];

  let values = [];

  let prev = null;

  let curr = null;

  values.push(root);

  while (values.length > 0) {
    const len = values.length;

    for (let i = 0; i < len; i++) {
      curr = values.shift();
      if (curr.left) values.push(curr.left);
      if (curr.right) values.push(curr.right);
    }
    prev = curr;
    res.push(prev.val);
  }

  return res;
};
