/**
 * @param {number[][]} matrix
 * @return {number[][]}
 */
var updateMatrix = function(matrix) {
  if (matrix.length === 0) return matrix;
  const h = matrix.length;
  const w = matrix[0].length;
  const res = new Array(h);
  for (let k = 0; k < h; k++) {
    res[k] = new Array(w).fill(Infinity);
  }
  const queue = [];

  for (let i = 0; i < h; i++) {
    for (let j = 0; j < w; j++) {
      if (matrix[i][j] === 0) {
        res[i][j] = 0;
        queue.push([i, j]);
      }
    }
  }

  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  while (queue.length > 0) {
    const [row, col] = queue.shift();
    for (let direction of directions) {
      const newRow = row + direction[0];
      const newCol = col + direction[1];
      if (
        newRow >= 0 &&
        newRow < h &&
        newCol >= 0 &&
        newCol < w &&
        res[newRow][newCol] > res[row][col] + 1
      ) {
        res[newRow][newCol] = res[row][col] + 1;
        queue.push([newRow, newCol]);
      }
    }
  }

  return res;
};
