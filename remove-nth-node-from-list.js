/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
const listLen = node => {
  let len = 0;
  while (node) {
    len++;
    node = node.next;
  }
  return len;
};

var removeNthFromEnd = function(head, n) {
  const len = listLen(head);
  if (len === 0) {
    return null;
  }
  if (n === 0) {
    return head;
  } else if (n === len) {
    const newHead = head.next;
    head = null;
    return newHead;
  }

  // position of node that needs to be removed
  const pos = len - n;
  let node = head;
  for (let pre = 0; pre < len; pre++) {
    if (pre + 1 === pos) {
      let removedNode = node.next;
      node.next = removedNode.next;
      removedNode = null;
      return head;
    }
    node = node.next;
  }
};
