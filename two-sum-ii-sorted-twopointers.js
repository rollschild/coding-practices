/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(numbers, target) {
  const len = numbers.length;
  let lo = 0;
  let hi = len - 1;
  while (lo < hi) {
    const sum = numbers[lo] + numbers[hi];
    if (sum < target) {
      lo++;
    } else if (sum > target) {
      hi--;
    } else {
      return [lo + 1, hi + 1];
    }
  }
};
