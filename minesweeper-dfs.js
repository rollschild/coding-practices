/* Time: O(N)
 */
/**
 * @param {character[][]} board
 * @param {number[]} click
 * @return {character[][]}
 */

var updateBoard = function(board, click) {
  const height = board.length;
  const width = board[0].length;
  const visited = {};

  dfs(board, click, height, width, visited);

  return board;
};

const dfs = (board, click, height, width, visited) => {
  const [row, col] = click;
  if (row < 0 || row >= height || col < 0 || col >= width) return;
  if (visited[row * width + col]) return;
  visited[row * width + col] = true;
  if (board[row][col] === "M") {
    board[row][col] = "X";
    return;
  }

  const directions = [
    [-1, 0],
    [1, 0],
    [0, 1],
    [0, -1],
    [-1, -1],
    [1, 1],
    [-1, 1],
    [1, -1],
  ];

  if (board[row][col] === "E") {
    let adjMine = 0;

    for (const [r, c] of directions) {
      const newRow = row + r;
      const newCol = col + c;
      if (newRow >= 0 && newRow < height && newCol >= 0 && newCol < width) {
        if (board[newRow][newCol] === "M") {
          adjMine++;
        }
      }
    }
    if (adjMine > 0) {
      board[row][col] = adjMine.toString();
    } else {
      board[row][col] = "B";
      for (const [r, c] of directions) {
        dfs(board, [row + r, col + c], height, width, visited);
      }
    }
  }

  return;
};
