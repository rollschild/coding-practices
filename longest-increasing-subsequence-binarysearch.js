/* Time: O(NlogN)
 * space: O(N) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var lengthOfLIS = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;
  const tails = new Array(len).fill(0);
  let size = 0;

  for (const num of nums) {
    let i = 0;
    let j = size;
    while (i !== j) {
      const mid = Math.floor((i + j) / 2);
      if (tails[mid] < num) {
        i = mid + 1;
      } else {
        j = mid;
      }
    }
    tails[i] = num;
    size = Math.max(i + 1, size);
  }

  return size;
};
