/**
 * @param {number} m
 * @param {number} n
 * @param {number[][]} positions
 * @return {number[]}
 */

var numIslands2 = function(m, n, positions) {
  if (positions.length === 0) return [0];
  if (positions.length === 1) return [1];

  const num = positions.length;
  const arr = new Array(m);
  for (let i = 0; i < m; i++) {
    arr[i] = new Array(n).fill(0);
  }
  const res = [];
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  for (const [row, col] of positions) {
    let currIslands = 0;
    if (arr[row][col] === 1) {
      currIslands = res[res.length - 1];
      res.push(currIslands);
      continue;
    }

    arr[row][col] = 1;

    if (res.length === 0) {
      currIslands = 1;
      res.push(currIslands);
    } else {
      let numOfAdj = 0;
      currIslands = res[res.length - 1];

      for (const [r, c] of directions) {
        const newRow = row + r;
        const newCol = col + c;
        if (
          newRow >= 0 &&
          newRow < m &&
          newCol >= 0 &&
          newCol < n &&
          arr[newRow][newCol] === 1
        ) {
          numOfAdj++;
        }
      }
      if (numOfAdj > 0) {
        if (numOfAdj > 1) res.push(currIslands - 1);
        else res.push(currIslands);
      } else {
        res.push(currIslands + 1);
      }
    }
  }

  return res;
};
