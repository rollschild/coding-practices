class Solution:
    def search(self, nums: List[int], target: int) -> int:
        nums_len = len(nums)
        if nums_len == 0:
            return -1
        if nums_len == 1:
            return 0 if nums[0] == target else -1

        start = 0
        end = nums_len - 1

        while start <= end:
            mid = (start + end) // 2
            if target == nums[mid]:
                return mid

            if nums[mid] >= nums[start]:
                # left side not rotated
                if target >= nums[start] and target < nums[mid]:
                    end = mid - 1
                else:
                    start = mid + 1
            else:
                # left side rotated
                # right side not rotated
                if target > nums[mid] and target <= nums[end]:
                    start = mid + 1
                else:
                    end = mid - 1

        return -1
