/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */
var wordBreak = function(s, wordDict) {
  return helper(s, wordDict, {});
};

const helper = (s, wordDict, visited) => {
  if (s.length === 0) return true;
  if (s in visited) return visited[s];
  for (const word of wordDict) {
    if (word.length <= s.length && s.startsWith(word)) {
      const sub = s.substring(word.length);
      if (helper(sub, wordDict, visited)) {
        visited[sub] = true;
        return true;
      }
    }
  }

  visited[s] = false;
  return false;
};
