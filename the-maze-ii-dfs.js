/* Time: O(m*n*max(m, n))
 * Space: O(m*n)
 */
/**
 * @param {number[][]} maze
 * @param {number[]} start
 * @param {number[]} destination
 * @return {number}
 */

var shortestDistance = function(maze, start, destination) {
  if (maze.length === 0) return -1;
  const height = maze.length;
  const width = maze[0].length;
  const arr = new Array(height);
  for (let i = 0; i < height; i++) {
    arr[i] = new Array(width).fill(Infinity);
  }
  arr[start[0]][start[1]] = 0;

  dfs(start, maze, arr);

  return arr[destination[0]][destination[1]] === Infinity
    ? -1
    : arr[destination[0]][destination[1]];
};

const dfs = (pos, maze, arr) => {
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  for (const [r, c] of directions) {
    let [row, col] = pos;
    let count = 0;

    while (
      row + r >= 0 &&
      col + c >= 0 &&
      row + r < maze.length &&
      col + c < maze[0].length &&
      maze[row + r][col + c] === 0
    ) {
      row += r;
      col += c;
      count++;
    }

    if (arr[pos[0]][pos[1]] + count < arr[row][col]) {
      arr[row][col] = arr[pos[0]][pos[1]] + count;
      dfs([row, col], maze, arr);
    }
  }
};
