# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution:
    def lowestCommonAncestor(
        self, root: "TreeNode", p: "TreeNode", q: "TreeNode"
    ) -> "TreeNode":
        if root is None:
            return None

        nodes = [root]
        parents = {root: None}

        while len(nodes) > 0 and (p not in parents or q not in parents):
            node = nodes.pop(0)
            if node.left:
                nodes.append(node.left)
                parents[node.left] = node
            if node.right:
                nodes.append(node.right)
                parents[node.right] = node

        ancestors = set()

        while p:
            ancestors.add(p)
            p = parents[p]
        while q not in ancestors:
            q = parents[q]

        return q
