/* Time: O(N*KlogK) - N is length of strs, K is average length of each string
 * space: O(N*K)
 */
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function(strs) {
  const obj = {};

  for (const str of strs) {
    const newStr = str
      .split("")
      .sort((a, b) => {
        if (a < b) return -1;
        else if (a > b) return 1;
        else return 0;
      })
      .join("");
    if (!(newStr in obj)) {
      obj[newStr] = [str];
    } else {
      obj[newStr].push(str);
    }
  }

  return Object.values(obj);
};
