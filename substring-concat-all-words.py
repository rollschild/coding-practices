class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        from collections import Counter

        obj = Counter(words)

        res = []

        num_of_words = len(words)
        str_len = len(s)

        if num_of_words == 0 or str_len == 0:
            return res

        word_len = len(words[0])

        for i in range(0, str_len + 1 - word_len):
            visited = {}
            j = 0
            while j < num_of_words:
                word = s[i + j * word_len : i + (j + 1) * word_len]
                if word not in obj:
                    break

                visited[word] = visited.get(word, 0) + 1
                if visited[word] > obj[word]:
                    break

                j += 1

            if j == num_of_words:
                res.append(i)

        return res
