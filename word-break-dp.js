/* Time O(n^2)
 * Space O(n)
 */
/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */
var wordBreak = function(s, wordDict) {
  const dp = new Array(s.length + 1).fill(false);
  dp[0] = true;

  for (let i = 0; i <= s.length; i++) {
    for (let j = 0; j < i; j++) {
      const substr = s.substring(j, i);
      if (dp[j] && wordDict.includes(substr)) {
        dp[i] = true;
        break;
      }
    }
  }

  return dp[s.length];
};
