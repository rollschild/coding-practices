/* Time: O(N^2)
 * space: O(N)
 */
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function(nums) {
  const len = nums.length;
  if (len === 0) return true;
  const constants = {
    GOOD: 1,
    BAD: 0,
    UNKNOWN: -1,
  };
  const memo = new Array(len).fill(constants.UNKNOWN);
  memo[len - 1] = constants.GOOD;

  for (i = len - 2; i >= 0; i--) {
    const steps = nums[i];
    const furthest = Math.min(i + steps, len - 1);
    for (let pos = i + 1; pos <= furthest; pos++) {
      if (memo[pos] === constants.GOOD) {
        memo[i] = constants.GOOD;
      }
    }
    if (memo[i] === constants.UNKNOWN) memo[i] = constants.BAD;
  }

  return memo[0];
};
