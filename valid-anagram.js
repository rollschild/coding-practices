/* Time: O(N)
 * space: O(1) */
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function(s, t) {
  if (s.length !== t.length) return false;

  const alphaS = new Array(26).fill(0);
  const alphaT = new Array(26).fill(0);

  for (let i = 0; i < s.length; i++) {
    const pos = s.charCodeAt(i) - "a".charCodeAt(0);
    alphaS[pos]++;
  }
  for (let j = 0; j < t.length; j++) {
    const pos = t.charCodeAt(j) - "a".charCodeAt(0);
    alphaT[pos]++;
  }

  for (let k = 0; k < 26; k++) {
    if (alphaS[k] !== alphaT[k]) return false;
  }

  return true;
};
