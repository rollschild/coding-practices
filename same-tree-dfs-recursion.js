/** time: O(n)
 * space: O(log(n)) best, O(n) worst
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree = function(p, q) {
  const nodesP = [];
  const nodesQ = [];
  traverse(p, nodesP);
  traverse(q, nodesQ);
  return checkArrayEqual(nodesP, nodesQ);
};

const checkArrayEqual = (arr1, arr2) => {
  if (arr1.length !== arr2.length) return false;
  const len = arr1.length;
  for (let i = 0; i < len; i++) {
    if (arr1[i] !== arr2[i]) return false;
  }
  return true;
};

const traverse = (node, nodesArray) => {
  if (node) {
    nodesArray.push(node.val);
    if (node.left && node.right) {
      traverse(node.left, nodesArray);
      traverse(node.right, nodesArray);
    } else if (node.left) {
      traverse(node.left, nodesArray);
    } else if (node.right) {
      nodesArray.push(null);
      traverse(node.right, nodesArray);
    }
  }
};
