class Solution:
    def minWindow(self, s: str, t: str) -> str:
        s_len = len(s)
        t_len = len(t)

        if s_len == 0 or t_len == 0:
            return ""

        left = 0
        right = 0

        from collections import Counter

        required_dict = Counter(t)
        required_unique_len = len(required_dict)

        window_dict = {}
        count = 0

        window = [float("inf"), ""]

        while left <= right and right < s_len:
            letter = s[right]

            if required_dict[letter]:
                window_dict[letter] = window_dict.get(letter, 0) + 1
                if window_dict[letter] == required_dict[letter]:
                    count += 1

                while count == required_unique_len and left <= right:
                    about_to_remove = s[left]
                    if right - left + 1 < window[0]:
                        window[0] = right - left + 1
                        window[1] = s[left : (right + 1)]

                    if required_dict[about_to_remove]:
                        window_dict[about_to_remove] -= 1
                        if (
                            window_dict[about_to_remove]
                            < required_dict[about_to_remove]
                        ):
                            count -= 1

                    left += 1

            right += 1

        return "" if window[0] == float("inf") else window[1]


solution = Solution()
print(solution.minWindow("ADOBECODEBANC", "ABC"))
