/* Time O(V + E)
 * Space O(V) */
/**
 * // Definition for a Node.
 * function Node(val, neighbors) {
 *    this.val = val === undefined ? 0 : val;
 *    this.neighbors = neighbors === undefined ? [] : neighbors;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
var cloneGraph = function(node) {
  if (!node) return null;
  const queue = [node];
  const root = new Node(node.val, []);
  const visited = {};
  visited[root.val] = root;

  // use a hashtable to store the nodes
  // plus as a visited map to skip duplicate nodes

  while (queue.length > 0) {
    const n = queue.shift();
    if (!(n.val in visited)) {
      visited[n.val] = new Node(n.val, []);
    }
    for (const adj of n.neighbors) {
      if (!(adj.val in visited)) {
        visited[adj.val] = new Node(adj.val, []);
        queue.push(adj);
      }
      visited[n.val].neighbors.push(visited[adj.val]);
    }
  }

  return root;
};
