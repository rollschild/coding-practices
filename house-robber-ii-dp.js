/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;
  if (len === 1) return nums[0];
  if (len === 2) return Math.max(...nums);

  const dp = new Array(len).fill(0);

  // count the first
  dp[0] = nums[0];
  dp[1] = Math.max(dp[0], nums[1]);
  for (let i = 2; i < len - 1; i++) {
    dp[i] = Math.max(dp[i - 2] + nums[i], dp[i - 1]);
  }

  const max1 = dp[len - 2];
  for (let i = 0; i < len; i++) dp[i] = 0;
  dp[0] = 0;
  dp[1] = nums[1];
  dp[2] = Math.max(dp[1], nums[2]);
  for (let i = 3; i < len; i++) {
    dp[i] = Math.max(dp[i - 2] + nums[i], dp[i - 1]);
  }
  const max2 = dp[len - 1];

  return Math.max(max1, max2);
};
