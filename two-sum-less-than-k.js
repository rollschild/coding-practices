/**
 * @param {number[]} A
 * @param {number} K
 * @return {number}
 */
var twoSumLessThanK = function(A, K) {
  const len = A.length;

  A.sort((a, b) => a - b);
  console.log("sorted: ", A);
  let sum = 0;

  for (let i = len - 1; i > 0; i--) {
    for (let j = len - 2; j >= 0; j--) {
      if (i > j && A[i] + A[j] < K) sum = Math.max(sum, A[i] + A[j]);
    }
  }

  if (sum === 0) return -1;

  return sum;
};
