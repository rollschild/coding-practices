# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Codec:
    def serialize(self, root):
        """Encodes a tree to a single string.
        :type root: TreeNode
        :rtype: str
        """

        if root is None:
            return "[None]"
        queue = []
        arr = []
        queue.append(root)
        arr.append(str(root.val))
        while len(queue) > 0:
            node = queue.pop(0)
            if node.left is not None:
                queue.append(node.left)
                arr.append(str(node.left.val))
            else:
                arr.append("None")
            if node.right is not None:
                queue.append(node.right)
                arr.append(str(node.right.val))
            else:
                arr.append("None")

        return "[" + ",".join(arr) + "]"

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        :type data: str
        :rtype: TreeNode
        """

        if len(data) <= 2:
            return None
        arr = data[1 : (len(data) - 1)].split(",")
        first_val = arr[0]
        if first_val == "None":
            return None
        queue = []
        root = TreeNode(int(first_val))
        queue.append(root)
        pos = 0

        while len(queue) > 0 and pos < len(arr):
            node = queue.pop(0)
            leftPos = pos * 2 + 1
            rightPos = pos * 2 + 2
            if leftPos < len(arr) and arr[leftPos] != "None":
                leftChild = TreeNode(int(arr[leftPos]))
                queue.append(leftChild)
                node.left = leftChild
            if rightPos < len(arr) and arr[rightPos] != "None":
                rightChild = TreeNode(int(arr[rightPos]))
                queue.append(rightChild)
                node.right = rightChild
            pos = pos + 1
        return root


# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))
