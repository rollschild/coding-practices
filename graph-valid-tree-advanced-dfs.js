/* Time and Space O(N)
 */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */

var validTree = function(n, edges) {
  // advanced graph theory
  if (edges.length !== n - 1) return false;
  if (edges.length === 0) return true;

  const graph = {};
  for (const [startNode, endNode] of edges) {
    if (!(startNode in graph)) graph[startNode] = [];
    graph[startNode].push(endNode);
    if (!(endNode in graph)) graph[endNode] = [];
    graph[endNode].push(startNode);
  }

  const visited = new Set();
  const firstNode = edges[0][0];
  dfs(firstNode, graph, visited);

  return visited.size === n;
};

const dfs = (firstNode, graph, visited) => {
  if (!(firstNode in graph)) return;
  if (visited.has(firstNode)) return;
  visited.add(firstNode);
  for (const adj of graph[firstNode]) {
    dfs(adj, graph, visited);
  }
};
