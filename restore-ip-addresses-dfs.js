/**
 * @param {string} s
 * @return {string[]}
 */
var restoreIpAddresses = function(s) {
  const startPos = -1;
  const ipList = backtrack(-1, s, 3, [], []);
  return ipList;
};

const backtrack = (startPos, str, dots, res, ipList) => {
  // place dot
  /* let currPos = startPos + 1; */
  const len = str.length;
  const posLimit = Math.min(startPos + 4, len - 1);
  for (let currPos = startPos + 1; currPos < posLimit; currPos++) {
    const segment = str.substring(startPos + 1, currPos + 1);
    if (isValid(segment)) {
      res.push(segment);
      if (dots === 1) {
        // we finished inserting all dots
        const rest = str.substring(currPos + 1, len);
        if (isValid(rest)) {
          res.push(rest);
          ipList.push(res.join("."));
          res.pop();
        }
      } else {
        // more dots need to be inserted
        ipList = backtrack(currPos, str, dots - 1, res, ipList);
      }
      res.pop();
    }
  }

  return ipList;
};

const isValid = str => {
  if (str.length > 3 || str.length === 0) return false;
  return str[0] === "0" ? str.length === 1 : parseInt(str, 10) <= 255;
};

const testIp = "25525511135";
