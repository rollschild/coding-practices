/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var minWindow = function(s, t) {
  let res = "";
  let left = 0;
  let right = 0;
  const target = {};
  for (let i = 0; i < t.length; i++) {
    target[t[i]] = (target[t[i]] || 0) + 1;
  }

  const obj = {};
  while (left <= right && right <= s.length) {
    if (compare(obj, target)) {
      if (res.length === 0 || right - left < res.length) {
        res = s.substring(left, right);
      }
      const leftLetter = s[left];
      left++;
      if (leftLetter in target) {
        if (obj[leftLetter] === 1) delete obj[leftLetter];
        else obj[leftLetter]--;
      }
    } else {
      if (right < s.length) {
        const letter = s[right];
        if (letter in target) obj[letter] = (obj[letter] || 0) + 1;
      }
      right++;
    }
  }

  return res;
};

const compare = (obj, target) => {
  if (Object.keys(obj).length !== Object.keys(target).length) return false;
  for (const key in target) {
    if (!(key in obj) || obj[key] < target[key]) return false;
  }
  return true;
};
