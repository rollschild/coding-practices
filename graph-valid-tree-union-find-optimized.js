/* Time: O(N)
 * Space: O(N)
 */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */

var validTree = function(n, edges) {
  // Union Find
  // with path compression and union by size

  if (edges.length !== n - 1) return false;
  const unionFind = new UnionFind(n);
  for (const [node1, node2] of edges) {
    const res = unionFind.union(node1, node2);
    if (!res) {
      return false;
    }
  }

  return true;
};

class UnionFind {
  constructor(num) {
    this.parents = new Array(num);
    this.sizes = new Array(num).fill(1);
    for (let i = 0; i < num; i++) {
      this.parents[i] = i;
    }
  }

  find(node) {
    let root = node;
    while (root !== this.parents[root]) {
      root = this.parents[root];
    }

    while (node !== root) {
      const oldRoot = this.parents[node];
      this.parents[node] = root;
      node = oldRoot;
    }

    return root;
  }

  union(node1, node2) {
    const parent1 = this.find(node1);
    const parent2 = this.find(node2);

    if (parent1 === parent2) {
      return false;
    }
    if (this.sizes[parent1] < this.sizes[parent2]) {
      this.parents[parent1] = parent2;
    } else {
      this.parents[parent2] = parent1;
    }

    return true;
  }
}
