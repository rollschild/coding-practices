/* Time: O(n)
 * space: O(n)
 */
/**
 * initialize your data structure here.
 */

var MedianFinder = function() {
  this.arr = [];
};

/**
 * @param {number} num
 * @return {void}
 */

MedianFinder.prototype.addNum = function(num) {
  // inserstion sort
  const len = this.arr.length;

  if (len === 0) {
    this.arr.push(num);
    return;
  }

  let left = 0;
  let right = len - 1;

  while (left <= right && right < len) {
    const midPos = Math.floor((left + right) / 2);
    const mid = this.arr[midPos];

    if (mid === num) {
      this.arr.splice(midPos, 0, num);
      return;
    }
    if (mid < num) {
      if (midPos === len - 1) {
        this.arr.push(num);
        return;
      }
      if (this.arr[midPos + 1] >= num) {
        this.arr.splice(midPos + 1, 0, num);
        return;
      }
      left = midPos + 1;
    }

    if (mid > num) {
      if (midPos === 0) {
        this.arr.unshift(num);
        return;
      }

      if (this.arr[midPos - 1] <= num) {
        this.arr.splice(midPos, 0, num);
        return;
      }
      right = midPos - 1;
    }
  }

  return;
};

/**
 * @return {number}
 */

MedianFinder.prototype.findMedian = function() {
  const len = this.arr.length;
  if (len % 2 === 0) {
    const right = len / 2;
    const left = len / 2 - 1;
    return (this.arr[left] + this.arr[right]) / 2;
  } else {
    return this.arr[Math.floor(len / 2)];
  }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * var obj = new MedianFinder()
 * obj.addNum(num)
 * var param_2 = obj.findMedian()
 */
