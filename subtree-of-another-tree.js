/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} s
 * @param {TreeNode} t
 * @return {boolean}
 */
var isSubtree = function(s, t) {
  if (!s && !t) return true;
  if ((!s && t) || (s && !t)) return false;
  if (s.val !== t.val) {
    return isSubtree(s.left, t) || isSubtree(s.right, t);
  } else {
    if (traverse(s, t)) return true;
    return isSubtree(s.left, t) || isSubtree(s.right, t);
  }
};

const traverse = (s, t) => {
  if (!s && !t) return true;
  if ((!s && t) || (s && !t)) return false;
  if (s.val !== t.val) return false;
  return traverse(s.left, t.left) && traverse(s.right, t.right);
};
