/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function(head) {
  if (!head) return false;
  const visited = new Map();

  while (head) {
    if (visited.has(head)) return true;
    visited.set(head, true);
    head = head.next;
  }

  return false;
};
