/* N is number of variables
 * Q is number of equations
 * M is number of queries
 * Time: O(M(N^2))
 * Space: O(M*N + Q + N)
 */
/**
 * @param {string[][]} equations
 * @param {number[]} values
 * @param {string[][]} queries
 * @return {number[]}
 */

var calcEquation = function(equations, values, queries) {
  const graph = {};
  const len = equations.length;

  for (let i = 0; i < len; i++) {
    const [dividend, divisor] = equations[i];
    const quotient = values[i];

    if (!(dividend in graph)) {
      graph[dividend] = [];
    }
    graph[dividend].push([divisor, quotient]);

    if (!(divisor in graph)) {
      graph[divisor] = [];
    }
    graph[divisor].push([dividend, 1 / quotient]);
  }

  const res = [];
  queries.forEach(pair => divide(pair, res, graph));

  return res;
};

const divide = (pair, res, graph) => {
  const [dividend, divisor] = pair;

  if (!(dividend in graph) || !(divisor in graph)) {
    res.push(-1);
    return;
  }

  const queue = [[dividend, 1.0]];
  const visited = {};

  while (queue.length > 0) {
    const [front, product] = queue.shift();
    if (front === divisor) {
      res.push(product);
      return;
    }

    for (const [adj, quotient] of graph[front]) {
      if (!(adj in visited)) {
        queue.push([adj, quotient * product]);
        visited[adj] = true;
      }
    }
  }

  res.push(-1);
  return;
};
