from collections import defaultdict


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        obj = defaultdict(list)
        for str in strs:
            obj[tuple(sorted(str))].append(str)

        return obj.values()
