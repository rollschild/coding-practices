/* Time: O(logN)
 * Space: O(1)
 */
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
  // binary search
  // find smallest item
  const len = nums.length;
  if (len === 0) return -1;
  if (len === 1) return nums[0] === target ? 0 : -1;
  let left = 0;
  let right = len - 1;
  let rotateIndex = 0;

  let pivot = Math.floor((right + left) / 2);
  let offset = 0;
  while (pivot >= 0 && pivot < len && left <= right) {
    if (pivot > 0 && nums[pivot] < nums[pivot - 1]) {
      rotateIndex = pivot;
      break;
    } else if (pivot < len - 1 && nums[pivot] > nums[pivot + 1]) {
      rotateIndex = pivot + 1;
      break;
    } else if (nums[pivot] < nums[right]) {
      right = pivot - 1;
      pivot = Math.floor((right + left) / 2);
    } else {
      left = pivot + 1;
      pivot = Math.floor((right + left) / 2);
    }
  }

  if (nums[rotateIndex] === target) return rotateIndex;
  if (rotateIndex === 0) return findItem(nums, target, 0, len - 1);

  if (target >= nums[0]) return findItem(nums, target, 0, rotateIndex - 1);
  else return findItem(nums, target, rotateIndex, len - 1);
};

const findItem = (arr, target, left, right) => {
  const len = arr.length;

  let pivot = Math.floor((right + left) / 2);

  while (left >= 0 && right < len && left <= right) {
    if (arr[pivot] === target) {
      return pivot;
    } else if (arr[pivot] > target) {
      right = pivot - 1;
      pivot = Math.floor((left + right) / 2);
    } else {
      left = pivot + 1;
      pivot = Math.floor((left + right) / 2);
    }
  }

  return -1;
};
