/* Time: O(N^2)
 * space: O(N) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var lengthOfLIS = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;

  const dp = new Array(len).fill(0);
  dp[0] = 1;
  let maxLen = 1;

  for (let head = 1; head < len; head++) {
    let maxVal = 0;
    for (let tail = 0; tail < head; tail++) {
      if (nums[head] > nums[tail]) {
        maxVal = Math.max(maxVal, dp[tail]);
      }
    }
    dp[head] = maxVal + 1;
    maxLen = Math.max(dp[head], maxLen);
  }

  return maxLen;
};
