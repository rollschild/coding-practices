/* Time: O(N)
 * space: O(1)
 */
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function(nums) {
  // greedy
  const len = nums.length;
  if (len === 0) return true;
  let leftmost = len - 1;

  for (let i = len - 2; i >= 0; i--) {
    if (i + nums[i] >= leftmost) {
      leftmost = i;
    }
  }

  return leftmost === 0;
};
