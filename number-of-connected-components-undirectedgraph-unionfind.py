class Solution:
    def countComponents(self, n: int, edges) -> int:
        def find(x):
            if x != parent[x]:
                parent[x] = find(parent[x])
            return parent[x]

        def union(node):
            parent_x, parent_y = map(find, node)
            if parent_x == parent_y:
                return

            if rank[parent_x] < rank[parent_y]:
                parent_x, parent_y = parent_y, parent_x

            parent[parent_y] = parent_x

            if rank[parent_x] == rank[parent_y]:
                rank[parent_x] += 1

        parent = [i for i in range(0, n)]
        rank = [0] * n
        list(map(union, edges))

        return len({find(key) for key in parent})


sol = Solution()
print(sol.countComponents(5, [[0, 1], [1, 2], [3, 4]]))
