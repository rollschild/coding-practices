/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function(root) {
  const arrLeft = [];
  const arrRight = [];

  if (!root) return true;

  // dfs
  dfsLeft(root, arrLeft);
  dfsRight(root, arrRight);

  if (arrLeft.length !== arrRight.length) return false;
  for (let i = 0; i < arrLeft.length; i++) {
    if (arrLeft[i] !== arrRight[i]) return false;
  }

  return true;
};

const dfsLeft = (root, arr) => {
  if (!root) {
    arr.push(null);
    return;
  }

  arr.push(root.val);

  dfsLeft(root.left, arr);
  dfsLeft(root.right, arr);
};

const dfsRight = (root, arr) => {
  if (!root) {
    arr.push(null);
    return;
  }

  arr.push(root.val);

  dfsRight(root.right, arr);
  dfsRight(root.left, arr);
};
