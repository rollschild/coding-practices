/* Time and space: O(N)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */

var subarraySum = function(nums, k) {
  const len = nums.length;

  let sum = 0;
  let count = 0;
  const obj = {};
  obj[0] = 1;

  for (let start = 0; start < len; start++) {
    sum += nums[start];

    if (sum - k in obj) {
      count += obj[sum - k];
    }

    obj[sum] = (obj[sum] || 0) + 1;
  }

  return count;
};
