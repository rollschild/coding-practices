/* Time: O(N)
 * space: O(1) */
/**
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function(s) {
  const len = s.length;
  if (len === 0) return true;
  s = s.toLowerCase();
  let left = 0;
  let right = len - 1;
  const zero = "0".charCodeAt(0);
  const nine = "9".charCodeAt(0);
  const a = "a".charCodeAt(0);
  const z = "z".charCodeAt(0);

  const isValidChar = letter => {
    const code = letter.charCodeAt(0);
    return (code >= zero && code <= nine) || (code >= a && code <= z);
  };
  while (left <= right) {
    if (!isValidChar(s[left])) {
      left++;
      continue;
    }
    if (!isValidChar(s[right])) {
      right--;
      continue;
    }

    if (s[left] !== s[right]) return false;

    left++;
    right--;
  }

  return true;
};
