/* Time and space: O(N)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */

var lowestCommonAncestor = function(root, p, q) {
  if (!root) return null;
  const nodes = [root];
  const parents = {};

  parents[root.val] = null;

  while (nodes.length > 0 && (!parents[p.val] || !parents[q.val])) {
    const node = nodes.shift();
    if (node.left) {
      nodes.push(node.left);
      parents[node.left.val] = node;
    }
    if (node.right) {
      nodes.push(node.right);
      parents[node.right.val] = node;
    }
  }

  const ancestors = new Set();

  while (p) {
    ancestors.add(p);
    p = parents[p.val];
  }
  while (!ancestors.has(q)) {
    q = parents[q.val];
  }

  return q;
};
