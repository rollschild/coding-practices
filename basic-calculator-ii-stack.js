/**
 * @param {string} s
 * @return {number}
 */
// Use stack
var calculate = function(s) {
  if (s.length === 0) return 0;
  const len = s.length;
  const stack = [];
  let sign = "+";
  let pos = 0;
  let num = 0;
  const operators = ["+", "-", "*", "/"];
  while (pos < len) {
    if (!isNaN(s[pos]) && s[pos] !== " ") {
      num = num * 10 + Number.parseInt(s[pos]);
    }
    if (operators.includes(s[pos]) || pos === len - 1) {
      if (sign === "+") {
        stack.push(num);
      } else if (sign === "-") {
        stack.push(-num);
      } else if (sign === "*") {
        const lhs = stack.pop();
        stack.push(lhs * num);
      } else {
        const lhs = stack.pop();
        stack.push(Math.trunc(lhs / num));
      }
      num = 0;
      sign = s[pos];
    }
    pos++;
  }

  return stack.reduce((accu, curr) => accu + curr, 0);
};

console.log(calculate("3+2*2"));
