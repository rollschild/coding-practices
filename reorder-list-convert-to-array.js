/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function(head) {
  let node = head;
  if (!node) return node;

  const values = [];
  while (node) {
    values.push(node.val);
    node = node.next;
  }

  let index = 0;
  node = head;
  while (values.length > 0) {
    if (index === 0) {
      index++;
      values.shift();
      continue;
    }
    if (index % 2 === 0) {
      const val = values.shift();
      node.next = new ListNode(val);
      node = node.next;
    } else {
      const val = values.pop();
      node.next = new ListNode(val);
      node = node.next;
    }
    index++;
  }
};
