/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var addStrings = function(num1, num2) {
  const reversedNum1 = num1
    .split("")
    .reverse()
    .join("");
  const reversedNum2 = num2
    .split("")
    .reverse()
    .join("");
  const len = Math.min(num1.length, num2.length);
  let carry = 0;
  let res = "";
  const zero = "0".charCodeAt(0);
  for (let i = 0; i < len; i++) {
    let sum =
      reversedNum1.charCodeAt(i) -
      zero +
      reversedNum2.charCodeAt(i) -
      zero +
      carry;
    if (sum > 9) {
      sum -= 10;
      carry = 1;
    } else {
      carry = 0;
    }

    res += sum;
  }
  if (len < reversedNum1.length) {
    for (let j = len; j < reversedNum1.length; j++) {
      let sum = reversedNum1.charCodeAt(j) - zero + carry;
      if (sum > 9) {
        sum -= 10;
        carry = 1;
      } else {
        carry = 0;
      }

      res += sum;
    }
  }
  if (len < reversedNum2.length) {
    for (let j = len; j < reversedNum2.length; j++) {
      let sum = reversedNum2.charCodeAt(j) - zero + carry;
      if (sum > 9) {
        sum -= 10;
        carry = 1;
      } else {
        carry = 0;
      }

      res += sum;
    }
  }
  if (carry > 0) {
    res += "1";
    carry = 0;
  }
  return res
    .split("")
    .reverse()
    .join("");
};
