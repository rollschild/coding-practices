/**
 * @param {number[][]} grid
 * @return {number}
 */
const checkFresh = function(grid) {
  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[i].length; j++) {
      if (grid[i][j] === 1) {
        return true;
      }
    }
  }

  return false;
};

const checkRotten = grid => {
  const arr = [];
  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[0].length; j++) {
      if (grid[i][j] === 2) {
        arr.push([i, j]);
      }
    }
  }
  return arr;
};

var orangesRotting = function(grid) {
  if (grid.length === 0) {
    return 0;
  }
  if (!checkFresh(grid)) {
    return 0;
  }

  const queue = checkRotten(grid);
  const obj = {};
  const height = grid.length;
  const width = grid[0].length;
  const dr = [-1, 0, 1, 0];
  const dc = [0, -1, 0, 1];

  let dist = 0;
  while (queue.length > 0) {
    const node = queue.shift();
    // a position coordinate

    for (let p = 0; p < 4; p++) {
      const row = node[0] + dr[p];
      const col = node[1] + dc[p];
      if (
        row >= 0 &&
        row < height &&
        col >= 0 &&
        col < width &&
        grid[row][col] === 1
      ) {
        grid[row][col] = 2;
        if (queue.indexOf([row, col]) === -1) {
          queue.push([row, col]);
        }
        const tempDist = (obj[node[0] * width + node[1]] || 0) + 1;
        obj[row * width + col] = tempDist;
        dist = tempDist;
      }
    }
  }

  if (checkFresh(grid)) {
    return -1;
  }

  return dist;
};
