#include <stdio.h>
#include <stdlib.h>

struct Node {
  int val;
  struct Node* next;
};

struct Node* to_list(int num) {
  if (num == 1) {
    return NULL;
  }

  struct Node* node = (struct Node*)malloc(sizeof(struct Node));
  node->val = num;
  num--;
  node->next = to_list(num);

  return node;
}

int main() {
  struct Node* res = to_list(3);
  printf("final res: %d\n", res->val);
}
