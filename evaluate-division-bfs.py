class Solution:
    def calcEquation(
        self,
        equations: List[List[str]],
        values: List[float],
        queries: List[List[str]],
    ) -> List[float]:
        graph = {}

        def build_graph(equations, values):
            def add_edge(f, b, value):
                if f in graph:
                    graph[f].append((b, value))
                else:
                    graph[f] = [(b, value)]

            for pair, value in zip(equations, values):
                f, b = pair
                add_edge(f, b, value)
                add_edge(b, f, 1 / value)

        def find_path(pair):
            dividend, divisor = pair
            if dividend not in graph or divisor not in graph:
                return -1

            queue = [(dividend, 1.0)]
            visited = {}
            while queue:
                d, product = queue.pop(0)
                if d == divisor:
                    return product
                for adj, quotient in graph[d]:
                    if adj not in visited:
                        visited[adj] = True
                        queue.append((adj, product * quotient))

            return -1

        build_graph(equations, values)
        return [find_path(pair) for pair in queries]
