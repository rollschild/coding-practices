/* Time: O(N)
 * Space: O(1) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;
  if (len === 1) return nums[0];

  let prevMax = nums[0];
  let maxProfit = Math.max(prevMax, nums[1]);
  for (let i = 2; i < len; i++) {
    const temp = maxProfit;
    maxProfit = Math.max(prevMax + nums[i], maxProfit);
    prevMax = temp;
  }

  return maxProfit;
};
