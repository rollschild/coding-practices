from queue import Queue
from typing import Tuple, List


class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        if len(grid) == 0:
            return 0
        if not self.check_for_fresh(grid):
            return -1

        arr = self.check_for_rotten(grid)
        obj = {}
        height = len(grid)
        width = len(grid[0])
        dr = [-1, 0, 1, 0]
        dc = [0, -1, 0, 1]

        dist = 0
        while not arr.empty():
            node = arr.get()

            for pos in range(0, 4):
                old_row = node[0]
                old_col = node[1]
                new_row = old_row + dr[pos]
                new_col = old_col + dc[pos]
                if (
                    new_row < height
                    and new_row >= 0
                    and new_col < width
                    and new_col >= 0
                    and grid[new_row][new_col] == 1
                ):
                    # need to rot
                    grid[new_row][new_col] = 2
                    arr.put((new_row, new_col))
                    temp_dist = (
                        obj[old_row * width + old_col]
                        if old_row * width + old_col in obj
                        else 0
                    ) + 1
                    obj[new_row * width + new_col] = temp_dist
                    dist = temp_dist

        if self.check_for_fresh(grid):
            return -1

        return dist

    def check_for_fresh(self, grid):
        for row in grid:
            for cell in row:
                if cell == 1:
                    return True

        return False

    def check_for_rotten(self, grid: List[List[int]]):
        arr = Queue()
        for i, row in enumerate(grid):
            for j, cell in enumerate(row):
                if cell == 2:
                    arr.put((i, j))

        return arr


sol = Solution()
print(sol.orangesRotting([[2, 1, 1], [1, 1, 0], [0, 1, 1]]))
