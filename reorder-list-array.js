/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function(head) {
  const arr = [];
  let node = head;
  while (node) {
    arr.push(node);
    node = node.next;
  }

  let root = head;
  let pos = 0;
  while (arr.length > 0) {
    if (pos === 0) {
      // root unchanged;
      pos++;
      arr.shift();
      continue;
    }
    if (pos % 2 !== 0) {
      root.next = arr.pop();
      root.next.next = null;
      root = root.next;
    } else {
      root.next = arr.shift();
      root.next.next = null;
      root = root.next;
    }
    pos++;
  }
};
