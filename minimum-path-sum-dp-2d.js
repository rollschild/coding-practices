/**
 * @param {number[][]} grid
 * @return {number}
 */
var minPathSum = function(grid) {
  if (grid.length === 0) {
    return 0;
  }
  const height = grid.length;
  const width = grid[0].length;
  const dp = new Array(height);
  for (let m = 0; m < height; m++) {
    dp[m] = new Array(width);
  }

  for (let i = height - 1; i >= 0; i--) {
    for (let j = width - 1; j >= 0; j--) {
      if (i === height - 1 && j === width - 1) {
        dp[i][j] = grid[i][j];
      } else if (i === height - 1) {
        dp[i][j] = grid[i][j] + dp[i][j + 1];
      } else if (j === width - 1) {
        dp[i][j] = grid[i][j] + dp[i + 1][j];
      } else {
        dp[i][j] = grid[i][j] + Math.min(dp[i + 1][j], dp[i][j + 1]);
      }
    }
  }

  return dp[0][0];
};
