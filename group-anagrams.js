/* Time: O(N * KlogK) - N is number of items in `strs`, K is max length of a str
 * Space: O(N * K)
 */
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function(strs) {
  const obj = {};
  for (let i = 0; i < strs.length; i++) {
    const strArray = strs[i].split("");
    const sortedStrArray = strArray.sort((a, b) => {
      if (a < b) return -1;
      else if (a > b) return 1;
      else return 0;
    });
    const sortedStr = sortedStrArray.join("");
    if (!obj[sortedStr]) {
      obj[sortedStr] = [strs[i]];
    } else {
      obj[sortedStr].push(strs[i]);
    }
  }

  return Object.values(obj);
};
