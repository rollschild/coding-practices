/* Time: O(N)
 * space: O(1)
 */
/**
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function(matrix) {
  const height = matrix.length;
  if (height === 0) return [];
  const width = matrix[0].length;

  let colStart = 0;
  let colEnd = width - 1;
  let rowStart = 0;
  let rowEnd = height - 1;
  const res = [];

  while (rowStart <= rowEnd && colStart <= colEnd) {
    for (let c = colStart; c <= colEnd; c++) {
      res.push(matrix[rowStart][c]);
    }
    for (let r = rowStart + 1; r <= rowEnd; r++) {
      res.push(matrix[r][colEnd]);
    }
    if (colStart < colEnd && rowStart < rowEnd) {
      for (let c = colEnd - 1; c > colStart; c--) {
        res.push(matrix[rowEnd][c]);
      }
      for (let r = rowEnd; r > rowStart; r--) {
        res.push(matrix[r][colStart]);
      }
    }

    colStart++;
    colEnd--;
    rowStart++;
    rowEnd--;
  }

  return res;
};
