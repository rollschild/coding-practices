# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def mergeTwoLists(self, l1, l2):
        if l1 is None:
            return l2
        if l2 is None:
            return l1

        if l1.val < l2.val:
            l1.next = self.mergeTwoLists(l1.next, l2)
            return l1
        else:
            l2.next = self.mergeTwoLists(l1, l2.next)
            return l2

    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        if len(lists) == 1:
            return lists[0]
        virtualHead = ListNode(0)
        node = virtualHead
        for index, item in enumerate(lists):
            if index < len(lists) - 1:
                if index == 0:
                    node.next = self.mergeTwoLists(
                        lists[index], lists[index + 1]
                    )
                else:
                    node.next = self.mergeTwoLists(node.next, lists[index + 1])

        return virtualHead.next
