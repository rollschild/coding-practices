/**
 * @param {number[]} height
 * @return {number}
 */
var maxArea = function(height) {
  let head = 0;
  let tail = height.length - 1;
  let area = 0;

  while (head < tail) {
    area = Math.max(area, Math.min(height[head], height[tail]) * (tail - head));
    if (height[head] < height[tail]) head++;
    else tail--;
  }

  return area;
};
