/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function(n, edges) {
  if (n !== edges.length + 1) return false;
  if (edges.length === 0) return true;

  const graph = {};
  for (const [node1, node2] of edges) {
    if (!(node1 in graph)) graph[node1] = [];
    graph[node1].push(node2);
    if (!(node2 in graph)) graph[node2] = [];
    graph[node2].push(node1);
  }

  const seen = new Set();

  const dfs = (node, parent) => {
    if (!(node in graph)) return false;
    if (seen.has(node)) return false;
    seen.add(node);

    for (const adj of graph[node]) {
      if (adj === parent) continue;
      if (seen.has(adj)) return false;
      if (!dfs(adj, node)) return false;
    }

    return true;
  };

  return dfs(0, -1) && seen.size === n;
};
