/* Time: O(N)
 * space: O(N)
 */
/**
 * @param {string} s
 * @return {number}
 */
var numDecodings = function(s) {
  const len = s.length;
  if (len === 0) return 0;
  const dp = new Array(len).fill(0);
  if (Number(s[0]) === 0) return 0;
  dp[0] = 1;

  for (let i = 1; i < len; i++) {
    const num = Number(s.substring(i - 1, i + 1));
    if (s[i] === "0") {
      if (num > 26 || num <= 0) return 0;
      if (i === 1) {
        dp[i] = 1;
      } else {
        dp[i] = dp[i - 2];
      }
    } else {
      if (i === 1) {
        dp[i] = dp[i - 1] + (num >= 11 && num <= 26 ? 1 : 0);
      } else {
        dp[i] = dp[i - 1] + (num >= 11 && num <= 26 ? dp[i - 2] : 0);
      }
    }
  }

  return dp[len - 1];
};
