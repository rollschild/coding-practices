/* Time: O(N)
 * space: O(N)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree = function(p, q) {
  const pArr = [];
  const qArr = [];
  dfs(p, pArr);
  dfs(q, qArr);

  if (pArr.length !== qArr.length) return false;
  for (let i = 0; i < pArr.length; i++) {
    if (pArr[i] !== qArr[i]) return false;
  }

  return true;
};

const dfs = (node, arr) => {
  if (!node) {
    arr.push(null);
    return;
  }
  arr.push(node.val);
  dfs(node.left, arr);
  dfs(node.right, arr);
};
