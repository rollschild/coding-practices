class Solution:
    def memoization(self, pos, memo):
        if pos == 0:
            return 0
        if pos == 1:
            return 1
        if pos == 2:
            return 2

        if memo[pos] is not None and memo[pos] > 0:
            return memo[pos]

        memo[pos] = self.memoization(pos - 1, memo) + self.memoization(
            pos - 2, memo
        )
        return memo[pos]

    def climbStairs(self, n: int) -> int:
        memo = [0 for x in range(n + 1)]
        return self.memoization(n, memo)
