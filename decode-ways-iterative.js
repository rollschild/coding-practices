/** O(N)
 */
/**
 * @param {string} s
 * @return {number}
 */
var numDecodings = function(s) {
  if (s.length === 0 || s[0] === "0") return 0;
  const len = s.length;
  const dp = new Array(len);
  dp[0] = 1;
  for (i = 1; i < len; i++) {
    dp[i] = 0;
  }
  for (i = 1; i < len; i++) {
    if (s[i] !== "0") {
      dp[i] += dp[i - 1];
    }
    const num = parseInt(s.substring(i - 1, i + 1), 10);
    if (num >= 10 && num <= 26) {
      if (i >= 2) {
        dp[i] += dp[i - 2];
      } else {
        dp[i] += 1;
      }
    }
  }
  return dp[len - 1];
};
console.log(numDecodings("1212"));
