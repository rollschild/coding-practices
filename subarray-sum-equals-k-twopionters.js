/* Time: O(n^2)
 * Space: O(1)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */

var subarraySum = function(nums, k) {
  const len = nums.length;
  let count = 0;

  for (let start = 0; start < len; start++) {
    let sum = 0;
    for (let end = start; end < len; end++) {
      sum += nums[end];
      if (sum === k) {
        count++;
      }
    }
  }

  return count;
};
