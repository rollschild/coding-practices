/* Time: O(NlogN)
 * space: O(1)
 */
/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function(intervals) {
  const len = intervals.length;
  if (len === 0) return [];
  intervals.sort((a, b) => {
    if (a[0] < b[0]) return -1;
    else if (a[0] > b[0]) return 1;
    else return a[1] - b[1];
  });
  const res = [];

  for (let i = 0; i < len; i++) {
    const interval = intervals[i];
    if (res.length === 0) res.push(interval);
    else {
      const latest = res[res.length - 1];
      if (latest[1] >= interval[0]) {
        res[res.length - 1] = [latest[0], Math.max(interval[1], latest[1])];
      } else {
        res.push(interval);
      }
    }
  }

  return res;
};
