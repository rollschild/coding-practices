# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        virtualHead = ListNode(0)
        virtualHead.next = head
        pre = virtualHead
        post = virtualHead

        for i in range(1, n + 2):
            post = post.next

        while post is not None:
            pre = pre.next
            post = post.next

        node = pre.next
        pre.next = node.next
        node = None

        return virtualHead.next
