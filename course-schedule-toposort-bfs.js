/* Time: O(V + E)
 * space: O(V + E) */
/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {boolean}
 */
var canFinish = function(numCourses, prerequisites) {
  const len = prerequisites.length;
  if (len === 0) return true;
  const graph = {};
  const degree = new Array(numCourses).fill(0);
  for (const [course1, course2] of prerequisites) {
    if (!(course2 in graph)) graph[course2] = [];
    graph[course2].push(course1);
    degree[course1]++;
  }

  const queue = [];
  for (let i = 0; i < degree.length; i++) {
    if (degree[i] === 0) queue.push(i);
  }

  for (const i of queue) {
    if (i in graph) {
      for (const j of graph[i]) {
        degree[j]--;
        if (degree[j] === 0) queue.push(j);
      }
    }
  }

  return queue.length === numCourses;
};
