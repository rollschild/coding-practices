from typing import List


class Solution:
    def __init__(self):
        self.visited = {}
        self.grid = None

    def explore(self, row, col, direction, res):
        height = len(self.grid)
        width = len(self.grid[0])

        if row >= 0 and row < height and col >= 0 and col < width:
            if (
                row * width + col not in self.visited
                or not self.visited[row * width + col]
            ) and self.grid[row][col] == 1:
                self.visited[row * width + col] = True
                res.append(direction)

                self.explore(row, col - 1, 1, res)
                self.explore(row, col + 1, 2, res)
                self.explore(row - 1, col, 3, res)
                self.explore(row + 1, col, 4, res)

                # exit
                res.append(0)

        return res

    def numDistinctIslands(self, grid: List[List[int]]) -> int:
        if len(grid) == 0:
            return 0
        self.grid = grid
        obj = {}

        for i, row in enumerate(grid):
            for j, cell in enumerate(row):
                res = tuple(self.explore(i, j, 0, []))
                if len(res) > 0 and not res in obj:
                    obj[res] = 1

        return len(obj)
