/* Time and space: O(N)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */

var maxSlidingWindow = function(nums, k) {
  // dp
  const len = nums.length;
  const fromLeft = new Array(len).fill(0);
  const fromRight = new Array(len).fill(0);

  fromLeft[0] = nums[0];
  fromRight[len - 1] = nums[len - 1];

  const res = [];

  for (let i = 1; i < len; i++) {
    if (i % k === 0) {
      // block start
      fromLeft[i] = nums[i];
    } else {
      fromLeft[i] = Math.max(fromLeft[i - 1], nums[i]);
    }

    let j = len - i - 1;
    if (j % k === k - 1) {
      // block end
      fromRight[j] = nums[j];
    } else {
      fromRight[j] = Math.max(fromRight[j + 1], nums[j]);
    }
  }

  for (let i = 0; i <= len - k; i++) {
    res.push(Math.max(fromLeft[i + k - 1], fromRight[i]));
  }

  return res;
};
