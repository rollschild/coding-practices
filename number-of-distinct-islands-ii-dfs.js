/* Time: O(R*C*log(R*C))
 */
/**
 * @param {number[][]} grid
 * @return {number}
 */
var numDistinctIslands2 = function(grid) {
  if (grid.length === 0) return 0;
  const height = grid.length;
  const width = grid[0].length;
  const visited = {};
  const islands = {};

  for (let r = 0; r < height; r++) {
    for (let c = 0; c < width; c++) {
      if (r * width + c in visited) continue;
      if (grid[r][c] === 1) {
        const island = [];
        dfs(r, c, grid, height, width, visited, island);
        if (island.length === 0) continue;
        island.sort((a, b) => {
          if (a[0] < b[0]) return -1;
          else if (a[0] > b[0]) return 1;
          else return a[1] - b[1];
        });
        if (!(island.length in islands)) islands[island.length] = [island];
        else {
          let duplicate = false;
          for (const isl of islands[island.length]) {
            if (isDuplicate(isl, island)) {
              duplicate = true;
              break;
            }
          }
          if (!duplicate) islands[island.length].push(island);
        }
      }
    }
  }

  return Object.values(islands).reduce((accum, curr) => accum + curr.length, 0);
};

const isDuplicate = (arr1, arr2) => {
  let diff = [arr2[0][0] - arr1[0][0], arr2[0][1] - arr1[0][1]];
  if (
    arr2.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;

  // rotation

  const rotatedArr2 = arr2
    .map(([x, y]) => [-y, x])
    .sort((a, b) => {
      if (a[0] < b[0]) return -1;
      else if (a[0] > b[0]) return 1;
      else return a[1] - b[1];
    });
  diff = [rotatedArr2[0][0] - arr1[0][0], rotatedArr2[0][1] - arr1[0][1]];
  if (
    rotatedArr2.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;

  // -x, -y
  const rotatedArr3 = arr2
    .map(([x, y]) => [-x, -y])
    .sort((a, b) => {
      if (a[0] < b[0]) return -1;
      else if (a[0] > b[0]) return 1;
      else return a[1] - b[1];
    });
  diff = [rotatedArr3[0][0] - arr1[0][0], rotatedArr3[0][1] - arr1[0][1]];
  if (
    rotatedArr3.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;

  // -x, y
  const rotatedArr4 = arr2
    .map(([x, y]) => [y, -x])
    .sort((a, b) => {
      if (a[0] < b[0]) return -1;
      else if (a[0] > b[0]) return 1;
      else return a[1] - b[1];
    });
  diff = [rotatedArr4[0][0] - arr1[0][0], rotatedArr4[0][1] - arr1[0][1]];
  if (
    rotatedArr4.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;

  // reflection
  // x, -y
  const reflectedArr2 = arr2
    .map(([x, y]) => [x, -y])
    .sort((a, b) => {
      if (a[0] < b[0]) return -1;
      else if (a[0] > b[0]) return 1;
      else return a[1] - b[1];
    });
  diff = [reflectedArr2[0][0] - arr1[0][0], reflectedArr2[0][1] - arr1[0][1]];
  if (
    reflectedArr2.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;

  // -x, y
  const reflectedArr3 = arr2
    .map(([x, y]) => [-x, y])
    .sort((a, b) => {
      if (a[0] < b[0]) return -1;
      else if (a[0] > b[0]) return 1;
      else return a[1] - b[1];
    });
  diff = [reflectedArr3[0][0] - arr1[0][0], reflectedArr3[0][1] - arr1[0][1]];
  if (
    reflectedArr3.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;

  // y, x
  const reflectedArr4 = arr2
    .map(([x, y]) => [y, x])
    .sort((a, b) => {
      if (a[0] < b[0]) return -1;
      else if (a[0] > b[0]) return 1;
      else return a[1] - b[1];
    });
  diff = [reflectedArr4[0][0] - arr1[0][0], reflectedArr4[0][1] - arr1[0][1]];
  if (
    reflectedArr4.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;

  // -y, -x
  const reflectedArr5 = arr2
    .map(([x, y]) => [-y, -x])
    .sort((a, b) => {
      if (a[0] < b[0]) return -1;
      else if (a[0] > b[0]) return 1;
      else return a[1] - b[1];
    });
  diff = [reflectedArr5[0][0] - arr1[0][0], reflectedArr5[0][1] - arr1[0][1]];
  if (
    reflectedArr5.every(
      (ele, idx) =>
        ele[0] - arr1[idx][0] === diff[0] && ele[1] - arr1[idx][1] === diff[1],
    )
  )
    return true;
  return false;
};

const dfs = (r, c, grid, height, width, visited, island) => {
  if (r * width + c in visited) return;
  visited[r * width + c] = true;
  if (grid[r][c] !== 1) return;
  island.push([r, c]);
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  for (const [row, col] of directions) {
    const newR = r + row;
    const newC = c + col;
    if (newR >= 0 && newR < height && newC >= 0 && newC < width) {
      dfs(newR, newC, grid, height, width, visited, island);
    }
  }
};
