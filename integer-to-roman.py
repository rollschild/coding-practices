# O(1) and O(1)
class Solution:
    def intToRoman(self, num: int) -> str:
        from collections import OrderedDict

        obj = OrderedDict()
        obj[1000] = "M"
        obj[900] = "CM"
        obj[500] = "D"
        obj[400] = "CD"
        obj[100] = "C"
        obj[90] = "XC"
        obj[50] = "L"
        obj[40] = "XL"
        obj[10] = "X"
        obj[9] = "IX"
        obj[5] = "V"
        obj[4] = "IV"
        obj[1] = "I"

        res = ""

        for key, value in obj.items():
            if num == 0:
                break
            while num >= key:
                res += value
                num -= key

        return res
