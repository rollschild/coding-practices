# Time: O(KlogN)
# K: min(k, mn)
# N: min(k, n)
class Solution:
    def kSmallestPairs(
        self, nums1: List[int], nums2: List[int], k: int
    ) -> List[List[int]]:
        if len(nums1) == 0 or len(nums2) == 0 or k == 0:
            return []

        queue = [((nums1[0] + nums2[0]), 0, 0)]
        res = []
        visited = {}

        while queue and len(res) < k:
            _, i, j = heapq.heappop(queue)
            res.append([nums1[i], nums2[j]])

            if j + 1 < len(nums2) and (i, j + 1) not in visited:
                heapq.heappush(queue, ((nums1[i] + nums2[j + 1]), i, j + 1))
                visited[(i, j + 1)] = True
            if i + 1 < len(nums1) and (i + 1, j) not in visited:
                heapq.heappush(queue, ((nums1[i + 1] + nums2[j]), i + 1, j))
                visited[(i + 1, j)] = True

        return res
