/* Both time and space: O(1) */
/**
 * @param {number} num
 * @return {string}
 */
var intToRoman = function(num) {
  const dict = new Map();
  dict.set(1000, "M");
  dict.set(900, "CM");
  dict.set(500, "D");
  dict.set(400, "CD");
  dict.set(100, "C");
  dict.set(90, "XC");
  dict.set(50, "L");
  dict.set(40, "XL");
  dict.set(10, "X");
  dict.set(9, "IX");
  dict.set(5, "V");
  dict.set(4, "IV");
  dict.set(1, "I");

  let res = "";

  for (const [key, value] of dict) {
    while (num >= key) {
      res += value;
      num -= key;
    }
  }

  return res;
};
