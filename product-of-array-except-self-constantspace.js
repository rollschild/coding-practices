/* Time: O(N)
 * space: O(1) */
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function(nums) {
  const len = nums.length;
  if (len === 0) return [];
  const res = new Array(len).fill(0);

  res[0] = 1;
  for (let l = 1; l < len; l++) {
    res[l] = nums[l - 1] * res[l - 1];
  }

  let R = 1;
  for (let r = len - 1; r >= 0; r--) {
    res[r] = res[r] * R;
    R *= nums[r];
  }

  return res;
};
