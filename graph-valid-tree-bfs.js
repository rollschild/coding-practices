/* Time: O(E) - E is number of edges
 * Space: O(N + E) - N is number of nodes
 */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function(n, edges) {
  if (n === 0) {
    return false;
  }
  // first construct the adjacency list
  const graph = {};
  for (const [startNode, endNode] of edges) {
    if (!graph.hasOwnProperty(startNode)) {
      graph[startNode] = [];
    }
    graph[startNode].push(endNode);
    if (!graph.hasOwnProperty(endNode)) {
      graph[endNode] = [];
    }
    graph[endNode].push(startNode);
  }

  // bfs
  const queue = [0];
  const parents = { 0: -1 };

  while (queue.length > 0) {
    const node = queue.shift();
    if (!graph[node]) continue;
    for (const adj of graph[node]) {
      if (node in parents && parents[node] === adj) continue;

      if (adj in parents) return false;

      parents[adj] = node;
      queue.push(adj);
    }
  }

  return Object.keys(parents).length === n;
};
