/* Time: O(N)
 * space: O(1) */
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
  const len = prices.length;
  if (len === 0) return 0;
  let minPrice = prices[0];
  let maxRev = 0;
  for (let i = 1; i < prices.length; i++) {
    minPrice = Math.min(minPrice, prices[i - 1]);
    maxRev = Math.max(prices[i] - minPrice, maxRev);
  }
  return maxRev;
};
