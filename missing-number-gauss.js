/**
 * @param {number[]} nums
 * @return {number}
 */

var missingNumber = function(nums) {
  const expectedSum = ((0 + nums.length) * (nums.length + 1)) / 2;
  let sum = nums.reduce((accum, curr) => accum + curr);
  return expectedSum - sum;
};
