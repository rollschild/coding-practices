/**
 * Initialize your data structure here.
 */
var Trie = function() {
  this.letter = "";
  this.children = []; // array of nodes
};

/**
 * Inserts a word into the trie.
 * @param {string} word
 * @return {void}
 */
Trie.prototype.insert = function(word) {
  let node = this;
  for (let i = 0; i < word.length; i++) {
    const letter = word[i];
    // check existence
    let existing = false;
    for (let j = 0; j < node.children.length; j++) {
      if (node.children[j].letter === letter) {
        // existing letter
        node = node.children[j];
        existing = true;
        break;
      }
    }
    if (!existing) {
      let newNode = new Trie();
      newNode.letter = letter;
      node.children.push(newNode);
      node = newNode;
    }
  }
  // need to put a terminating symbol
  const terminator = new Trie();
  terminator.letter = "*";
  node.children.push(terminator);
};

/**
 * Returns if the word is in the trie.
 * @param {string} word
 * @return {boolean}
 */
Trie.prototype.search = function(word) {
  let node = this;
  for (let i = 0; i < word.length; i++) {
    const letter = word[i];
    let existing = false;
    for (let j = 0; j < node.children.length; j++) {
      if (node.children[j].letter === letter) {
        existing = true;
        node = node.children[j];
        break;
      }
    }
    if (!existing) {
      return false;
    }
    if (i === word.length - 1) {
      // last letter also existing
      // check if terminator exists
      // node is this letter's node
      for (let k = 0; k < node.children.length; k++) {
        if (node.children[k].letter === "*") {
          return true;
        }
      }
      return false;
    }
  }
};

/**
 * Returns if there is any word in the trie that starts with the given prefix.
 * @param {string} prefix
 * @return {boolean}
 */
Trie.prototype.startsWith = function(prefix) {
  let node = this;
  for (let i = 0; i < prefix.length; i++) {
    const letter = prefix[i];
    let existing = false;
    for (let j = 0; j < node.children.length; j++) {
      if (node.children[j].letter === letter) {
        existing = true;
        node = node.children[j];
        break;
      }
    }
    if (!existing) {
      return false;
    }
  }
  return true;
};

/**
 * Your Trie object will be instantiated and called as such:
 * var obj = new Trie()
 * obj.insert(word)
 * var param_2 = obj.search(word)
 * var param_3 = obj.startsWith(prefix)
 */
