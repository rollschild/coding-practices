/* Time: O(n)
 * space: O(1)
 */
/**
 * @param {string} s
 * @return {number}
 */

var lengthOfLongestSubstringTwoDistinct = function(s) {
  // two pointer
  // sliding window???

  if (s.length <= 2) return s.length;
  const len = s.length;

  let left = 0;
  let right = 0;
  let maxLen = 2;

  const freqs = {};

  while (right < len) {
    if (Object.keys(freqs).length < 3) {
      freqs[s[right]] = right;
      right++;
    }

    if (Object.keys(freqs).length === 3) {
      // remove left value
      const leftIndex = Math.min(...Object.values(freqs));
      delete freqs[s[leftIndex]];
      left = leftIndex + 1;
    }

    maxLen = Math.max(maxLen, right - left);
  }

  return maxLen;
};
