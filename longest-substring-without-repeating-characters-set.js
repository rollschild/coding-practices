/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
  // sliding window + Set
  if (s.length === 0 || s.length === 1) return s.length;
  const len = s.length;

  let left = 0;
  let right = 0;
  const freqs = new Set();
  let maxLen = 0;

  while (left <= right && right < len) {
    const letter = s[right];
    if (!freqs.has(letter)) {
      freqs.add(letter);
      maxLen = Math.max(maxLen, right - left + 1);
      right++;
    } else {
      // left proceeds one position
      const leftLetter = s[left];
      left++;
      freqs.delete(leftLetter);
    }
  }

  return maxLen;
};
