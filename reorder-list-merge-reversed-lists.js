/* Time: O(n)
 * Space: O(1)
 */
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function(head) {
  // find middle
  // reverse
  // merge
  if (!head) return null;

  let slow = head;
  let fast = head;
  while (fast && fast.next) {
    slow = slow.next;
    fast = fast.next.next;
  }

  // slow is middle
  // reverse
  let prev = null;
  let curr = slow;
  while (curr) {
    const temp = curr.next;

    curr.next = prev;
    prev = curr;
    curr = temp;
  }
  // prev is the head of the reversed list?

  // merge
  let first = head;
  let second = prev;
  while (second.next) {
    const firstTemp = first.next;
    const secondTemp = second.next;
    first.next = second;
    second.next = firstTemp;
    first = firstTemp;
    second = secondTemp;
  }
};
