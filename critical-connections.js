/**
 * @param {number} n
 * @param {number[][]} connections
 * @return {number[][]}
 */
var criticalConnections = function(n, connections) {
  if (connections.length === 0) return [];
  const len = connections.length;
  const graph = {}; // directed

  const res = [];
  for (let i = 0; i < len; i++) {
    const [node1, node2] = connections[i];
    if (!graph[node1]) graph[node1] = [node2];
    else {
      if (!graph[node1].includes(node2)) graph[node1].push(node2);
    }
    if (!graph[node2]) graph[node2] = [node1];
    else {
      if (!graph[node2].includes(node1)) graph[node2].push(node1);
    }
  }

  const lowestRank = new Array(n);

  const currNode = 0;
  const currRank = 0;
  const prevNode = -1;
  const visited = {};

  dfs(currRank, currNode, prevNode, lowestRank, visited, graph, res);

  return res;
};

const dfs = (currRank, currNode, prevNode, lowestRank, visited, graph, res) => {
  lowestRank[currNode] = currRank;
  visited[currNode] = true;

  for (let nextNode of graph[currNode]) {
    if (nextNode === prevNode) continue;
    if (!visited[nextNode]) {
      dfs(currRank + 1, nextNode, currNode, lowestRank, visited, graph, res);
    }

    lowestRank[currNode] = Math.min(lowestRank[currNode], lowestRank[nextNode]);

    if (lowestRank[nextNode] >= currRank + 1) res.push([currNode, nextNode]);
  }
};
