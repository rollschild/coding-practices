/* Time: O(N)
 * space: O(1) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var maxProduct = function(nums) {
  const len = nums.length;
  if (len === 0) return 0;
  if (len === 1) return nums[0];
  let currProduct = 1;
  let maxProduct = -Infinity;

  for (let i = 0; i < len; i++) {
    currProduct *= nums[i];
    maxProduct = Math.max(currProduct, maxProduct);
    if (nums[i] === 0) {
      currProduct = 1;
    }
  }

  currProduct = 1;
  for (let i = len - 1; i >= 0; i--) {
    currProduct *= nums[i];
    maxProduct = Math.max(currProduct, maxProduct);
    if (nums[i] === 0) {
      currProduct = 1;
    }
  }
  return maxProduct;
};
