/**
 * @param {number} n
 * @return {number[][]}
 */
var generateMatrix = function(n) {
  const matrix = new Array(n);
  for (let i = 0; i < n; i++) {
    matrix[i] = new Array(n).fill(0);
  }

  const directions = [[0, 1], [1, 0], [0, -1], [-1, 0]];

  let row = 0;
  let col = 0;

  let num = 1;
  let dir = 0;
  while (row < n && col < n && num <= n * n) {
    matrix[row][col] = num;
    const nextRow = row + directions[dir][0];
    const nextCol = col + directions[dir][1];
    if (
      nextRow >= n ||
      nextRow < 0 ||
      nextCol >= n ||
      nextCol < 0 ||
      matrix[nextRow][nextCol] > 0
    ) {
      // change direction
      dir += 1;
      dir = dir % 4;
    }
    console.log(directions[dir][0], directions[dir][1]);
    row += directions[dir][0];
    col += directions[dir][1];
    num++;
  }

  return matrix;
};
