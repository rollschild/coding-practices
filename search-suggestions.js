/**
 * @param {string[]} products
 * @param {string} searchWord
 * @return {string[][]}
 */
var suggestedProducts = function(products, searchWord) {
  const root = new TrieNode();
  const res = [];
  for (let word of products) {
    root.add(word);
  }

  for (let pos = 0; pos < searchWord.length; pos++) {
    const word = searchWord.slice(0, pos + 1);
    const arr = root.search(word);
    res.push(arr.length > 3 ? arr.slice(0, 3) : arr);
  }
  return res;
};

function TrieNode() {
  this.letter = "";
  this.children = new Array(27); // children of TrieNode
}

TrieNode.prototype.search = function(word) {
  let root = this;
  const res = [];
  let resWord = "";
  for (let i = 0; i < word.length; i++) {
    const letter = word[i];
    const num = word.charCodeAt(i) - "a".charCodeAt(0) + 1;
    if (!root.children[num]) return res;
    root = root.children[num];
    resWord += letter;
    if (i === word.length - 1) {
      // dfs
      traverse(root, resWord, res);
    }
  }

  return res;
};

const traverse = (node, resWord, res) => {
  for (let letterNode of node.children) {
    if (!letterNode) continue;
    if (letterNode.letter === "*") {
      res.push(resWord);
      continue;
    }
    // resWord += letterNode.letter;
    traverse(letterNode, resWord + letterNode.letter, res);
  }
};

TrieNode.prototype.add = function(word) {
  let root = this;
  for (let i = 0; i < word.length; i++) {
    const letter = word[i];
    const num = word.charCodeAt(i) - "a".charCodeAt(0) + 1;
    if (!root.children[num]) {
      const newNode = new TrieNode();
      newNode.letter = letter;
      root.children[num] = newNode;
      root = newNode;
    } else {
      root = root.children[num];
    }
    if (i === word.length - 1) {
      // push * to children
      const endNode = new TrieNode();
      endNode.letter = "*";
      root.children[0] = endNode;
      break;
    }
  }
};

console.log(
  suggestedProducts(
    ["mobile", "mouse", "moneypot", "monitor", "mousepad"],
    "mouse",
  ),
);
