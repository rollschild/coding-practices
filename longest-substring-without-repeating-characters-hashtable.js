/* Time: O(n)
 * Space: O(min(n, m)), where n is the size of string and m is size of alphabet
 */
/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
  // sliding window + hashtable
  if (s.length === 0 || s.length === 1) return s.length;
  const len = s.length;

  let left = 0;
  let right = 0;
  const freqs = {};
  let maxLen = 0;

  while (left <= right && right < len) {
    const letter = s[right];
    if (!(letter in freqs)) {
      freqs[letter] = 1;
      maxLen = Math.max(maxLen, right - left + 1);
      right++;
    } else {
      // left proceeds one position
      const leftLetter = s[left];
      left++;
      if (freqs[leftLetter] === 1) {
        delete freqs[leftLetter];
      } else {
        freqs[leftLetter]--;
      }
    }
  }

  return maxLen;
};
