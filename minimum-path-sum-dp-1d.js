/**
 * @param {number[][]} grid
 * @return {number}
 */
var minPathSum = function(grid) {
  if (grid.length === 0) {
    return 0;
  }
  const height = grid.length;
  const width = grid[0].length;
  const dp = new Array(height);

  for (let i = height - 1; i >= 0; i--) {
    for (let j = width - 1; j >= 0; j--) {
      if (i === height - 1 && j === width - 1) {
        dp[j] = grid[i][j];
      } else if (i === height - 1) {
        dp[j] = grid[i][j] + dp[j + 1];
      } else if (j === width - 1) {
        dp[j] = grid[i][j] + dp[j];
      } else {
        dp[j] = grid[i][j] + Math.min(dp[j], dp[j + 1]);
      }
    }
  }

  return dp[0];
};
