/** time and space both O(n)
 */
/**
 * @param {number} n
 * @return {number}
 */

const memoization = function(pos, memo) {
  if (pos === 0) {
    return 0;
  }
  if (pos === 1) {
    return 1;
  }
  if (pos === 2) {
    return 2;
  }

  if (memo[pos] && memo[pos] > 0) {
    return memo[pos];
  }

  memo[pos] = memoization(pos - 1, memo) + memoization(pos - 2, memo);
  return memo[pos];
};
var climbStairs = function(n) {
  memo = new Array(n + 1);
  return memoization(n, memo);
};
