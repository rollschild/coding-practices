/**
 * @param {character[][]} board
 * @param {number[]} click
 * @return {character[][]}
 */

var updateBoard = function(board, click) {
  const height = board.length;
  const width = board[0].length;

  if (board[click[0]][click[1]] === "M") {
    board[click[0]][click[1]] = "X";
    return board;
  }

  const queue = [click];
  const directions = [
    [-1, 0],
    [1, 0],
    [0, -1],
    [0, 1],
    [-1, -1],
    [1, 1],
    [-1, 1],
    [1, -1],
  ];

  while (queue.length > 0) {
    const [row, col] = queue.shift();

    if (board[row][col] === "E") {
      let adjMine = 0;
      for (const [r, c] of directions) {
        const newRow = row + r;
        const newCol = col + c;
        if (newRow >= 0 && newRow < height && newCol >= 0 && newCol < width) {
          if (board[newRow][newCol] === "M") {
            adjMine++;
          }
        }
      }
      if (adjMine > 0) {
        board[row][col] = adjMine.toString();
      } else {
        board[row][col] = "B";
        for (const [r, c] of directions) {
          const newRow = row + r;
          const newCol = col + c;
          if (newRow >= 0 && newRow < height && newCol >= 0 && newCol < width) {
            queue.push([newRow, newCol]);
          }
        }
      }
    }
  }

  return board;
};
