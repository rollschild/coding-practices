from typing import List


class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:
        if len(grid) == 0:
            return 0

        height, width = (len(grid), len(grid[0]))
        dp = [[0 for i in range(width)] for j in range(height)]
        print(dp)

        for i in reversed(range(height)):
            for j in reversed(range(width)):
                if i == height - 1 and j == width - 1:
                    dp[i][j] = grid[i][j]
                elif i == height - 1:
                    dp[i][j] = grid[i][j] + dp[i][j + 1]
                elif j == width - 1:
                    dp[i][j] = grid[i][j] + dp[i + 1][j]
                else:
                    dp[i][j] = grid[i][j] + min(dp[i + 1][j], dp[i][j + 1])

        return dp[0][0]


sol = Solution()
print(sol.minPathSum([[1, 3, 1], [1, 5, 1], [4, 2, 1]]))
