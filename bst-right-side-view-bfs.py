# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def rightSideView(self, root: TreeNode) -> List[int]:
        if root is None:
            return []
        if root.left is None and root.right is None:
            return [root.val]

        values = []
        values.append(root)
        node = None
        res = []

        while len(values) > 0:
            # node = values.pop(0)
            l = len(values)
            for i in range(0, l):
                node = values.pop(0)
                if node.left is not None:
                    values.append(node.left)
                if node.right is not None:
                    values.append(node.right)

            res.append(node.val)

        return res
