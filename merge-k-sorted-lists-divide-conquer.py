"""
time complexity O(N * logK)
"""
# Definition for singly-linked list.
import math
from typing import List


class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def mergeTwoLists(self, l1, l2):
        if l1 is None:
            return l2
        if l2 is None:
            return l1

        if l1.val < l2.val:
            l1.next = self.mergeTwoLists(l1.next, l2)
            return l1
        else:
            l2.next = self.mergeTwoLists(l1, l2.next)
            return l2

    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        if len(lists) == 0:
            return None
        if len(lists) == 1:
            return lists[0]

        if len(lists) >= 2:
            mid = math.floor(len(lists) / 2)
            first_half = lists[:mid]
            second_half = lists[mid:]
            return self.mergeTwoLists(
                self.mergeKLists(first_half), self.mergeKLists(second_half)
            )
        return self.mergeTwoLists(lists[0], lists[1])
