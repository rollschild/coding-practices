"""
Time: O(N)
Space: O(N)
"""


class Solution:
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        from collections import deque

        nums_len = len(nums)

        def clean_deque(index):
            if deq and deq[0] == index - k:
                deq.popleft()
            while deq and nums[index] > nums[deq[-1]]:
                deq.pop()

        deq = deque()

        max_index = 0
        output = []

        # build initial deque
        for i in range(0, k):
            clean_deque(i)
            deq.append(i)
            if nums[i] > nums[max_index]:
                max_index = i

        output.append(nums[max_index])

        for j in range(k, nums_len):
            clean_deque(j)
            deq.append(j)
            output.append(nums[deq[0]])

        return output
