/** O(m * n)
 */
/**
 * @param {string} s1
 * @param {string} s2
 * @param {string} s3
 * @return {boolean}
 */
var isInterleave = function(s1, s2, s3) {
  if (s1.length === 0) return s2 === s3;
  if (s2.length === 0) return s1 === s3;
  if (s3.length === 0) return false;
  const memo = new Array(s1.length);
  for (let i = 0; i < s1.length; i++) {
    memo[i] = new Array(s2.length).fill(-1);
  }
  return checkInterleave(s1, 0, s2, 0, s3, 0, memo);
};

// returns an array
const checkInterleave = (s1, i, s2, j, s3, k, memo) => {
  if (i === s1.length) return s2.substring(j) === s3.substring(k);
  if (j === s2.length) return s1.substring(i) === s3.substring(k);

  if (memo[i][j] !== -1) {
    return memo[i][j];
  }
  let ans = false;
  if (
    (s1[i] === s3[k] && checkInterleave(s1, i + 1, s2, j, s3, k + 1, memo)) ||
    (s2[j] === s3[k] && checkInterleave(s1, i, s2, j + 1, s3, k + 1, memo))
  )
    ans = true;
  memo[i][j] = ans;
  return ans;
};

console.log(isInterleave("aabcc", "dbbca", "aadbbcbcac"));
