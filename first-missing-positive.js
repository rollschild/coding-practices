/* Time: O(N)
 * Space: O(1)
 */
/**
 * @param {number[]} nums
 * @return {number}
 */
var firstMissingPositive = function(nums) {
  const len = nums.length;
  if (len === 0) {
    return 1;
  }
  if (!nums.includes(1)) {
    return 1;
  }
  if (len === 1) {
    return 2;
  }

  for (let i = 0; i < len; i++) {
    if (nums[i] <= 0 || nums[i] > len) {
      nums[i] = 1;
    }
  }

  for (let i = 0; i < len; i++) {
    const value = Math.abs(nums[i]);
    if (value === len) {
      nums[0] = -Math.abs(nums[0]);
    } else {
      nums[value] = -Math.abs(nums[value]);
    }
  }

  for (let i = 1; i < len; i++) {
    if (nums[i] > 0) {
      return i;
    }
  }

  if (nums[0] > 0) {
    return len;
  }

  return len + 1;
};
