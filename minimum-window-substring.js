/* Time: O(S + T)
 * Space: O(T)
 */
/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var minWindow = function(s, t) {
  const len = s.length;
  if (len === 0 || t.length === 0) return "";

  let left = 0;
  let right = 0;

  const requiredObj = {};
  for (let i = 0; i < t.length; i++) {
    requiredObj[t[i]] = (requiredObj[t[i]] || 0) + 1;
  }
  const requiredUniqueLen = Object.keys(requiredObj).length;
  let window = [Infinity, ""];

  let count = 0;
  let obj = {};

  while (left <= right && right < len) {
    const letter = s[right];
    if (requiredObj[letter]) {
      obj[letter] = (obj[letter] || 0) + 1;

      if (obj[letter] === requiredObj[letter]) {
        // this letter is good enough;
        count++;
      }

      while (count === requiredUniqueLen && left <= right) {
        const aboutToRemove = s[left];
        if (right - left + 1 < window[0]) {
          window[0] = right - left + 1;
          window[1] = s.substring(left, right + 1);
        }

        if (requiredObj[aboutToRemove]) {
          obj[aboutToRemove]--;
          if (obj[aboutToRemove] < requiredObj[aboutToRemove]) {
            count--;
          }
        }

        left++;
      }
    }

    right++;
  }

  return window[0] === Infinity ? "" : window[1];
};
