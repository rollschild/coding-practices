/**
 * @param {string} beginWord
 * @param {string} endWord
 * @param {string[]} wordList
 * @return {string[][]}
 */
var findLadders = function(beginWord, endWord, wordList) {
  if (!wordList.includes(endWord)) return [];
  // const paths = [[beginWord]];
  let paths = {};
  paths[beginWord] = [[beginWord]];
  wordList = [...new Set(wordList)];

  let resList = [];
  const alphabet = "abcdefghijklmnopqrstuvwxyz";

  while (paths && Object.keys(paths).length > 0) {
    let newPaths = {};
    for (let currWord of Object.keys(paths)) {
      if (currWord === endWord) {
        for (let x of paths[currWord]) {
          resList.push([...new Set(x)]);
        }
        continue;
      }
      for (let i = 0; i < currWord.length; i++) {
        for (let j = 0; j < 26; j++) {
          const word =
            currWord.slice(0, i) + alphabet[j] + currWord.slice(i + 1);
          if (wordList.includes(word)) {
            if (!newPaths[word]) newPaths[word] = [];
            for (let x of paths[currWord]) {
              newPaths[word].push([...x, word]);
            }
          }
        }
      }
    }
    for (let key of [...new Set(Object.keys(newPaths))]) {
      wordList.splice(wordList.indexOf(key), 1);
    }
    paths = newPaths;
  }

  return resList;
};

console.log(findLadders("hot", "dog", ["hot", "dog", "dot"]));
console.log(findLadders("a", "c", ["a", "b", "c"]));
console.log(
  findLadders("qa", "sq", [
    "si",
    "go",
    "se",
    "cm",
    "so",
    "ph",
    "mt",
    "db",
    "mb",
    "sb",
    "kr",
    "ln",
    "tm",
    "le",
    "av",
    "sm",
    "ar",
    "ci",
    "ca",
    "br",
    "ti",
    "ba",
    "to",
    "ra",
    "fa",
    "yo",
    "ow",
    "sn",
    "ya",
    "cr",
    "po",
    "fe",
    "ho",
    "ma",
    "re",
    "or",
    "rn",
    "au",
    "ur",
    "rh",
    "sr",
    "tc",
    "lt",
    "lo",
    "as",
    "fr",
    "nb",
    "yb",
    "if",
    "pb",
    "ge",
    "th",
    "pm",
    "rb",
    "sh",
    "co",
    "ga",
    "li",
    "ha",
    "hz",
    "no",
    "bi",
    "di",
    "hi",
    "qa",
    "pi",
    "os",
    "uh",
    "wm",
    "an",
    "me",
    "mo",
    "na",
    "la",
    "st",
    "er",
    "sc",
    "ne",
    "mn",
    "mi",
    "am",
    "ex",
    "pt",
    "io",
    "be",
    "fm",
    "ta",
    "tb",
    "ni",
    "mr",
    "pa",
    "he",
    "lr",
    "sq",
    "ye",
  ]),
);
