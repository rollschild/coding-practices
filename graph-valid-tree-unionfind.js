/* Time: O(N)
 * space: O(N) */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function(n, edges) {
  if (n !== edges.length + 1) return false;
  const uf = new UnionFind(n);
  for (const [node1, node2] of edges) {
    if (!uf.union(node1, node2)) return false;
  }
  return true;
};

class UnionFind {
  constructor(size) {
    this.parents = new Array(size);
    for (let i = 0; i < size; i++) {
      this.parents[i] = i;
    }
    this.ranks = new Array(size).fill(0);
  }

  find(x) {
    if (this.parents[x] !== x) this.parents[x] = this.find(this.parents[x]);
    return this.parents[x];
  }

  union(x, y) {
    let xParent = this.find(x);
    let yParent = this.find(y);
    if (xParent === yParent) return false;

    if (this.ranks[xParent] < this.ranks[yParent]) {
      const temp = xParent;
      xParent = yParent;
      yParent = temp;
    }

    this.parents[yParent] = xParent;
    if (this.ranks[xParent] === this.ranks[yParent]) {
      this.ranks[xParent]++;
    }

    return true;
  }
}
