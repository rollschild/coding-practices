/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
  // binary search
  const len = nums.length;
  if (len === 0) return -1;
  let left = 0;
  let right = len - 1;
  let pivot = Math.floor((right + left) / 2);
  let sorted = [...nums];
  let offset = 0;
  while (pivot >= 0 && pivot < len && left <= right) {
    if (pivot > 0 && nums[pivot] < nums[pivot - 1]) {
      sorted = [...nums.slice(pivot), ...nums.slice(0, pivot)];
      offset = pivot;
      break;
    } else if (pivot < len - 1 && nums[pivot] > nums[pivot + 1]) {
      sorted = [...nums.slice(pivot + 1), ...nums.slice(0, pivot + 1)];
      offset = pivot + 1;
      break;
    } else if (nums[pivot] < nums[right]) {
      // left side
      right = pivot - 1;
      pivot = Math.floor((right + left) / 2);
    } else {
      left = pivot + 1;
      pivot = Math.floor((right + left) / 2);
    }
  }

  const index = findItem(sorted, target);

  return index >= 0
    ? index + offset >= len
      ? index + offset - len
      : index + offset
    : -1;
};

const findItem = (arr, target) => {
  const len = arr.length;
  let left = 0;
  let right = len - 1;
  let pivot = Math.floor((right + left) / 2);

  while (left >= 0 && right < len && left <= right) {
    if (arr[pivot] === target) {
      return pivot;
    } else if (arr[pivot] > target) {
      right = pivot - 1;
      pivot = Math.floor((left + right) / 2);
    } else {
      left = pivot + 1;
      pivot = Math.floor((left + right) / 2);
    }
  }

  return -1;
};
