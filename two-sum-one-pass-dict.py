from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        obj = {}
        for i, num in enumerate(nums):
            goal = target - num
            if goal in obj:
                return [obj[goal], i]
            else:
                obj[num] = i
