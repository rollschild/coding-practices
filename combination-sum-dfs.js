/* Time: O(len ^ (target/(min of candidates)))
 */
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
var combinationSum = function(candidates, target) {
  const len = candidates.length;
  if (len === 0) return [];

  const res = [];
  candidates.sort();
  dfs(candidates, target, 0, [], res);

  return res;
};

const dfs = (arr, target, index, path, res) => {
  if (target < 0) return;
  if (target === 0) {
    res.push(path);
    return;
  }

  for (let i = index; i < arr.length; i++) {
    dfs(arr, target - arr[i], i, [...path, arr[i]], res);
  }
  return;
};
