/**
 * Initialize your data structure here.
 * @param {number} n
 */

var TicTacToe = function(n) {
  this.matrix = new Array(n);
  for (let i = 0; i < n; i++) {
    this.matrix[i] = new Array(n).fill(0);
  }
  this.size = n;
};

/**
 * Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. 
 * @param {number} row 
 * @param {number} col 
 * @param {number} player
 * @return {number}
 */

TicTacToe.prototype.move = function(row, col, player) {
  this.matrix[row][col] = player;

  // horizontal
  if (this.matrix[row].every(val => val === player)) return player;

  // vertical
  const vertical = [];
  this.matrix.forEach(row => vertical.push(row[col]));
  if (vertical.every(val => val === player)) return player;

  // diagonal
  if (row === col) {
    let pos = 0;
    const diagonal = [];
    while (pos < this.size) {
      diagonal.push(this.matrix[pos][pos]);
      pos++;
    }
    if (diagonal.every(val => val === player)) return player;
  }
  if (row + col === this.size - 1) {
    let pos = 0;
    const diagonal = [];
    while (pos < this.size) {
      diagonal.push(this.matrix[pos][this.size - pos - 1]);
      pos++;
    }
    if (diagonal.every(val => val === player)) return player;
  }

  return 0;
};

/**
 * Your TicTacToe object will be instantiated and called as such:
 * var obj = new TicTacToe(n)
 * var param_1 = obj.move(row,col,player)
 */
