/* time O(n)
 * space O(n)
 */
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
  if (nums.length === 2) {
    return [0, 1];
  }

  let obj = {};
  for (let i = 0; i < nums.length; i++) {
    obj[nums[i]] = i;
  }

  for (let j = 0; j < nums.length; j++) {
    const goal = target - nums[j];
    if (obj[goal] != undefined && obj[goal] !== j) {
      return [j, obj[goal]];
    }
  }
};
