/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
  if (s.length === 0) return true;

  const stack = [];
  const open = ["(", "{", "["];
  const close = [")", "}", "]"];

  for (let i = 0; i < s.length; i++) {
    const letter = s[i];
    if (open.includes(letter)) {
      stack.push(letter);
    } else {
      const symb = stack.pop();
      if (symb !== open[close.indexOf(letter)]) return false;
    }
  }

  return stack.length === 0;
};
