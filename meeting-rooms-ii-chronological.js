/* Time: O(NlogN)
 * space: O(N) */
/**
 * @param {number[][]} intervals
 * @return {number}
 */
var minMeetingRooms = function(intervals) {
  const len = intervals.length;
  if (len === 0) return 0;
  if (len === 1) return 1;

  const starts = intervals.map(meeting => meeting[0]).sort((a, b) => a - b);
  const ends = intervals.map(meeting => meeting[1]).sort((a, b) => a - b);

  let startPtr = 0;
  let endPtr = 0;
  let rooms = 0;

  while (startPtr < len && endPtr < len) {
    if (starts[startPtr] >= ends[endPtr]) {
      // some meeting ends before another starts
      // empty room!
      rooms--;
      endPtr++;
    }

    rooms++;
    startPtr++;
  }

  return rooms;
};
