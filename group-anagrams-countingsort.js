/* Time: O(NK)
 * Space: O(NK)
 */
/**
 * @param {string[]} strs
 * @return {string[][]}
 */
var groupAnagrams = function(strs) {
  const obj = {};

  for (const str of strs) {
    const sorted = countingSort(str);
    if (!(sorted in obj)) {
      obj[sorted] = [str];
    } else {
      obj[sorted].push(str);
    }
  }

  return Object.values(obj);
};

const countingSort = str => {
  const arr = new Array(26).fill(0);
  for (let i = 0; i < str.length; i++) {
    const pos = str.charCodeAt(i) - "a".charCodeAt(0);
    arr[pos]++;
  }

  let res = "";
  for (let j = 0; j < 26; j++) {
    if (arr[j] > 0) {
      res += String.fromCharCode(j + "a".charCodeAt(0)).repeat(arr[j]);
    }
  }

  return res;
};
