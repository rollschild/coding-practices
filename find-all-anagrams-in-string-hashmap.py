class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        s_len = len(s)
        p_len = len(p)
        if s_len < p_len:
            return []

        p_dict = Counter(p)
        str_dict = Counter()
        res = []

        for i in range(0, s_len):
            letter = s[i]
            str_dict[letter] = str_dict.get(letter, 0) + 1
            if i >= p_len:
                pos = i - p_len
                l = s[pos]
                if str_dict[l] == 1:
                    del str_dict[l]
                else:
                    str_dict[l] -= 1
            if str_dict == p_dict:
                res.append(i - p_len + 1)

        return res
