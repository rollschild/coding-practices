// Implementatin of Insertion Sort
// Time: O(n^2) - worst case when arr is reverse sorted
// space: O(1)
const insertionSort = arr => {
  const len = arr.length;
  if (len === 0) return;

  for (let i = 1; i < len; i++) {
    const key = arr[i];

    let j = i - 1;
    while (j >= 0 && arr[j] > key) {
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = key;
  }

  return;
};

let arr = [2037, -1, 0, 199, 12, 13, 13, 100, 2037, -100, 6];
console.log("before sort:", arr);
insertionSort(arr);
console.log("after sort:", arr);
