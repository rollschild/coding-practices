/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
  if (strs.length === 0) {
    return "";
  }
  if (strs.length === 1) {
    return strs[0];
  }

  let len = strs[0].length;
  for (let i = 1; i < strs.length; i++) {
    if (len > strs[i].length) {
      len = strs[i].length;
    }
  }

  let head = 0;
  let tail = len;

  while (head < tail) {
    const common = strs[0].slice(head, tail);
    for (let j = 1; j < strs.length; j++) {
      str = strs[j].slice(head, tail);
      if (common !== str) {
        break;
      }
      if (j === strs.length - 1) {
        return common;
      }
    }
    tail--;
  }

  return "";
};
