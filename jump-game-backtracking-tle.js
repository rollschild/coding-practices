/* Time: O(2^N)
 * Space: O(N) - recursion requirs extra space for stack frames
 */
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function(nums) {
  if (nums.length === 1) return true;
  for (let i = 0; i < nums.length; i++) {
    const steps = nums[i];
    if (steps === 0) return false;
    for (let step = 1; step <= steps; step++) {
      if (step + i === nums.length) return true;
      if (step + i > nums.length) break;
      if (canJump(nums.slice(step + i, nums.length))) return true;
    }
  }

  return false;
};
