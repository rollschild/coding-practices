/* Time: O(n), where n is number of nodes
 * Space: for recursive calls space complexity is bound to height of tree;
 * worst case O(n); on average O(logn)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
var isSymmetric = function(root) {
  return traverse(root, root);
};

const traverse = (left, right) => {
  if (left === null && right === null) return true;
  if (left === null || right === null) return false;

  return (
    left.val === right.val &&
    traverse(left.left, right.right) &&
    traverse(left.right, right.left)
  );
};
