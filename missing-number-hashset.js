/* Time: O(N)
 * space: O(N) */
/**
 * @param {number[]} nums
 * @return {number}
 */
var missingNumber = function(nums) {
  const hash = new Set();
  for (const num of nums) {
    hash.add(num);
  }

  for (let i = 0; i <= nums.length; i++) {
    if (!hash.has(i)) return i;
  }

  return 0;
};
