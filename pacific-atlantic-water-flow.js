/* Time: O(m*n)
 * space: O(m*n) */
/**
 * @param {number[][]} matrix
 * @return {number[][]}
 */
var pacificAtlantic = function(matrix) {
  const height = matrix.length;
  if (height === 0) return [];
  const width = matrix[0].length;

  const pacificVisited = new Set();
  const atlanticVisited = new Set();
  const directions = [[-1, 0], [1, 0], [0, 1], [0, -1]];

  const dfs = ([row, col], visited) => {
    const key = row * width + col;
    if (visited.has(key)) return;
    visited.add(key);

    for (const [r, c] of directions) {
      const newRow = row + r;
      const newCol = col + c;
      if (
        newRow >= 0 &&
        newRow < height &&
        newCol >= 0 &&
        newCol < width &&
        matrix[row][col] <= matrix[newRow][newCol]
      ) {
        dfs([newRow, newCol], visited);
      }
    }
  };

  for (let r = 0; r < height; r++) {
    dfs([r, 0], pacificVisited);
    dfs([r, width - 1], atlanticVisited);
  }
  for (let c = 0; c < width; c++) {
    dfs([0, c], pacificVisited);
    dfs([height - 1, c], atlanticVisited);
  }

  return Array.from(pacificVisited)
    .filter(item => atlanticVisited.has(item))
    .map(coor => [Math.floor(coor / width), coor % width]);
};
