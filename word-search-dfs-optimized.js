/* Space: O(L)
 */
/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function(board, word) {
  const height = board.length;
  const width = board[0].length;
  const first = word[0];

  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      const letter = board[row][col];
      if (letter === first) {
        if (dfs(board, [row, col], word, 0)) {
          return true;
        }
      }
    }
  }

  return false;
};

const dfs = (board, pos, word, wordPos) => {
  if (wordPos >= word.length) return false;
  const [row, col] = pos;
  const height = board.length;
  const width = board[0].length;
  const letter = board[row][col];
  if (letter !== word[wordPos]) {
    return false;
  }
  if (wordPos === word.length - 1) return true;
  board[row][col] = "#";
  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];
  for (const [r, c] of directions) {
    const newRow = row + r;
    const newCol = col + c;
    if (
      newRow >= 0 &&
      newRow < height &&
      newCol >= 0 &&
      newCol < width &&
      board[newRow][newCol] !== "#"
    ) {
      if (dfs(board, [newRow, newCol], word, wordPos + 1)) return true;
    }
  }
  board[row][col] = letter;

  return false;
};
