/* Time: O(k*n), where k is the length of str, n is number of words
 * space: O(n)
 */
/**
 * @param {string} s
 * @param {string[]} words
 * @return {number[]}
 */
var findSubstring = function(s, words) {
  const freqs = {};
  for (let word of words) {
    freqs[word] = (freqs[word] || 0) + 1;
  }

  const res = [];
  if (words.length === 0 || s.length === 0) return res;

  const strLen = s.length;
  const numOfWords = words.length;
  const wordLen = words[0].length;
  const windowLen = strLen + 1 - numOfWords * wordLen;
  for (let i = 0; i < windowLen; i++) {
    let j = 0;
    const visited = {};
    while (j < numOfWords) {
      const str = s.substring(i + j * wordLen, i + (j + 1) * wordLen);
      if (!freqs[str]) break;

      visited[str] = (visited[str] || 0) + 1;

      if (visited[str] > freqs[str]) break;

      j++;
    }
    if (j === numOfWords) res.push(i);
  }

  return res;
};
