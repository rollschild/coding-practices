/* Time: O(N + M), where M is number of edges
 * space: O(N) */
/**
 * // Definition for a Node.
 * function Node(val, neighbors) {
 *    this.val = val === undefined ? 0 : val;
 *    this.neighbors = neighbors === undefined ? [] : neighbors;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
var cloneGraph = function(node) {
  return dfs(node, {});
};

const dfs = (node, visited) => {
  if (!node) return null;
  if (node.val in visited) return visited[node.val];
  const newNode = new Node(node.val, []);
  visited[newNode.val] = newNode;
  for (const adj of node.neighbors) {
    newNode.neighbors.push(dfs(adj, visited));
  }

  return newNode;
};
