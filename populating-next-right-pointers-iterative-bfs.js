/**
 * // Definition for a Node.
 * function Node(val, left, right, next) {
 *    this.val = val === undefined ? null : val;
 *    this.left = left === undefined ? null : left;
 *    this.right = right === undefined ? null : right;
 *    this.next = next === undefined ? null : next;
 * };
 */
/**
 * @param {Node} root
 * @return {Node}
 */
var connect = function(root) {
  if (!root) return null;
  let parentArr = [];
  parentArr.push(root);
  while (parentArr.length > 0) {
    let childArr = [];
    let node = parentArr.shift();
    while (parentArr.length > 0) {
      node.next = parentArr[0];
      if (node.left) childArr.push(node.left);
      if (node.right) childArr.push(node.right);
      node = parentArr.shift();
    }
    node.next = null;
    if (node.left) childArr.push(node.left);
    if (node.right) childArr.push(node.right);
    parentArr = [...childArr];
  }
  return root;
};
