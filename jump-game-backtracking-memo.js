/* Time: O(N ^ 2)
 * Space: O(2N)
 */
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function(nums) {
  const len = nums.length;
  if (len === 0) return true;
  const constants = {
    GOOD: 1,
    BAD: 0,
    UNKNOWN: -1,
  };
  const memo = new Array(len).fill(constants.UNKNOWN);
  memo[len - 1] = constants.GOOD;

  return canJumpFromPosition(nums, 0, memo, constants);
};

const canJumpFromPosition = (nums, position, memo, constants) => {
  if (memo[position] !== constants.UNKNOWN)
    return memo[position] === constants.GOOD ? true : false;
  const steps = nums[position];
  const furthest = Math.min(position + steps, nums.length - 1);

  for (let i = position + 1; i <= furthest; i++) {
    if (canJumpFromPosition(nums, i, memo, constants)) {
      memo[position] = constants.GOOD;
      return true;
    }
  }

  memo[position] = constants.BAD;
  return false;
};
