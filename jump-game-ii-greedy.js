/* Time: O(n)
 * Space: O(1)
 */
/**
 * @param {number[]} nums
 * @return {number}
 */

var jump = function(nums) {
  const len = nums.length;
  if (len <= 1) return 0;

  let maxPos = nums[0];
  let maxDuringJump = nums[0];
  let jumps = 1;

  for (let i = 1; i < len; i++) {
    if (maxDuringJump < i) {
      jumps++;
      maxDuringJump = maxPos;
    }
    maxPos = Math.max(maxPos, i + nums[i]);
  }

  return jumps;
};
