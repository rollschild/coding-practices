/**
 * @param {number[]} sticks
 * @return {number}
 */
var connectSticks = function(sticks) {
  if (sticks.length === 0) return 0;
  if (sticks.length === 1) return 0;
  const queue = new PrimaryQueue();

  for (let stick of sticks) {
    queue.add(stick);
  }

  let res = 0;
  while (queue.size() > 1) {
    const first = queue.pop();
    const second = queue.pop();
    res += first + second;
    queue.add(first + second);
  }

  return res;
};

function PrimaryQueue() {
  this.items = [];
}

PrimaryQueue.prototype.size = function() {
  return this.items.length;
};

PrimaryQueue.prototype.add = function(item) {
  this.items.push(item);

  const len = this.items.length;
  let pos = len - 1;
  while (pos > 0 && item < this.items[this.parent(pos)]) {
    // bubble up
    const parentPos = this.parent(pos);
    const tmp = this.items[parentPos];
    this.items[parentPos] = item;
    this.items[pos] = tmp;
    pos = parentPos;
  }
};

PrimaryQueue.prototype.parent = function(pos) {
  return (pos - 1) >> 1;
};

PrimaryQueue.prototype.pop = function() {
  let item = 0;
  if (this.size() > 0) item = this.items[0];
  let pos = 0;
  this.items[pos] = this.items[this.size() - 1];
  this.items.pop();
  let leftChild = (pos << 1) + 1;
  let rightChild = (pos << 1) + 2;

  while (
    (leftChild < this.size() && this.items[pos] > this.items[leftChild]) ||
    (rightChild < this.size() && this.items[pos] > this.items[rightChild])
  ) {
    const minPos =
      rightChild < this.size() && this.items[rightChild] < this.items[leftChild]
        ? rightChild
        : leftChild;
    const tmp = this.items[minPos];
    this.items[minPos] = this.items[pos];
    this.items[pos] = tmp;
    pos = minPos;
    leftChild = (pos << 1) + 1;
    rightChild = (pos << 1) + 2;
  }

  return item;
};

console.log(
  connectSticks([3354, 4316, 3259, 4904, 4598, 474, 3166, 6322, 8080, 9009]),
);
