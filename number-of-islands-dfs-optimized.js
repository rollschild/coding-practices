/* Time: O(M * N)
 * space: O(M * N) */
/**
 * @param {character[][]} grid
 * @return {number}
 */
var numIslands = function(grid) {
  // dfs
  const height = grid.length;
  if (height === 0) return 0;
  const width = grid[0].length;
  let num = 0;
  const visited = {};

  const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];

  const dfs = (r, c) => {
    visited[r * width + c] = true;
    for (const [row, col] of directions) {
      const newRow = r + row;
      const newCol = c + col;
      if (
        !visited[newRow * width + newCol] &&
        newRow >= 0 &&
        newCol >= 0 &&
        newRow < height &&
        newCol < width &&
        grid[newRow][newCol] === "1"
      ) {
        dfs(newRow, newCol);
      }
    }
  };

  for (let r = 0; r < height; r++) {
    for (let c = 0; c < width; c++) {
      if (grid[r][c] === "1" && !visited[r * width + c]) {
        num++;
        dfs(r, c);
      }
    }
  }

  return num;
};
