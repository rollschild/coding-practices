/**
 * @param {string} s
 * @return {number}
 */
var numDecodings = function(s) {
  // recursion with memoization
  if (s.length === 0 || s[0] === "0") return 0;
  return decode(0, s, {});
};

const decode = (pos, s, memo) => {
  if (pos === s.length) return 1;
  if (s[pos] === "0") return 0;
  if (pos === s.length - 1) return 1;
  if (memo[pos]) return memo[pos];

  let res = decode(pos + 1, s, memo);
  const num = Number(s.substring(pos, pos + 2));
  if (num > 0 && num <= 26) {
    res += decode(pos + 2, s, memo);
  }
  memo[pos] = res;
  return res;
};
