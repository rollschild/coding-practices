/* Time: O(M * (4 * 3 ^ (L - 1)))
 * space: O(N) */
/**
 * @param {character[][]} board
 * @param {string[]} words
 * @return {string[]}
 */
var findWords = function(board, words) {
  const height = board.length;
  if (height === 0) return [];
  const width = board[0].length;
  const SYMBOL = "$";
  const res = [];

  const buildTrie = () => {
    const root = {};

    for (const word of words) {
      let node = root;
      for (const letter of word) {
        if (!(letter in node)) node[letter] = {};
        node = node[letter];
      }
      node[SYMBOL] = word;
    }

    return root;
  };

  const backtrack = (node, r, c) => {
    if (!node) return;
    if (node[SYMBOL]) {
      res.push(node[SYMBOL]);
      node[SYMBOL] = null;
    }

    if (r < 0 || r >= height || c < 0 || c >= width) return;
    const cell = board[r][c];
    if (!node[cell]) return;

    board[r][c] = "#";

    backtrack(node[cell], r + 1, c);
    backtrack(node[cell], r - 1, c);
    backtrack(node[cell], r, c + 1);
    backtrack(node[cell], r, c - 1);

    board[r][c] = cell;

    return;
  };

  const root = buildTrie();
  for (let r = 0; r < height; r++) {
    for (let c = 0; c < width; c++) {
      backtrack(root, r, c);
    }
  }

  return res;
};
