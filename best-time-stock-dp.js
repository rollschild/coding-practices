/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
  if (prices.length === 0) return 0;
  const dp = new Array(prices.length).fill(0);
  let minPrice = prices[0];
  for (let i = 0; i < prices.length; i++) {
    if (i === 0) {
      dp[i] = 0;
      continue;
    }
    if (prices[i] < minPrice) minPrice = prices[i];
    dp[i] = Math.max(dp[i], dp[i - 1], prices[i] - minPrice);
  }
  return dp[prices.length - 1];
};
