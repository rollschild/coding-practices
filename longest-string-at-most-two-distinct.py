class Solution:
    def lengthOfLongestSubstringTwoDistinct(self, s: str) -> int:
        if len(s) <= 2:
            return len(s)

        left = 0
        right = 0
        max_len = 2

        freqs = {}

        while right < len(s):
            if len(freqs) < 3:
                freqs[s[right]] = right
                right = right + 1

            if len(freqs) == 3:
                # delete left most character
                leftIndex = min(list(freqs.values()))
                del freqs[s[leftIndex]]
                left = leftIndex + 1

            max_len = max(max_len, right - left)

        return max_len
