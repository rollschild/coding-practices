/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var topKFrequent = function(nums, k) {
  const freqs = new Array(nums.length + 1);

  const freqMap = {};
  for (let item of nums) {
    freqMap[item] = (freqMap[item] || 0) + 1;
  }

  for (let key in freqMap) {
    const freq = freqMap[key];
    if (!freqs[freq]) {
      freqs[freq] = [];
    }
    freqs[freq].push(key);
  }

  let res = [];
  let counter = 0;
  let pos = freqs.length - 1;
  while (pos >= 0 && counter < k) {
    if (freqs[pos] && freqs[pos].length > 0) {
      if (freqs[pos].length > k) {
        res = res.concat(freqs[pos].slice(0, k));
        counter += k;
      } else {
        res = res.concat(freqs[pos]);
        counter += freqs[pos].length;
      }
    }
    pos--;
  }
  return res;
};
console.log(topKFrequent([4, 1, -1, 2, -1, 2, 3], 2));
