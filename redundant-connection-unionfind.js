/* Time: O(N)
 * space: O(N) */
/**
 * @param {number[][]} edges
 * @return {number[]}
 */
var findRedundantConnection = function(edges) {
  const uf = new UnionFind(edges.length);

  for (const [x, y] of edges) {
    if (!uf.union(x, y)) return [x, y];
  }
};

class UnionFind {
  constructor(size) {
    this.parents = new Array(size);
    for (let i = 0; i < size; i++) {
      this.parents[i] = i;
    }
    this.rank = new Array(size).fill(0);
  }

  find(x) {
    if (this.parents[x] !== x) {
      this.parents[x] = this.find(this.parents[x]);
    }
    return this.parents[x];
  }

  union(x, y) {
    let xParent = this.find(x);
    let yParent = this.find(y);

    if (xParent === yParent) return false;
    if (this.rank[xParent] < this.rank[yParent]) {
      const temp = xParent;
      xParent = yParent;
      yParent = temp;
    }

    this.parents[yParent] = xParent;
    if (this.rank[xParent] === this.rank[yParent]) {
      this.rank[xParent]++;
    }

    return true;
  }
}
