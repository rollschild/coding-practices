// Time: O(1)
/**
 * @param {number} num
 * @return {string}
 */
var intToRoman = function(num) {
  let res = "";

  while (num > 0) {
    if (num >= 1000) {
      res += "M".repeat(Math.floor(num / 1000));
      num = num % 1000;
    } else if (num >= 900) {
      res += "CM";
      num -= 900;
    } else if (num >= 500) {
      res += "D".repeat(Math.floor(num / 500));
      num = num % 500;
    } else if (num >= 400) {
      res += "CD";
      num -= 400;
    } else if (num >= 100) {
      res += "C".repeat(Math.floor(num / 100));
      num = num % 100;
    } else if (num >= 90) {
      res += "XC";
      num -= 90;
    } else if (num >= 50) {
      res += "L".repeat(Math.floor(num / 50));
      num = num % 50;
    } else if (num >= 40) {
      res += "XL";
      num -= 40;
    } else if (num >= 10) {
      res += "X".repeat(Math.floor(num / 10));
      num = num % 10;
    } else if (num >= 9) {
      res += "IX";
      num -= 9;
    } else if (num >= 5) {
      res += "V".repeat(Math.floor(num / 5));
      num = num % 5;
    } else if (num >= 4) {
      res += "IV";
      num = num - 4;
    } else {
      res += "I".repeat(num);
      num = 0;
    }
  }
  return res;
};
