/* Time: O(mn)
 * space: O(m+n)
 */
/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var setZeroes = function(matrix) {
  const height = matrix.length;
  if (height === 0) return;
  const width = matrix[0].length;

  const rows = new Set();
  const cols = new Set();

  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      if (matrix[row][col] === 0) {
        rows.add(row);
        cols.add(col);
      }
    }
  }

  for (const r of rows) {
    for (let col = 0; col < width; col++) {
      matrix[r][col] = 0;
    }
  }
  for (const c of cols) {
    for (let row = 0; row < height; row++) {
      matrix[row][c] = 0;
    }
  }
};
