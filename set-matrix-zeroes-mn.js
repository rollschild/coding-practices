/* Time: O(mn)
 * space: O(mn)
 */
/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
var setZeroes = function(matrix) {
  const height = matrix.length;
  if (height === 0) return;
  const width = matrix[0].length;
  const zeroes = [];

  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      if (matrix[row][col] === 0) zeroes.push([row, col]);
    }
  }

  for (const [r, c] of zeroes) {
    console.log(r, c);
    matrix[r] = new Array(width).fill(0);
    for (let row = 0; row < height; row++) {
      matrix[row][c] = 0;
    }
  }
};
