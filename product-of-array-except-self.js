/* Time: O(N)
 * space: O(N) */
/**
 * @param {number[]} nums
 * @return {number[]}
 */
var productExceptSelf = function(nums) {
  const len = nums.length;
  if (len === 0) return [];
  const res = new Array(len).fill(0);
  const left = new Array(len).fill(0);
  const right = new Array(len).fill(0);
  left[0] = 1;
  right[len - 1] = 1;

  for (let i = 1; i < len; i++) {
    left[i] = left[i - 1] * nums[i - 1];
  }

  for (let j = len - 2; j >= 0; j--) {
    right[j] = nums[j + 1] * right[j + 1];
  }

  for (let k = 0; k < len; k++) {
    res[k] = left[k] * right[k];
  }

  return res;
};
