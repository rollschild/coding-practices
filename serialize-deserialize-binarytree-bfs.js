/* Time and space: O(n)
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */

var serialize = function(root) {
  // bfs?
  if (!root) return "[null]";
  const queue = [];
  const arr = [];
  queue.push(root);
  arr.push(root.val);

  while (queue.length > 0) {
    const node = queue.shift();
    if (node.left) {
      queue.push(node.left);
      arr.push(node.left.val.toString());
    } else {
      arr.push("null");
    }
    if (node.right) {
      queue.push(node.right);
      arr.push(node.right.val.toString());
    } else {
      arr.push("null");
    }
  }

  return "[" + arr.join(",") + "]";
};

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */
var deserialize = function(data) {
  if (data.length <= 2) return null;
  const arr = data.slice(1, data.length - 1).split(",");
  const queue = [];
  if (arr[0] === "null") return null;
  const root = new TreeNode(Number(arr[0]));
  queue.push(root);
  let pos = 0;

  while (queue.length > 0 && pos < arr.length) {
    const node = queue.shift();
    const leftPos = pos * 2 + 1;
    const rightPos = pos * 2 + 2;

    if (leftPos < arr.length && arr[leftPos] !== "null") {
      const leftChild = new TreeNode(Number(arr[leftPos]));
      node.left = leftChild;
      queue.push(leftChild);
    }
    if (rightPos < arr.length && arr[rightPos] !== "null") {
      const rightChild = new TreeNode(Number(arr[rightPos]));
      node.right = rightChild;
      queue.push(rightChild);
    }
    pos++;
  }
  return root;
};

/**
 * Your functions will be called as such:
 * deserialize(serialize(root));
 */
