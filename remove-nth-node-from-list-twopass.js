/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function(head, n) {
  let len = 0;
  if (!head) return head;

  let node = head;
  while (node) {
    len++;
    node = node.next;
  }

  if (n === len) {
    return head.next;
  }

  let root = head;
  let pos = 0;
  while (root) {
    if (pos === len - n - 1) {
      const toBeRemoved = root.next;
      const next = root.next.next;
      root.next = next;
      return head;
    }
    pos++;
    root = root.next;
  }
};
