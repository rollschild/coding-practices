/**
 * @param {string} s1
 * @param {string} s2
 * @param {string} s3
 * @return {boolean}
 */
var isInterleave = function(s1, s2, s3) {
  if (s3.length === 0) return false;
  return checkInterleave(s1, 0, s2, 0, "", s3);
};

// returns an array
const checkInterleave = (s1, i, s2, j, res, s3) => {
  console.log("res:", res);
  if (res === s3 && i === s1.length && j === s2.length) {
    return true;
  }

  let ans = false;
  if (i < s1.length) {
    ans |= checkInterleave(s1, i + 1, s2, j, res + s1[i], s3);
  }
  if (j < s2.length) {
    ans |= checkInterleave(s1, i, s2, j + 1, res + s2[j], s3);
  }
  return ans;
};

console.log(isInterleave("aabcc", "dbbca", "aadbbcbcac"));
