/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */

var serialize = function(root) {
  const arr = [];
  dfs(root, arr);
  return "[" + arr.join(",") + "]";
};

const dfs = (node, arr) => {
  if (!node) {
    arr.push("null");
    return;
  }
  arr.push(node.val.toString());
  dfs(node.left, arr);
  dfs(node.right, arr);
  return;
};

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */

var deserialize = function(data) {
  if (data.length <= 2) return null;
  const arr = data.slice(1, data.length - 1).split(",");
  const root = rdfs(arr);
  return root;
};

const rdfs = arr => {
  if (arr.length === 0) return null;
  const val = arr.shift();
  if (val === "null") return null;
  const node = new TreeNode(Number(val));
  node.left = rdfs(arr);
  node.right = rdfs(arr);
  return node;
};

/**
 * Your functions will be called as such:
 * deserialize(serialize(root));
 */
