class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        dp = [0 for item in range(0, len(triangle) + 1)]

        # bottom up
        for r in reversed(range(0, len(triangle))):
            for c in range(0, len(triangle[r])):
                dp[c] = min(dp[c], dp[c + 1]) + triangle[r][c]

        return dp[0]
