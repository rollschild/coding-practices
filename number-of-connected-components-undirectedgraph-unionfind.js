/* Time: O(n + m), where m is number of edges
 * Space: O(n)
 */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {number}
 */

var countComponents = function(n, edges) {
  const uf = new UnionFind(n);

  for (const [node1, node2] of edges) {
    uf.union(node1, node2);
  }

  const parent = [...uf.getParent()];
  const roots = new Set();

  parent.forEach(node => {
    roots.add(uf.find(node));
  });

  return roots.size;
};

class UnionFind {
  constructor(num) {
    this.parent = new Array(num);
    for (let i = 0; i < num; i++) {
      this.parent[i] = i;
    }
    this.rank = new Array(num).fill(0);
  }

  find(x) {
    if (x !== this.parent[x]) {
      this.parent[x] = this.find(this.parent[x]);
    }
    return this.parent[x];
  }

  union(x, y) {
    const parentX = this.find(x);
    const parentY = this.find(y);

    if (this.rank[parentX] > this.rank[parentY]) {
      this.parent[parentY] = parentX;
    } else {
      this.parent[parentX] = parentY;
      if (this.rank[parentX] === this.rank[parentY]) {
        this.rank[parentX]++;
      }
    }
  }

  getParent() {
    return this.parent;
  }
}
