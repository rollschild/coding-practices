"""
Time: O(NlogN)
Space: O(N)
"""


class Solution:
    def minMeetingRooms(self, intervals: List[List[int]]) -> int:
        # priority queue
        # min heap

        if len(intervals) == 0:
            return 0
        if len(intervals) == 1:
            return 1

        rooms = []
        intervals.sort(key=lambda x: x[0])
        heapq.heappush(rooms, intervals[0][1])

        for meeting in intervals[1:]:
            if rooms[0] <= meeting[0]:
                heapq.heappop(rooms)
            heapq.heappush(rooms, meeting[1])

        return len(rooms)
