/* Time: O(N)
 * space: O(N) */
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} preorder
 * @param {number[]} inorder
 * @return {TreeNode}
 */
var buildTree = function(preorder, inorder) {
  const helper = (preIndex, preLimit, inIndex, inLimit) => {
    if (preIndex > preLimit || inIndex > inLimit) return null;
    const rootVal = preorder[preIndex];
    const rootNode = new TreeNode(rootVal);
    const index = inorder.indexOf(rootVal);
    rootNode.left = helper(preIndex + 1, preLimit, inIndex, index - 1);
    rootNode.right = helper(
      preIndex + index - inIndex + 1,
      preLimit,
      index + 1,
      inLimit,
    );
    return rootNode;
  };

  return helper(0, preorder.length - 1, 0, inorder.length - 1);
};
