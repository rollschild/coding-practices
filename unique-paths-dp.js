/* Time: O(mn)
 * space: O(mn)
 */
/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var uniquePaths = function(m, n) {
  const dp = new Array(m);
  for (let i = 0; i < m; i++) {
    dp[i] = new Array(n).fill(0);
  }

  dp[0][0] = 1;

  for (let r = 0; r < m; r++) {
    for (let c = 0; c < n; c++) {
      if (r === 0 && c === 0) continue;
      if (r === 0) dp[r][c] = dp[r][c - 1];
      else if (c === 0) dp[r][c] = dp[r - 1][c];
      else dp[r][c] = dp[r][c - 1] + dp[r - 1][c];
    }
  }

  return dp[m - 1][n - 1];
};
