/* Time: O(Nk)
 * Space: O(N - k + 1)
 */
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
var maxSlidingWindow = function(nums, k) {
  // brute force
  const len = nums.length;
  let left = 0;
  let right = left + k - 1;
  const res = [];

  while (left <= right && right < len) {
    res.push(Math.max(...nums.slice(left, right + 1)));
    left++;
    right++;
  }

  return res;
};
