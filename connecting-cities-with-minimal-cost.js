/**
 * @param {number} N
 * @param {number[][]} connections
 * @return {number}
 */
var minimumCost = function(N, connections) {
  connections.sort((a, b) => a[2] - b[2]);
  let res = 0;
  const parents = new Array(N);
  for (let i = 0; i < N; i++) {
    parents[i] = i;
  }

  for (let edge of connections) {
    const v1 = edge[0];
    const v2 = edge[1];
    if (find(v1, parents) !== find(v2, parents)) {
      union(v1, v2, parents);
      N--;
      res += edge[2];
    }
  }

  if (N !== 1) return -1;

  return res;
};

// find(x) -> return x's parent/root node
const find = (node, parents) => {
  if (parents[node] === node) {
    return parents[node];
  }
  parents[node] = find(parents[node], parents);
  return parents[node];
};

const union = (node1, node2, parents) => {
  let parentNode1 = find(node1, parents);
  let parentNode2 = find(node2, parents);
  if (parentNode1 !== parentNode2) {
    parents[parentNode2] = parentNode1;
  }
};
