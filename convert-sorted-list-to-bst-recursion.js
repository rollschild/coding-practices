/* Time: O(NlogN)
 * Space: O(logN)
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {TreeNode}
 */
var sortedListToBST = function(head) {
  if (!head) return null;
  if (!head.next) return new TreeNode(head.val);

  const midNode = findMiddle(head);
  const root = new TreeNode(midNode.val);

  root.left = sortedListToBST(head);
  root.right = sortedListToBST(midNode.next);

  return root;
};

const findMiddle = node => {
  let prevPtr = null;
  let slowPtr = node;
  let fastPtr = node;

  while (fastPtr && fastPtr.next) {
    prevPtr = slowPtr;
    slowPtr = slowPtr.next;
    fastPtr = fastPtr.next.next;
  }

  if (prevPtr) {
    // breaks the list
    prevPtr.next = null;
  }

  return slowPtr;
};
