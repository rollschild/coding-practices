/* Time: O(V + E)
 * Space: O(V + E) */
/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {boolean}
 */
var canFinish = function(numCourses, prerequisites) {
  const len = prerequisites.length;
  if (len === 0) return true;
  const graph = {};
  for (const [course1, course2] of prerequisites) {
    if (!(course1 in graph)) graph[course1] = [];
    graph[course1].push(course2);
  }

  const checked = new Array(numCourses).fill(false);
  const path = new Array(numCourses).fill(false);
  for (let currCourse = 0; currCourse < numCourses; currCourse++) {
    if (dfs(currCourse, graph, checked, path)) return false;
  }

  return true;
};

const dfs = (currCourse, graph, checked, path) => {
  if (checked[currCourse]) return false;
  if (path[currCourse]) return true;

  path[currCourse] = true;

  let res = false;
  if (currCourse in graph) {
    for (const adj of graph[currCourse]) {
      res = dfs(adj, graph, checked, path);
      if (res) return true;
    }
  }

  path[currCourse] = false;
  checked[currCourse] = true;

  return res;
};
