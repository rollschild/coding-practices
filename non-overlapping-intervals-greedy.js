/* time: O(NlogN)
 * space: O(1) */
/**
 * @param {number[][]} intervals
 * @return {number}
 */
var eraseOverlapIntervals = function(intervals) {
  let len = intervals.length;
  if (len <= 0) return 0;

  intervals.sort((a, b) => a[0] - b[0]);
  let count = 0;
  let pos = 1;

  while (pos < len) {
    if (intervals[pos][0] >= intervals[pos - 1][1]) {
      pos++;
      continue;
    }
    if (intervals[pos][1] >= intervals[pos - 1][1]) {
      // remove current
      intervals.splice(pos, 1);
    } else {
      intervals.splice(pos - 1, 1);
    }
    len--;
    count++;
  }

  return count;
};
