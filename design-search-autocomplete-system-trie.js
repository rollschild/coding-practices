/**
 * Time:
 *** AutocompleteSystem: O(l*k), where l is the number of sentences,
 *** k is the average length of a sentence
 *** input: O(p + q + mlogm), where p is the search input so far,
 *** q is the number of nodes in the trie consiering the current search input
 *** as root
 *** m is the length of the result list - sorting
 */
/**
 * @param {string[]} sentences
 * @param {number[]} times
 */
var AutocompleteSystem = function(sentences, times) {
  this.freqs = {};
  this.searchInput = "";
  this.trie = new TrieNode("");
  this.history = [...sentences];

  for (let i = 0; i < this.history.length; i++) {
    this.freqs[sentences[i]] = times[i];
  }

  for (const sentence of this.history) {
    this.trie.add(sentence);
  }

  this.reset = function() {
    this.searchInput = "";
    this.trie = new TrieNode("");
    for (const sentence of this.history) {
      this.trie.add(sentence);
    }
  };
};

/**
 * @param {character} c
 * @return {string[]}
 */
AutocompleteSystem.prototype.input = function(c) {
  if (c === "#") {
    this.history.push(this.searchInput);
    this.freqs[this.searchInput] = (this.freqs[this.searchInput] || 0) + 1;
    this.reset();
    return [];
  }
  this.searchInput += c;
  const res = this.trie.search(this.searchInput).sort((a, b) => {
    if (this.freqs[a] < this.freqs[b]) {
      return 1;
    } else if (this.freqs[a] > this.freqs[b]) {
      return -1;
    } else {
      if (a < b) return -1;
      else if (a > b) return 1;
      else return 0;
    }
  });
  return res.length >= 3 ? res.slice(0, 3) : res;
};

function TrieNode(char) {
  this.char = char;
  this.children = new Array(28).fill(null);
}

TrieNode.prototype.add = function(str) {
  const len = str.length;
  let root = this;
  for (let i = 0; i < len; i++) {
    const letter = str[i];
    const pos = letter === " " ? 0 : str.charCodeAt(i) - "a".charCodeAt(0);
    const node = new TrieNode(letter);
    if (!root.children[pos]) {
      root.children[pos] = node;
      root = node;
    } else {
      root = root.children[pos];
    }
    if (i === len - 1) {
      const endNode = new TrieNode("#");
      root.children[26] = endNode;
      break;
    }
  }

  return;
};

// return an array
TrieNode.prototype.search = function(str) {
  const len = str.length;
  const list = [];
  let res = "";

  let node = this;
  for (let i = 0; i < len; i++) {
    const letter = str[i];
    const pos = letter === " " ? 0 : str.charCodeAt(i) - "a".charCodeAt(0);
    if (!node.children[pos] || node.children[pos].char !== letter) {
      return list;
    }
    res += letter;
    node = node.children[pos];
    if (i === len - 1) {
      // now node is the last letter of the string
      // dfs
      traverse(node, res, list);
      break;
    }
  }

  return list;
};

const traverse = (node, word, list) => {
  if (!node) return;
  for (const letterNode of node.children) {
    if (letterNode) {
      const letter = letterNode.char;
      if (letter === "#") {
        list.push(word);
        return;
      }
      traverse(letterNode, word + letter, list);
    }
  }
  return;
};

/**
 * Your AutocompleteSystem object will be instantiated and called as such:
 * var obj = new AutocompleteSystem(sentences, times)
 * var param_1 = obj.input(c)
 */
