/**
 * @param {string} s
 * @return {boolean}
 */

var checkValidString = function(s) {
  if (s.length === 0) return true;
  const len = s.length;
  let cmin = 0;
  let cmax = 0;

  for (let i = 0; i < len; i++) {
    const letter = s[i];

    if (letter === "(") {
      cmin++;
      cmax++;
    }

    if (letter === ")") {
      cmax--;
      cmin = Math.max(cmin - 1, 0);
    }

    if (letter === "*") {
      cmax++;
      cmin = Math.max(cmin - 1, 0);
    }

    if (cmax < 0) return false;
  }

  return cmin === 0;
};
