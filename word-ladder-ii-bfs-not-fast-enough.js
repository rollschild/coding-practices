/**
 * @param {string} beginWord
 * @param {string} endWord
 * @param {string[]} wordList
 * @return {string[][]}
 */
var findLadders = function(beginWord, endWord, wordList) {
  if (!wordList.includes(endWord)) return [];
  const paths = [[beginWord]];
  const res = [];
  let resList = [];
  let level = 1;
  const alphabet = "abcdefghijklmnopqrstuvwxyz";
  wordList.push(endWord);
  let minLevel = wordList.length;
  /*   lookup(endWord, wordList, wordList.length, res, resList, {});
   *   let pos = 0;
   *   resList.sort((a, b) => a.length - b.length);
   *   while (pos < resList.length - 1) {
   *     if (resList[pos].length < resList[pos + 1].length) {
   *       resList = resList.slice(0, pos + 1);
   *       break;
   *     }
   *     pos++;
   *   }
   *  */
  let visited = {};
  while (paths.length > 0) {
    const path = paths.shift();
    if (path.length > level) {
      // reach a new level
      for (let key in visited) {
        wordList.splice(wordList.indexOf(key), 1);
      }
      visited = {};

      if (path.length > minLevel) {
        break;
      } else {
        level = path.length;
      }
    }

    const currWord = path[path.length - 1];
    for (let i = 0; i < currWord.length; i++) {
      for (let j = 0; j < 26; j++) {
        const word = currWord.slice(0, i) + alphabet[j] + currWord.slice(i + 1);
        if (wordList.includes(word)) {
          const tempArr = [...path, word];
          visited[word] = true;
          if (word === endWord) {
            resList.push(tempArr);
            minLevel = level;
          } else {
            paths.push(tempArr);
          }
        }
      }
    }
  }

  return resList;
};

// const lookup = (targetWord, wordList, currArr, resList, visited) => {
//   const currWord = currArr[currArr.length - 1];
//
//   for (let j = 0; j < wordList.length; j++) {
//     if (wordList[j] === currWord) {
//       continue;
//     }
//     if (!visited[wordList[j]]) {
//       [> visited[wordList[j]] = true; <]
//       if (differByOne(currWord, wordList[j])) {
//         if (wordList[j] === targetWord) {
//           resList.push([...currArr, wordList[j]]);
//           return;
//         }
//         lookup(targetWord, wordList, [...currArr, wordList[j]], resList, {
//           ...visited,
//           [wordList[j]]: true,
//         });
//       }
//     }
//   }
// };
const lookup = (targetWord, wordList, minLen, currArr, resList, visited) => {
  const alphabet = "abcdefghijklmnopqrstuvwxyz";
  if (currArr.length >= minLen) return;
  const currWord = currArr[currArr.length - 1];

  for (let i = 0; i < currWord.length; i++) {
    for (let j = 0; j < 26; j++) {
      if (i === 0 && j === 0) console.log("currWord:", currWord);
      const word = currWord.slice(0, i) + alphabet[j] + currWord.slice(i + 1);
      if (i === 0 && j === 0) console.log("word:", word);
      if (!visited[word] && wordList.includes(word)) {
        const tempArr = [...currWord, word];
        visited[word] = true;
        if (word === targetWord) {
          resList.push(tempArr);
          if (minLen > tempArr.length) minLen = tempArr.length;
        } else {
          // lookup(targetWord, wordList, minLen, tempArr, resList, {
          //   ...visited,
          //   [word]: true,
          // });
          lookup(targetWord, wordList, minLen, tempArr, resList, visited);
        }
      }
    }
  }
};

const differByOne = (str1, str2) => {
  if (str1.length !== str2.length) return false;
  let num = 0;
  for (let i = 0; i < str1.length; i++) {
    if (str1[i] !== str2[i]) num++;
  }
  return num === 1;
};
console.log(findLadders("hot", "dog", ["hot", "dog", "dot"]));
console.log(
  findLadders("qa", "sq", [
    "si",
    "go",
    "se",
    "cm",
    "so",
    "ph",
    "mt",
    "db",
    "mb",
    "sb",
    "kr",
    "ln",
    "tm",
    "le",
    "av",
    "sm",
    "ar",
    "ci",
    "ca",
    "br",
    "ti",
    "ba",
    "to",
    "ra",
    "fa",
    "yo",
    "ow",
    "sn",
    "ya",
    "cr",
    "po",
    "fe",
    "ho",
    "ma",
    "re",
    "or",
    "rn",
    "au",
    "ur",
    "rh",
    "sr",
    "tc",
    "lt",
    "lo",
    "as",
    "fr",
    "nb",
    "yb",
    "if",
    "pb",
    "ge",
    "th",
    "pm",
    "rb",
    "sh",
    "co",
    "ga",
    "li",
    "ha",
    "hz",
    "no",
    "bi",
    "di",
    "hi",
    "qa",
    "pi",
    "os",
    "uh",
    "wm",
    "an",
    "me",
    "mo",
    "na",
    "la",
    "st",
    "er",
    "sc",
    "ne",
    "mn",
    "mi",
    "am",
    "ex",
    "pt",
    "io",
    "be",
    "fm",
    "ta",
    "tb",
    "ni",
    "mr",
    "pa",
    "he",
    "lr",
    "sq",
    "ye",
  ]),
);
