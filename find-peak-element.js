/**

 * @param {number[]} nums

 * @return {number}

 */

var findPeakElement = function(nums) {
  const len = nums.length;

  if (len === 1) return 0;

  if (len === 2) return nums[0] > nums[1] ? 0 : 1;

  let left = 0;

  let right = len - 1;

  while (left <= right && right < len) {
    const mid = Math.floor((left + right) / 2);

    const pivot = nums[mid];

    if (
      (mid === 0 && pivot > nums[mid + 1]) ||
      (mid === len - 1 && pivot > nums[mid - 1])
    ) {
      return mid;
    } else if (pivot > nums[mid - 1] && pivot > nums[mid + 1]) {
      return mid;
    } else {
      if (pivot > nums[mid - 1]) {
        left++;
      } else {
        right--;
      }
    }
  }

  return;
};
