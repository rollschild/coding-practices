/**
 * @param {number[]} nums
 * @return {number}
 */
var jump = function(nums) {
  const len = nums.length;
  if (len <= 1) return 0;
  let currFurthest = 0;
  let currEnd = 0;
  let jumps = 0;

  for (let i = 0; i < len - 1; i++) {
    currFurthest = Math.max(currFurthest, nums[i] + i);
    if (currEnd === i) {
      currEnd = currFurthest;
      jumps++;
    }
  }

  return jumps;
};
