/* Time: O(N)
 * Space: O(N)
 */
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {TreeNode}
 */
var sortedListToBST = function(head) {
  let node = head;
  if (!head) return null;

  if (!node.next) return new TreeNode(node.val);

  const nodes = [];
  while (node) {
    nodes.push(node.val);
    node = node.next;
  }
  const len = nodes.length;
  const mid = Math.floor(len / 2);
  const root = new TreeNode(nodes[mid]);
  build(root, nodes);

  return root;
};

const build = (node, nodes) => {
  if (!node) return;
  const mid = Math.floor(nodes.length / 2);
  // leftList and rightList
  const leftList = nodes.slice(0, mid);
  const rightList = nodes.slice(mid + 1);
  if (leftList.length === 0) {
    node.left = null;
  } else if (leftList.length === 1) {
    const leftNode = new TreeNode(leftList[0]);
    node.left = leftNode;
  } else {
    const leftNode = new TreeNode(leftList[Math.floor(leftList.length / 2)]);
    node.left = leftNode;
  }

  if (rightList.length === 0) {
    node.right = null;
  } else if (rightList.length === 1) {
    const rightNode = new TreeNode(rightList[0]);
    node.right = rightNode;
  } else {
    const rightNode = new TreeNode(rightList[Math.floor(rightList.length / 2)]);
    node.right = rightNode;
  }

  build(node.left, leftList);
  build(node.right, rightList);
};
