/* Time: O(N + E)
 * space: O(N + E) */
/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function(n, edges) {
  if (n !== edges.length + 1) return false;

  const graph = {};
  for (const [parent, child] of edges) {
    if (!(parent in graph)) graph[parent] = [];
    graph[parent].push(child);
    if (!(child in graph)) graph[child] = [];
    graph[child].push(parent);
  }

  const parents = { 0: -1 };
  const stack = [0];

  while (stack.length > 0) {
    const node = stack.pop();
    if (!(node in graph)) continue;
    for (const adj of graph[node]) {
      if (adj === parents[node]) continue;
      if (adj in parents) return false;
      parents[adj] = node;
      stack.push(adj);
    }
  }

  return Object.keys(parents).length === n;
};
