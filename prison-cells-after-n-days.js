/**
 * @param {number[]} cells
 * @param {number} N
 * @return {number[]}
 */
var prisonAfterNDays = function(cells, N) {
  if (N === 0) return cells;
  const obj = {};
  obj[0] = [...cells];
  for (let i = 1; i <= N; i++) {
    if (i === 15) break;
    const copy = [...cells];
    cells[0] = 0;
    cells[7] = 0;
    for (let j = 1; j <= 6; j++) {
      if (
        (copy[j - 1] === 1 && copy[j + 1] === 1) ||
        (copy[j - 1] === 0 && copy[j + 1] === 0)
      ) {
        cells[j] = 1;
      } else {
        cells[j] = 0;
      }
    }
    obj[i % 14] = [...cells];
  }
  return N > 14 ? obj[N % 14] : cells;
};
