/**
 * @param {string} s
 * @param {string} p
 * @return {number[]}
 */

var findAnagrams = function(s, p) {
  const sLen = s.length;
  const pLen = p.length;
  if (pLen > sLen) return [];
  const res = [];
  const pSorted = p
    .split("")
    .sort()
    .join("");

  for (let i = 0; i <= sLen - pLen; i++) {
    if (!p.includes(s[i])) continue;
    const str = s.substring(i, i + pLen);
    if (isAnagram(str, pSorted)) res.push(i);
  }

  return res;
};

const isAnagram = (str, p) => {
  const strSorted = str
    .split("")
    .sort()
    .join("");
  if (strSorted === p) return true;
  return false;
};
