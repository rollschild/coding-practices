/**
 * @param {number[][]} intervals
 * @return {number[][]}
 */
var merge = function(intervals) {
  let res = [];
  intervals.sort((a, b) => a[0] - b[0]);

  intervals.forEach(arr => {
    if (res.length === 0 || res[res.length - 1][1] < arr[0]) {
      res.push(arr);
    } else {
      res[res.length - 1][1] = Math.max(res[res.length - 1][1], arr[1]);
    }
  });

  return res;
};
