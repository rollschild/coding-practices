/* Time: O(N*L^2)
 */
/**
 * @param {string[]} words
 * @return {string[]}
 */

var findAllConcatenatedWordsInADict = function(words) {
  if (words.length <= 1) return [];
  words.sort((a, b) => a.length - b.length);
  const dict = {};
  const res = [];

  words.forEach(word => {
    if (check(word, dict)) res.push(word);
    dict[word] = true;
  });

  return res;
};

const check = (word, dict) => {
  if (Object.keys(dict).length === 0) return false;
  if (word.length === 0) return false;
  const len = word.length;
  if (dict[word]) return true;

  for (let i = 1; i <= len; i++) {
    if (dict[word.substring(0, i)] && check(word.substring(i), dict))
      return true;
  }

  return false;
};
