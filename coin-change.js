/* Time: O(S * n) where S is the goal of the amount, n is number of coin types
 * Space: O(S) */
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function(coins, amount) {
  const dp = new Array(amount + 1).fill(amount + 1);
  dp[0] = 0;

  for (const coin of coins) {
    for (let money = coin; money <= amount; money++) {
      dp[money] = Math.min(dp[money], dp[money - coin] + 1);
    }
  }

  return dp[amount] === amount + 1 ? -1 : dp[amount];
};
