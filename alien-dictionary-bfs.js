/* Time: O(C), where C is the total length of *all* words in the input list,
 * added together
 * space: O(1) */
/**
 * @param {string[]} words
 * @return {string}
 */
var alienOrder = function(words) {
  const len = words.length;
  if (len === 0) return "";

  const degrees = {};
  for (const word of words) {
    for (let i = 0; i < word.length; i++) {
      degrees[word[i]] = 0;
    }
  }

  const graph = {};
  for (let j = 0; j < len - 1; j++) {
    const front = words[j];
    const end = words[j + 1];
    let isDifferent = false;
    let pos = Math.min(front.length, end.length);
    for (let k = 0; k < pos; k++) {
      const f = front[k];
      const e = end[k];
      if (f !== e) {
        isDifferent = true;
        if (!(f in graph)) graph[f] = new Set();
        if (!graph[f].has(e)) {
          graph[f].add(e);
          degrees[e]++;
        }
        break;
      }
    }
    if (!isDifferent) {
      if (end.length < front.length) return "";
    }
  }

  const queue = Object.entries(degrees)
    .filter(item => item[1] === 0)
    .map(item => item[0]);
  const res = [];
  while (queue.length > 0) {
    const char = queue.shift();
    res.push(char);
    if (!(char in graph)) continue;
    for (const adj of graph[char]) {
      degrees[adj]--;
      if (degrees[adj] === 0) queue.push(adj);
    }
  }

  if (res.length !== Object.keys(degrees).length) return "";

  return res.join("");
};
