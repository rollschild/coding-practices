/* Time: O(n)
 * space: O(n)
 */
/**
 * @param {number} n
 * @return {number}
 */
var climbStairs = function(n) {
  // recursion with memoization
  const memo = new Array(n + 1).fill(0);
  return climbFromPosition(0, n, memo);
  // return memo[0];
};

const climbFromPosition = (pos, n, memo) => {
  if (pos > n) return 0;
  if (pos === n) return 1;
  if (memo[pos] > 0) return memo[pos];
  memo[pos] =
    climbFromPosition(pos + 1, n, memo) + climbFromPosition(pos + 2, n, memo);
  return memo[pos];
};
