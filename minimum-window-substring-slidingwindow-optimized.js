/* Time: O(S + T)
 * Space: O(S + T)
 */
/**
 * @param {string} s
 * @param {string} t
 * @return {string}
 */
var minWindow = function(s, t) {
  let res = "";
  let left = 0;
  let right = 0;
  const target = {};
  for (let i = 0; i < t.length; i++) {
    target[t[i]] = (target[t[i]] || 0) + 1;
  }

  const obj = {};
  let formed = 0;
  while (left <= right && right <= s.length) {
    const letter = s[right];

    if (letter in target) {
      obj[letter] = (obj[letter] || 0) + 1;
      if (obj[letter] === target[letter]) formed++;

      while (formed === Object.keys(target).length) {
        if (res.length === 0 || res.length > right - left + 1) {
          res = s.substring(left, right + 1);
        }
        const leftLetter = s[left];
        left++;
        if (leftLetter in target) {
          if (obj[leftLetter] === 1) {
            delete obj[leftLetter];
            formed--;
            break;
          }
          obj[leftLetter]--;
          if (obj[leftLetter] < target[leftLetter]) {
            formed--;
            break;
          }
        }
      }
    }
    right++;
  }

  return res;
};
