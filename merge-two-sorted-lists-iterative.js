/* Time: O(n + m)
 * space: O(1)
 */
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var mergeTwoLists = function(l1, l2) {
  if (!l1) return l2;
  if (!l2) return l1;
  let root = new ListNode(0);
  const resNode = root;
  while (l1 && l2) {
    if (l1.val < l2.val) {
      root.next = l1;
      l1 = l1.next;
    } else {
      root.next = l2;
      l2 = l2.next;
    }
    root = root.next;
  }
  if (l1) root.next = l1;
  else if (l2) root.next = l2;

  return resNode.next;
};
