function linkedListToInt(list) {
  let res = 0;
  let pos = 0;
  while (list) {
    res += Math.pow(10, pos) * list.val;
    pos++;
    list = list.next;
  }
  return res;
}

function Node(digit) {
  this.val = digit;
  this.next = null;
}

const intToLinkedList = num => {
  if (num === 0) {
    return null;
  }

  let val = num % 10;
  num = Math.floor(num / 10);

  let curr = new Node(val);
  curr.next = intToLinkedList(num);

  return curr;
};

var addTwoNumbers = function(l1, l2) {
  const num1 = linkedListToInt(l1);
  const num2 = linkedListToInt(l2);
  const res = intToLinkedList(num2 + num1);
  return res;
};

const node2 = new Node(2);
const node4 = new Node(4);
const node3 = new Node(3);
node2.next = node4;
node4.next = node3;

const node5 = new Node(5);
const node44 = new Node(4);
const node6 = new Node(6);
node5.next = node6;
node6.next = node44;

addTwoNumbers(node2, node5);
