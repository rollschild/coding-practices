/**
 * initialize your data structure here.
 */

var MedianFinder = function() {
  this.arr = [];
};

/**
 * @param {number} num
 * @return {void}
 */

MedianFinder.prototype.addNum = function(num) {
  this.arr.push(num);
  this.arr.sort((a, b) => a - b);
};

/**
 * @return {number}
 */

MedianFinder.prototype.findMedian = function() {
  const len = this.arr.length;
  if (len % 2 === 0) {
    const left = len / 2 - 1;
    const right = left + 1;
    return (this.arr[left] + this.arr[right]) / 2;
  } else {
    return this.arr[(len - 1) / 2];
  }
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * var obj = new MedianFinder()
 * obj.addNum(num)
 * var param_2 = obj.findMedian()
 */
