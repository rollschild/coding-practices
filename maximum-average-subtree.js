/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var maximumAverageSubtree = function(root) {
  const res = [];
  compute(root, res);
  res.sort((a, b) => a - b);
  return res[res.length - 1];
};

const compute = (node, res) => {
  if (!node) {
    return [0, 0];
  }

  const [sumLeft, numLeft] = compute(node.left, res);
  const [sumRight, numRight] = compute(node.right, res);
  const sum = sumLeft + sumRight + node.val;
  const num = numLeft + numRight + 1;
  res.push(sum / num);
  return [sum, num];
};
