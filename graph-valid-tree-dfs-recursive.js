/**
 * @param {number} n
 * @param {number[][]} edges
 * @return {boolean}
 */
var validTree = function(n, edges) {
  if (edges.length !== n - 1) return false;
  if (edges.length === 0) return true;
  // first construct the adjacency list
  const graph = {};
  for (const [startNode, endNode] of edges) {
    if (!graph.hasOwnProperty(startNode)) {
      graph[startNode] = [];
    }
    graph[startNode].push(endNode);
    if (!graph.hasOwnProperty(endNode)) {
      graph[endNode] = [];
    }
    graph[endNode].push(startNode);
  }

  // dfs
  const firstNode = edges[0][0];
  const parents = {};
  parents[firstNode] = -1;
  const res = dfs(firstNode, n, graph, parents);
  return res && Object.keys(parents).length === n;
};

const dfs = (node, numOfNodes, graph, parents) => {
  if (!graph.hasOwnProperty(node)) return false;
  for (const adj of graph[node]) {
    if (parents[node] === adj) continue;
    if (adj in parents) return false;
    parents[adj] = node;
    const result = dfs(adj, numOfNodes, graph, parents);
    if (!result) return false;
  }
  return true;
};
