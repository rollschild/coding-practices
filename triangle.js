/* Time O(nk), where k is max number of elements for each row
 * Space O(n), where n is number of rows
/**
 * @param {number[][]} triangle
 * @return {number}
 */
var minimumTotal = function(triangle) {
  const len = triangle.length;
  const dp = new Array(len + 1).fill(0);

  for (let r = len - 1; r >= 0; r--) {
    for (let c = 0; c < triangle[r].length; c++) {
      dp[c] = Math.min(dp[c], dp[c + 1]) + triangle[r][c];
    }
  }

  return dp[0];
};
