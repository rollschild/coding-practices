class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        if len(matrix) == 0:
            return 0

        row = len(matrix)
        col = len(matrix[0])
        prev = 0
        dp = [0 for i in range(0, col)]
        max_len = 0

        for r in range(0, row):
            for c in range(0, col):
                if matrix[r][c] == "1":
                    temp = dp[c]
                    if c == 0:
                        dp[c] = 1
                    else:
                        dp[c] = min(dp[c - 1], prev, temp) + 1
                    prev = temp
                    max_len = max(max_len, dp[c])
                else:
                    dp[c] = 0

        return max_len * max_len
